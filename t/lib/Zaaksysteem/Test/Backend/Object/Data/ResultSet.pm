package Zaaksysteem::Test::Backend::Object::Data::ResultSet;

use Zaaksysteem::Test;

use Zaaksysteem::Backend::Object::Data::ResultSet;
use Zaaksysteem::Object::Query;

=head1 NAME

Zaaksysteem::Test::Backend::Object::Data::ResultSet - Test Object query parsing

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Backend::Object::Data::ResultSet;

=cut

sub test_parse {
    my $rs = 'Zaaksysteem::Backend::Object::Data::ResultSet';
    my $field_sql = "index_hstore->'field'";
    my $a = qb_lit('text', 'a');
    my $b = qb_lit('text', 'b');
    
    lives_ok {
        $rs->parse_expr(qb_eq('field', 'value'));
    } 'Parser lives using basic expression';
    
    my ($sql, @args) = $rs->parse_expr(qb_lit('text', 'value'));

    is($sql, '?', 'parse(qb_lit())->sql is placeholder');

    for (@args) {
        ok(ref $_ eq 'ARRAY', 'bindvalue is [ col => value ] type');
        is($_->[1], 'value', 'bindvalue is literal value');
    }

    ($sql, @args) = $rs->parse_expr(qb_eq('field', 'value'));

    is($sql, sprintf('(%s = ?)', $field_sql), 'equality sql');
    is($args[0][1], 'value', 'quality argument');

    ($sql, @args) = $rs->parse_expr(qb_and($a, $b));

    is($sql, '? AND ?', 'conjunction sql');

    ($sql, @args) = $rs->parse_expr(qb_or($a, $b));

    is($sql, '? OR ?', 'disjunction sql');

    ($sql, @args) = $rs->parse_expr(qb_not($a));

    is($sql, 'NOT (?)', 'inversion sql');

    ($sql, @args) = $rs->parse_expr(qb_eq($a, $b));

    is($sql, '(? = ?)', 'equality sql');

    ($sql, @args) = $rs->parse_expr(qb_re($a, 'pattern'));

    is($sql, "(? ~ 'pattern')", 'regex sql');

    ($sql, @args) = $rs->parse_expr(qb_in($a, [qw[1 2 3]]));

    is($sql, '(? IN (?, ?, ?))', 'membership test sql');
    is($args[0][1], 'a', 'membership placeholder');
    is($args[1][1], '1', 'membership set element 1');
    is($args[2][1], '2', 'membership set element 2');
    is($args[3][1], '3', 'membership set element 3');

    ($sql, @args) = $rs->parse_expr(qb_set($a, $b));

    is($sql, '(?, ?)', 'set elements sql');

    ($sql, @args) = $rs->parse_expr(qb_field('field'));

    is($sql, $field_sql, 'field reference sql');

    ($sql, @args) = $rs->parse_expr(qb_like('field', 'value'));

    is(
        $sql,
        sprintf("(%s ILIKE ('%%' || (?)::text || '%%'))", $field_sql),
        'infix substr sql'
    );

    ($sql, @args) = $rs->parse_expr(qb_like('field', 'value', 'prefix'));

    is(
        $sql,
        sprintf("(%s ILIKE ((?)::text || '%%'))", $field_sql),
        'prefix substr sql'
    );

    ($sql, @args) = $rs->parse_expr(qb_like('field', 'value', 'postfix'));

    is(
        $sql,
        sprintf("(%s ILIKE ('%%' || (?)::text))", $field_sql),
        'postfix substr sql'
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
