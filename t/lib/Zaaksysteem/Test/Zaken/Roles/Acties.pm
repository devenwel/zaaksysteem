package Zaaksysteem::Test::Zaken::Roles::Acties;

use Zaaksysteem::Test;
use Zaaksysteem::Zaken::Roles::Acties;

=head1 NAME

Zaaksysteem::Test::Zaken::Roles::Acties - Test for the "Acties" role for Zaak instances

=head1 SYNOPSIS

    prove -l :: Zaaksysteem::Test::Zaken::Roles::Acties

=cut

sub test_update_streefafhandeldatum {
    my $now = DateTime->now;
    my $days = 4;

    my $zaak = Zaaksysteem::Test::MockZaak->new(
        status => "stalled",
        stalled_since => $now->clone()->subtract(days => $days),
        streefafhandeldatum => $now,
    );

    $zaak->wijzig_status({ status => "new" });

    is($zaak->status, "new", "Status has changed to 'new'");
    is($zaak->streefafhandeldatum, $now->clone()->add(days => $days), "Streefafhandeldatum has updated");
}

package Zaaksysteem::Test::MockZaak {
    use Moose;
    use Zaaksysteem::Types qw/DateTimeObject/;
    use Zaaksysteem::Test::Mocks qw/mock_one/;

    with qw(
        Zaaksysteem::Zaken::Roles::Acties
    );

    has status => (
        is => 'rw',
        isa => 'Str',
    );

    has streefafhandeldatum => (
        is => 'rw',
        isa => DateTimeObject,
    );

    has stalled_since => (
        is => 'rw',
        isa => DateTimeObject,
    );

    has stalled_until => (
        is => 'rw',
    );

    sub id { 1 }
    sub behandelaar { 'Henk de Vries' }
    sub trigger_logging { mock_one() }
    sub log { mock_one() }
    sub result_source { mock_one(
        format_datetime_object => sub { shift }
    )}
    sub update { mock_one() }
    sub reden_opschorten { }
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
