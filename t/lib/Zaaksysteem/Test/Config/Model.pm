package Zaaksysteem::Test::Config::Model;

use Moose;
extends "Zaaksysteem::Test::Moose";

use Zaaksysteem::Test;
use Zaaksysteem::Config::Model;
use Zaaksysteem::Object::Types::Config::Item;

sub test_pod {
    pod_coverage_ok('Zaaksysteem::Config::Model');
}

sub _get_model {

    my $model = Zaaksysteem::Config::Model->new(
        schema                      => mock_schema(),
        item_rs                     => mock_one,
        definition_store            => mock_one,
        bibliotheek_notificaties_rs => mock_one,
        subject_model               => mock_one,
        groups_rs                   => mock_one,
        roles_rs                    => mock_one,
        @_,
    );
    isa_ok($model, "Zaaksysteem::Config::Model");
    return $model;
}

sub test_search_items {

    my $model = _get_model();

    # TODO: Don't mock but have a simplestore ready for use
    my $override = override("Zaaksysteem::Config::Model::item_store" => sub {
        return mock_strict( 'X-Mock-SelfArg' => 1,
            search => sub {
                my ($self, $qb) = @_;
                isa_ok($qb, 'Zaaksysteem::Object::Query', "Arguments to item_store::search are correct");
                return (42);
            });
    });

    my @foo = ();
    my $items = $model->search_items_by_uuid(@foo);
    cmp_deeply($items, [42], "Got one item with value '42'");

}

1;

__END__

=head1 NAME

Zaaksysteem::Test::Config::Model - Test Zaaksysteem::Test::Config::Model

=head1 DESCRIPTION

Test the config model used by api/v1

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Config::Model

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
