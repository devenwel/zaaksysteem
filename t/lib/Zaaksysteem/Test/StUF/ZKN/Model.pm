package Zaaksysteem::Test::StUF::ZKN::Model;
use Moose;
extends 'Zaaksysteem::Test::Moose';
use Zaaksysteem::Test;

=head1 NAME

Zaaksysteem::Test::StUF::ZKN::Model - Test the base module of StUF-ZKN

=head2 SYNOPSIS

    prove -l :: Zaaksysteem::Test::StUF::ZKN::Model

=cut

use Zaaksysteem::StUF::ZKN;
use Zaaksysteem::StUF::ZKN::0102::Model;

sub test_stuf_zkn_model {

    my $model = Zaaksysteem::StUF::ZKN->new(
        schema            => mock_one(),
        interface         => mock_one(),
        major_version     => 1,
        minor_version     => 2,
        municipality_code => 9999,
        soap_actions      => [],
    );

    isa_ok($model, 'Zaaksysteem::StUF::ZKN');

    my @functions = qw(
        label
        shortname
        version
        find_case_by_reference
        create_object_subscription
        supports_soap_action
        _build_municipality_name
    );
    foreach (@functions) {
        can_ok($model, $_);
    }
}

sub test_stuf_zkn_version_1_2 {

    my $model = Zaaksysteem::StUF::ZKN::0102::Model->new(
        schema            => mock_one(),
        interface         => mock_one(),
        municipality_code => 9999,
    );

    isa_ok($model, 'Zaaksysteem::StUF::ZKN::0102::Model');

    is($model->label, 'StUF-ZKN versie 1.2', "label is correct for StUF ZKN version 1.2");
    is($model->shortname, 'stufzkn_1.2', "shortname is correct for StUF ZKN version 1.2");
    is($model->version, 1.2, "version is correct for StUF ZKN version 1.2");

    cmp_deeply(
        $model->soap_actions,
        [qw(
            http://www.stufstandaarden.nl/koppelvlak/zds0120/geefLijstZaakdocumenten_ZakLv01
            http://www.stufstandaarden.nl/koppelvlak/zds0120/geefZaakdocumentLezen_EdcLv01
        )],
        "Supports all the right actions"
    );

    is($model->_build_municipality_name, 'Testgemeente', "Builds the correct municipality name");
}

sub test_stuf_zkn_version_1_2_geefLijstZaakdocumenten_ZakLv01 {

    my $model = Zaaksysteem::StUF::ZKN::0102::Model->new(
        schema            => mock_one(),
        interface         => mock_one(),
        municipality_code => 9999,
    );

    isa_ok($model, 'Zaaksysteem::StUF::ZKN::0102::Model');

    my $soap_action = 'http://www.stufstandaarden.nl/koppelvlak/zds0120/geefLijstZaakdocumenten_ZakLv01';
    my $xml = "";

    ok($model->supports_soap_action($soap_action), "Supports '$soap_action'");

    TODO : {
        local $TODO = "Unimplemented code path";

        lives_ok(sub {
        my $ok = $model->process_action($soap_action, $xml);
        }, "TODO: Fix me");

    };

}



1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

