package Zaaksysteem::Test::Types;

use Moose;

extends 'Zaaksysteem::Test::Moose';

=head1 NAME

Zaaksysteem::Test::Types - Test the Zaaksysteem Type checks

=head2 SYNOPSIS

    prove -l :: Zaaksysteem::Test::Types

=cut

use Zaaksysteem::Test;
use DateTime;

use Zaaksysteem::Constants qw(
    ZAAKSYSTEEM_CONSTANTS
    ZAAKSYSTEEM_CSS_TEMPLATES
    ZAAKSYSTEEM_CSS_TEMPLATES_OLD
);

use Zaaksysteem::Types qw[
    ACLCapability
    ACLEntityType
    ACLScope
    Betrokkene
    BetrokkeneBedrijf
    CaseResult
    ChannelOfContact
    CompanyCocLocationNumber
    CompanyCocNumber
    CompanyCocRSIN
    CountryCode
    CustomerTemplate
    CustomerTemplateCurrent
    CustomerType
    Datestamp
    FQDN
    KvK
    MobileNumberSloppy
    MunicipalityCode
    ObjectSubjectType
    Otap
    RemoteSearchModuleName
    SubjectType
    TelephoneNumberSloppy
    Timestamp
    ZSFQDN
];

use Zaaksysteem::BR::Subject::Constants ':remote_search_module_names';

sub test_country_code {
    ok(CountryCode->check('5095'),  "Aruba dushi terra");
    ok(!CountryCode->check('0001'), "Non existent country");
}

sub test_municipality_code {
    ok(MunicipalityCode->check('0437'), "Ouder-Amstel");
    ok(!MunicipalityCode->check(0),     "Non existent municipality");
}

sub test_subject_type {
    ok(!SubjectType->check('invalid'), "'invalid' isn't a subject type");
    ok(SubjectType->check('natuurlijk_persoon'), "'natuurlijk_persoon' is");

    ok(!ObjectSubjectType->check('invalid'), "'invalid' isn't a subject type");
    ok(ObjectSubjectType->check('person'), "'person' is");
}

sub test_case_result {
    ok(!CaseResult->check('foo'), "'foo' isn't a case result type");
    ok(CaseResult->check('afgehandeld'), "'afgehandeld' is");
}

sub test_acl_types {
    ok(ACLEntityType->check('position'), "'position is correct");
    ok(!ACLEntityType->check('noo'),     "and 'noo' isn't");

    ok(ACLScope->check('type'), "'scope is correct'");
    ok(!ACLScope->check('noo'), "and 'noo' isn't");

    ok(ACLCapability->check('read'), "'capability is correct'");
    ok(!ACLCapability->check('no'),  "and 'noo' isn't");
}

sub test_betrokkene {
    ok(Betrokkene->check('betrokkene-natuurlijk_persoon-666'), 'Betrokkene matches natuurlijk_persoon identifier');
    ok(
        !Betrokkene->check('betrokkene-natuurlijk_persoon-betrokkene-natuurlijk_persoon-666'),
        'Betrokkene does not match invalid natuurlijk_persoon identifier'
    );
    ok(Betrokkene->check('betrokkene-bedrijf-42'), 'Betrokkene matches bedrijf identifier');
    ok(
        !Betrokkene->check('betrokkene-bedrijf-betrokkene-bedrijf-42'),
        'Betrokkene does not match invalid bedrijf identifier',
    );
    ok(!Betrokkene->check('42'), 'Betrokkene does not match invalid identifier');
}

sub test_betrokkene_bedrijf {
    ok(BetrokkeneBedrijf->check('betrokkene-bedrijf-42'), 'Betrokkene matches bedrijf identifier');
    ok(
        !BetrokkeneBedrijf->check('betrokkene-bedrijf-betrokkene-bedrijf-42'),
        'Betrokkene does not match invalid bedrijf identifier',
    );
    ok(!BetrokkeneBedrijf->check('42'), 'Betrokkene does not match invalid identifier');
}

sub test_channel_of_contact {
    _test_enum(ChannelOfContact, ZAAKSYSTEEM_CONSTANTS->{contactkanalen}, "testsuite");
}

sub test_customer_type {
    _test_enum(CustomerType, [qw(commercial government overheid commercieel)], "CustomerFoo");
}

sub test_otap {
    _test_enum(Otap, [qw(development testing accept production)], "quarterly");
}

sub test_customer_template {
    my @templates = @{ZAAKSYSTEEM_CSS_TEMPLATES()};
    _test_enum(CustomerTemplateCurrent, \@templates, 'bussum');

    push(@templates, @{ZAAKSYSTEEM_CSS_TEMPLATES_OLD()});
    _test_enum(CustomerTemplate, \@templates, 'foo');


}

sub test_kvk {
    # The KVK claims their kvk numbers are elfproef, but they aren't
    # https://www.kvk.nl/download/DataserviceInschrijving-FunctioneleServiceBeschrijving-V2.3_tcm109-396683.pdf
    # IPD0004
    # Het opgegeven KvKnummer voldoet niet aan zijn formaat.
    # - 8 posities lang
    # - Voldoet niet aan 11-proef
    #
    # Lies lies lies
    my $kvk = 30259896; # https://www.kvk.nl/orderstraat/product-kiezen/?kvknummer=302598960000
    ok(KvK->check($kvk), "$kvk is a valid KvK");
}

sub test_datestamp {
    ok Datestamp->check(DateTime->today(time_zone => 'floating')),
        'today is a valid datestamp';

    ok !Datestamp->check(DateTime->today(time_zone => 'UTC')),
        'today(UTC) is not a valid datestamp';

    lives_ok {
        Datestamp->assert_coerce(DateTime->today(time_zone => 'UTC'))
    } 'UTC DateTime instance coerced to valid Datestamp';

    lives_ok {
        Datestamp->assert_coerce(DateTime->today(time_zone => 'floating'))
    } 'floating DateTime instance coerced to valid Datestamp';

    lives_ok {
        Datestamp->assert_coerce(DateTime->today())
    } 'vanilla DateTime instance coerced to valid Datestamp';

    my $ex = exception {
        Datestamp->assert_valid(DateTime->now(time_zone => 'UTC'))
    };

    ok defined $ex && "$ex" =~ m/DateTime not in floating time zone/,
        'exception gives expected error message';

    my $dt = Datestamp->coerce('1987-10-10');

    ok Datestamp->check($dt), 'coerced datestamp validates';

    isa_ok $dt, 'DateTime', 'datestamp coercion';
    ok $dt->time_zone->is_floating, 'coerced datestamp timezone is floating';

    my $dt2 = Datestamp->coerce('1987-10-10T21:27:24Z');

    ok Datestamp->check($dt2), 'coerced timestamp string is valid datestamp';

    isa_ok $dt2, 'DateTime', 'datestamp coercion with time and zone';
    ok $dt2->time_zone->is_floating, 'coerced datestamp with time and zone has floating timezone';

    ok $dt->compare($dt2) == 0, 'datestamps equal after truncating';

    my $dt3 = Datestamp->coerce('1937-07-01T00:00:00');

    ok Datestamp->check($dt3), 'problematic date validates as datestamp';

    isa_ok $dt3, 'DateTime', 'datestamp coercion for problematic date';

    dies_ok { $dt3->set_time_zone('Europe/Amsterdam') } 'problematic date to EU/AMS timezone';

    dies_ok {
        DateTime->new(
            year => 1937,
            month => 7,
            day => 1,
            hour => 0,
            minute => 0,
            second => 0,
            time_zone => 'Europe/Amsterdam'
        )
    } 'problematic/non-existent date dies during construction';
}

sub test_timestamp {
    ok Timestamp->check(DateTime->now()), 'Timestamp type accepts DT->now()';

    ok Timestamp->check(DateTime->now(time_zone => 'UTC')),
        'Timestamp type accepts DT in UTC time_zone';

    lives_ok {
        Timestamp->assert_coerce(DateTime->now(time_zone => 'floating'))
    } 'Timestamp coerced value accepts floating time zone datetime input';

    lives_ok {
        Timestamp->assert_coerce(DateTime->now(time_zone => 'UTC'))
    } 'Timestamp coerced value accepts UTC time zone datetime input';

    lives_ok {
        Timestamp->assert_coerce('2017-10-10')
    } 'Timestamp coerced value accepts datestring input';

    lives_ok {
        Timestamp->assert_coerce('2017-10-10T12:34:56');
    } 'Timestamp coerced value accepts floating time zone datetime input string';

    lives_ok {
        Timestamp->assert_coerce('2017-10-10T12:34:56Z');
    } 'Timestamp coerced value accepts UTC time zone datetime input string';

    my $ex = exception {
        Timestamp->assert_valid(DateTime->now(time_zone => 'floating'))
    };

    ok defined $ex && "$ex" =~ m/DateTime not in UTC time zone/,
        'Timestamp validation of floating time zone DateTime threw exception';

    my $coerced = Timestamp->coerce('1987-10-10T12:34:56Z');

    isa_ok $coerced, 'DateTime', 'coerced timestamp string';

    ok $coerced->time_zone->is_utc, 'coerced timestamp string has UTC time zone';

    my $floating = Timestamp->coerce('1987-10-10T12:34:56');

    ok $floating->time_zone->is_utc, 'coerced timestamp string has UTC time zone';

    my $converty = DateTime->now(time_zone => 'Europe/Amsterdam');
    my $converted = Timestamp->coerce($converty);

    # Verify type constraint and coercion clone coerced values.
    ok $converted->time_zone->is_utc, 'coerced value has expected time zone';
    ok !$converty->time_zone->is_utc, 'original value not mutated';

    ok $converty->compare($converted) == 0,
        'coerced value still equal after time zone translation';
}

sub test_19400516_timezone_jump {
    note "Testing timezone jump on 16th May 1940 from NET->CEST";

    # This date+time does not exist in EU/AMS timezone
    my $ds = Datestamp->coerce('1940-05-16T00:00:00');

    isa_ok $ds, 'DateTime', 'Datestamp coercion for 1940-05-16T00:00:00 ok';

    ok $ds->time_zone->is_floating, 'Datestamp coerced in floating TZ for 1940-05-16T00:00:00';

    # This date+time does not exist in EU/AMS timezone
    my $ds2 = Datestamp->coerce('1940-05-16T00:00:00Z');

    isa_ok $ds2, 'DateTime', 'Datestamp coercion for 1940-05-16T00:00:00Z ok';

    ok $ds2->time_zone->is_floating, 'Datestamp coerced in floating TZ for 1940-05-16T00:00:00Z';
}

sub test_company_coc_number {
    my $test_0_digits = '';
    ok( ! CompanyCocNumber->check($test_0_digits), "CocNumber is rejected with 0 digits");

    my $test_5_digits = '12345';
    ok( ! CompanyCocNumber->check($test_5_digits), "CocNumber is rejected with 5 digits");

    my $test_6_digits = '123456';
    ok(   CompanyCocNumber->check($test_6_digits), "CocNumber is 6 digits");

    my $test_6_others = 'Ab#~.6';
    ok( ! CompanyCocNumber->check($test_6_others), "CocNumber is rejected with 6 others");

    my $test_7_digits = '1234567';
    ok(   CompanyCocNumber->check($test_7_digits), "CocNumber is 7 digits");

    my $test_7_others = 'Ab#~.6&8';
    ok( ! CompanyCocNumber->check($test_7_others), "CocNumber is rejected with 7 others");

    my $test_8_digits = '12345678';
    ok(   CompanyCocNumber->check($test_8_digits), "CocNumber is 8 digits");

    my $test_8_others = 'Ab#~.6&8';
    ok( ! CompanyCocNumber->check($test_8_others), "CocNumber is rejected with 8 others");

    my $test_9_digits = '123456789';
    ok( ! CompanyCocNumber->check($test_9_digits), "CocNumber is rejected with 9 digits");

    my $test_8_zeroes = '000012345678';
    ok( ! CompanyCocNumber->check($test_8_zeroes), "CocNumber is rejected with 8 digits with leading extra zeroes");
}

sub test_company_coc_location_number {
    my $test_some_digits = '123456789012';
    ok(   CompanyCocLocationNumber->check($test_some_digits), "CompanyCocLocationNumber is okay with some digits");

    my $test_less_digits = '12345678901';
    ok(   CompanyCocLocationNumber->check($test_less_digits), "CompanyCocLocationNumber is okay with less digits");

    my $test_some_zeroes = '000056789012';
    ok(   CompanyCocLocationNumber->check($test_some_zeroes), "CompanyCocLocationNumber is okay with some digits");

    my $test_more_digits = '1234567890123';
    ok( ! CompanyCocLocationNumber->check($test_more_digits), "CompanyCocLocationNumber is rejected with more digits");

    my $test_some_others = 'Abc#@$43fey&';
    ok( ! CompanyCocLocationNumber->check($test_some_others), "CompanyCocLocationNumber is rejected with some others");
}

sub test_company_coc_rsin {
    my $test_nine_digits = '123456789';
    ok(   CompanyCocRSIN->check($test_nine_digits), "CompanyCocLocationNumber is okay with nine digits");

    my $test_less_digits = '12345678';
    ok( ! CompanyCocRSIN->check($test_less_digits), "CompanyCocLocationNumber is rejected with less digits");

    my $test_some_zeroes = '000056789';
    ok(   CompanyCocRSIN->check($test_some_zeroes), "CompanyCocLocationNumber is okay with zeroes");

    my $test_more_digits = '1234567890';
    ok( ! CompanyCocRSIN->check($test_more_digits), "CompanyCocLocationNumber is rejected with more digits");

    my $test_some_others = 'Abc#@$43f';
    ok( ! CompanyCocRSIN->check($test_some_others), "CompanyCocLocationNumber is rejected with some others");

}

sub test_fqdn {

    my @fqdn = qw(foo.bar bar.nl bar.zaaksysteem.nl.tld bar.zaaksysteem.nl bar.zaaksysteem.net);

    foreach (@fqdn) {
        ok(FQDN->check($_), "$_ is a valid FQDN");
    }

    my @fail = (pop(@fqdn), pop(@fqdn));

    foreach (@fqdn) {
        ok(ZSFQDN->check($_), "$_ is a valid ZSFQDN");
    }

    foreach (@fail) { 
        ok(!ZSFQDN->check($_), "$_ isn't a valid ZSFQDN");
    }


}

sub test_RemoteSearchModuleName {

    my $warn;
    TODO : {
        local $TODO = "Fix the 'stuf' entry that is now allowed.. ";
        local $SIG{__WARN__} = sub { $warn = shift };
        _test_enum(
            RemoteSearchModuleName, [
                REMOTE_SEARCH_MODULE_NAME_KVKAPI,
                REMOTE_SEARCH_MODULE_NAME_STUFNP,
                REMOTE_SEARCH_MODULE_NAME_OVERHEIDIO,
            ],
        'stuf'
        );
    }

    like($warn, qr/'stuf' issued tombstone on 20180518/,
        "Tomstoned issued: this test will fail if the TODO above is resolved");
}

sub test_telephone_number_sloppy {
    my %valid = (
        '0612345678'   => 'Valid 06 number',
        '+31612345678' => 'Valid +316 number',
        '1234567890'   => 'Technically valid by frontend validation',
        '1234567898'   => 'Specifically given in ZS-15731',
    );

    foreach my $test (keys %valid) {
        ok TelephoneNumberSloppy->check($test), $valid{$test};
    }

    my %invalid = (
        '1'                => 'Too short',
        '12'               => 'Too short',
        '123'              => 'Too short',
        '1234'             => 'Too short',
        '12345'            => 'Too short',
        '123456'           => 'Too short',
        '1234567'          => 'Too short',
        '12345678'         => 'Too short',
        '123456789'        => 'Too short',
        'a123456789'       => 'Contains an invalid character',
        '-316123456789'    => 'Contains an invalid character',
        '1234567890123456' => 'Too long',
    );

    foreach my $test (%invalid) {
        ok !TelephoneNumberSloppy->check($test), $invalid{$test};
    }
}

sub test_mobile_number_sloppy {
    my %valid = (
        '0612345678'   => 'Valid 06 number',
        '+31612345678' => 'Valid +316 number',
        '1234567890'   => 'Technically valid by frontend validation',
        '1234567898'   => 'Specifically given in ZS-15731',
    );

    foreach my $test (keys %valid) {
        ok MobileNumberSloppy->check($test), $valid{$test};
    }

    my %invalid = (
        '1'                => 'Too short',
        '12'               => 'Too short',
        '123'              => 'Too short',
        '1234'             => 'Too short',
        '12345'            => 'Too short',
        '123456'           => 'Too short',
        '1234567'          => 'Too short',
        '12345678'         => 'Too short',
        '123456789'        => 'Too short',
        'a123456789'       => 'Contains an invalid character',
        '-316123456789'    => 'Contains an invalid character',
        '1234567890123456' => 'Too long',
    );

    foreach my $test (%invalid) {
        ok !MobileNumberSloppy->check($test), $invalid{$test};
    }
}

sub _test_enum {
    my ($type, $is, $not) = @_;

    if (ref $is eq 'ARRAY') {
        foreach (@$is) {
           ok($type->check($_), "'$_' is a $type");
        }
    }
    if (defined $not) {
        ok(!$type->check($not), ".. and '$not' isn't");
    }
    else {
        fail("You haven't defined a not for testing enums for type $type")
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017-2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
