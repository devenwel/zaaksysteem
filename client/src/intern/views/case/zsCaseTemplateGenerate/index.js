import angular from 'angular';
import template from './template.html';
import composedReducerModule from './../../../../shared/api/resource/composedReducer';
import vormFieldsetModule from './../../../../shared/vorm/vormFieldset';
import selectModule from './../../../../shared/vorm/types/select';
import inputModule from './../../../../shared/vorm/types/input';
import vormValidatorModule from './../../../../shared/vorm/util/vormValidator';
import seamlessImmutable from 'seamless-immutable';
import messages from './../../../../shared/vorm/util/vormValidator/messages';
import templateForm from './../forms/template';
import get from 'lodash/get';
import first from 'lodash/head';
import find from 'lodash/find';

export default
	angular.module('zsCaseTemplateGenerate', [
		composedReducerModule,
		vormFieldsetModule,
		selectModule,
		inputModule,
		vormValidatorModule
	])
		.directive('zsCaseTemplateGenerate', [ 'composedReducer', 'vormValidator', ( composedReducer, vormValidator ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					templates: '&',
					caseDocuments: '&',
					onClose: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this,
						fieldReducer,
						validityReducer,
						values = seamlessImmutable({ });

					let setTemplateDefaults = ( ) => {

						let tpl = find(ctrl.templates(), ( t ) => {
							return t.bibliotheek_sjablonen_id.id === values.template;
						});

						values = values.merge({
							filetype: tpl.target_format || 'odt',
							name: tpl.bibliotheek_sjablonen_id.naam
						});

					};

					fieldReducer = composedReducer({ scope }, ctrl.caseDocuments, ctrl.templates)
						.reduce(( caseDocuments, templates ) => {

							if (!caseDocuments || !templates) {
								return [];
							}

							let form = templateForm({
								caseDocuments: seamlessImmutable(caseDocuments).asMutable( { deep: true }),
								templates: seamlessImmutable(templates).asMutable( { deep: true })
							});

							return form.fields();

						});

					validityReducer = composedReducer( { scope }, ( ) => values, fieldReducer)
						.reduce( ( vals, fields ) => {

							let validity = vormValidator(fields, vals, messages);

							return validity;

						});

					ctrl.values = ( ) => values;

					ctrl.handleChange = ( name, value ) => {
						values = values.merge({ [name]: value });

						if (name === 'template') {

							setTemplateDefaults();

						}
					};

					ctrl.handleSubmit = ( ) => {

						let caseDoc = find(ctrl.caseDocuments(), ( doc ) => {
								return doc.bibliotheek_kenmerken_id.id === values.case_document;
							}),
							caseDocId = caseDoc ? caseDoc.id : null;

						ctrl.onClose({
							$values:
								values.merge( { case_document: caseDocId })
						});

					};

					ctrl.validity = ( ) => get(validityReducer.data(), 'validations');

					ctrl.isValid = ( ) => get(validityReducer.data(), 'valid', false);

					ctrl.getFields = fieldReducer.data;

					scope.$watch(ctrl.templates, ( ) => {

						if (!values.template) {
							
							let tpl = get(first(ctrl.templates()), 'bibliotheek_sjablonen_id.id', '');

							values = values.merge({
								template: tpl
							});
						}

						setTemplateDefaults();

					});

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
