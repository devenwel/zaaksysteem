import angular from 'angular';
import template from './template.html';
import composedReducerModule from './../../../../../../shared/api/resource/composedReducer';
import rwdServiceModule from './../../../../../../shared/util/rwdService';
import observableStateParamsModule from './../../../../../../shared/util/route/observableStateParams';
import zsCasePhaseSidebarTabListModule from './zsCasePhaseSidebarTabList';
import zsCaseCheckListModule from './zsCaseCheckList';
import zsCaseActionListModule from './zsCaseActionList';
import controller from './CasePhaseSidebarController';
import './styles.scss';

export default angular
	.module('zsCasePhaseSidebar', [
		composedReducerModule,
		rwdServiceModule,
		zsCasePhaseSidebarTabListModule,
		zsCaseCheckListModule,
		zsCaseActionListModule,
		observableStateParamsModule
	])
	.component('zsCasePhaseSidebar', {
		bindings: {
			actions: '&',
			checklist: '&',
			tab: '&',
			isCollapsed: '&',
			phaseState: '&',
			onActionAutomaticToggle: '&',
			onActionUntaint: '&',
			onActionTrigger: '&',
			onChecklistAdd: '&',
			onChecklistRemove: '&',
			onChecklistToggle: '&',
			requestor: '&',
			templates: '&',
			caseDocuments: '&',
			phases: '&',
			canEdit: '&'
		},
		controller,
		template
	})
	.name;
