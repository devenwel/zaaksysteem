import angular from 'angular';
import controller from './KeyboardNavigableListController';

export default angular
	.module('shared.ui.zsKeyboardNavigableList', [])
	.directive('zsKeyboardNavigableList', () => ({
		restrict: 'A',
		scope: {
			keyInputDelegate: '&',
			highlightableItems: '&',
			onKeyCommit: '&',
			onKeyHighlight: '&',
			commitOnTab: '&'
		},
		bindToController: true,
		controllerAs: 'zsKeyboardNavigableList',
		controller
	}))
	.name;
