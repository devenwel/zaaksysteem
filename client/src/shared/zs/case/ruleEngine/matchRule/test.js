import matchRule from '.';

describe('matchRule', () => {
	describe('when no conditions are given', () => {
		test('should match', () => {
			expect(matchRule(
				{ conditions: { conditions: [] } }, {}
			)).toBe(true);
		});
	});

	describe('when a attribute\'s value is set, but hidden', () => {
		test('should use null as an attribute value', () => {
			expect(
				matchRule(
					{
						conditions: {
							type: 'and',
							conditions: [
								{
									attribute_name: 'foo',
									values: ['foo']
								}
							]
						}
					},
					{
						foo: 'bar'
					},
					{
						foo: true
					}
				)
			).toBe(false);
		});
	});

	describe('when grouping method is "and"', () => {
		const rule = {
			conditions: {
				conditions: [
					{
						attribute_name: 'foo',
						values: ['foo']
					},
					{
						attribute_name: 'bar',
						values: ['bar']
					}
				],
				type: 'and'
			}
		};

		test('should match when all of the conditions are met', () => {
			expect(
				matchRule(rule, {
					foo: 'foo',
					bar: 'bar'
				})
			).toBe(true);
		});

		test('should not match when one or more conditions are not met', () => {
			expect(
				matchRule(rule, {
					foo: 'foo',
					bar: 'baz'
				})
			).toBe(false);
		});
	});

	describe('when grouping method is "or"', () => {
		const rule = {
			conditions: {
				conditions: [
					{
						attribute_name: 'foo',
						values: ['foo']
					},
					{
						attribute_name: 'bar',
						values: ['bar']
					}
				],
				type: 'or'
			}
		};

		test('should match when one or more conditions are met', () => {
			expect(
				matchRule(rule, {
					foo: 'foo',
					bar: 'bar'
				})
			).toBe(true);

			expect(
				matchRule(
					rule,
					{
						foo: 'oof',
						bar: 'bar'
					}
				)
			).toBe(true);
		});

		test('should not match when no conditions are met', () => {
			expect(
				matchRule(rule, {
					foo: 'bar',
					bar: 'foo'
				})
			).toBe(false);
		});
	});
});
