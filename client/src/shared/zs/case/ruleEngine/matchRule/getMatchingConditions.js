import matchCondition from './matchCondition';

export default
	( conditions, values, hidden ) => {

		return conditions.filter(condition => matchCondition(condition, values || {}, hidden || {}));

	};
