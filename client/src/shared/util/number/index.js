/**
 * @param {string} input
 * @return {number}
 */
export const stringToNumber = input => {
	if (input === '') {
		return NaN;
	}

	return Number(
		input
			.replace(',', '.')
	);
};

/**
 * @param {number} input
 * @return {string}
 */
export const numberToString = input =>
	String(input)
		.replace('.', ',');

/**
 * @param {number|string} input
 * @return {number}
 */
export function toNumber(input) {
	if (typeof input === 'string') {
		return stringToNumber(input);
	}

	return input;
}

/**
 * @param {number} input
 * @return {string}
 */
export const floatToCurrency = input =>
	input
		.toFixed(2)
		.replace('.', ',');

/**
 * @param {number|string} input
 * @return {number}
 */
export const toCurrency = input => (Math.round(toNumber(input) * 100) / 100);
