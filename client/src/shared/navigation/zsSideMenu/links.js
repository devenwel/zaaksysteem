import includes from 'lodash/includes';

export default ( opts ) => {

	let { user, $state, auxiliaryRouteService } = opts;

	let aboutStateName = auxiliaryRouteService.append($state.current, 'about');

	return [
		{
			name: 'user',
			children: [
				{
					name: 'dashboard',
					label: 'Dashboard',
					href: $state.href('home') || '/intern',
					icon: 'home',
					when: includes(user.capabilities, 'dashboard') && includes(user.system_roles, 'Behandelaar')
				},
				{
					name: 'intake',
					label: 'Documentintake',
					href: '/zaak/intake?scope=documents',
					icon: 'file',
					when: includes(user.capabilities, 'documenten_intake_subject') && includes(user.system_roles, 'Behandelaar')
				},
				{
					name: 'search',
					label: 'Uitgebreid zoeken',
					href: '/search',
					icon: 'magnify',
					when: includes(user.capabilities, 'search') && includes(user.system_roles, 'Behandelaar')
				},
				{
					name: 'search_contact',
					label: 'Contacten zoeken',
					href: '/betrokkene/search',
					icon: 'account-search',
					when: includes(user.capabilities, 'contact_search') && includes(user.system_roles, 'Behandelaar')
				}
			]
		},
		{
			name: 'admin',
			children: [
				{
					name: 'library',
					label: 'Catalogus',
					href: '/beheer/bibliotheek',
					icon: 'puzzle',
					when: includes(user.capabilities, 'beheer_zaaktype_admin')
				},
				{
					name: 'users',
					label: 'Gebruikers',
					href: '/medewerker',
					icon: 'account-multiple',
					when: includes(user.capabilities, 'admin')
				},
				{
					name: 'log',
					label: 'Logboek',
					href: '/beheer/logging',
					icon: 'console',
					when: includes(user.capabilities, 'admin')
				},
				{
					name: 'transactions',
					label: 'Transactieoverzicht',
					href: '/beheer/sysin/transactions',
					icon: 'swap-vertical',
					when: includes(user.capabilities, 'admin')
				},
				{
					name: 'interfaces',
					label: 'Koppelingconfiguratie',
					href: '/beheer/sysin/overview',
					icon: 'link',
					when: includes(user.capabilities, 'admin')
				},
				{
					name: 'datastore',
					label: 'Gegevensmagazijn',
					href: '/beheer/object/datastore',
					icon: 'share-variant',
					when: includes(user.capabilities, 'admin')
				},
				{
					name: 'config',
					label: 'Configuratie',
					href: '/beheer/configuration',
					icon: 'settings',
					when: includes(user.capabilities, 'admin')
				}
			]
		},
		{
			name: 'info',
			children: [
				{
					name: 'help',
					label: 'Help',
					href: 'https://help.zaaksysteem.nl',
					icon: 'help-circle',
					when: includes(user.system_roles, 'Behandelaar'),
					external: true
				},
				{
					name: 'info',
					label: 'Over Zaaksysteem.nl',
					href: $state.href(aboutStateName) || '/intern/!over',
					icon: 'information',
					when: includes(user.system_roles, 'Behandelaar')
				}
			]
		}
	];

};
