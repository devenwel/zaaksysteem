const { IS_DEV } = require('./library/constants');

process.env.NODE_ENV = IS_DEV ? 'development' : 'production';

module.exports = require('require-directory')(module, './config/app');
