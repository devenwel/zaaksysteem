#! perl
use lib 't/inc';

use TestSetup;
initialize_test_globals_ok;

use JSON;
use Test::Deep;
use File::Spec::Functions;

BEGIN { use_ok('Zaaksysteem::StUF') };
BEGIN { use_ok('Zaaksysteem::StUF::Stuurgegevens') };

my $VALIDATION_MAP      = {
    'R03'   => {
      'authentiekeWoonplaatsnaam'           => 'Amsterdam',
      'authentiekeIdentificatieWoonplaats'  => '363',
    }
};

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0204', '/r03/101-r03-create.xml'),
    );

    is($stuf->entiteittype, 'R03', 'Found entiteittype R03');

    my $params  = $stuf->as_params;
    note(explain($params));

    for my $key (keys %{ $VALIDATION_MAP->{R03} }) {
        my $givenvalue = $params->{R03}->{'extraElementen'}->{ $key };
        my $wantedvalue = $VALIDATION_MAP->{R03}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }
    my $r03_params = $stuf->get_params_for_r03;

    ### We have to poke this resultset, else the profile wont get loaded
    $schema->resultset('BagWoonplaats');

    my $rv = Data::FormValidator->check(
        $r03_params,
        Params::Profile->get_profile('method' => 'Zaaksysteem::Backend::BagWoonplaats::ResultSet::bag_create_or_update')
    );
    #note(explain($rv));

    ok($rv->success, 'Valid data for BagWoonplaats profile');

}, 'Check R03 native params');

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        catfile(STUF_TEST_XML_PATH, '0204', '/r03/101-r03-create.xml'),
    );

    my $r03_params = $stuf->get_params_for_r03;

    ### We have to poke this resultset, else the profile wont get loaded
    $schema->resultset('BagWoonplaats')->bag_create_or_update(
        $r03_params,
    );

}, 'Import R03 in database');


zs_done_testing;
