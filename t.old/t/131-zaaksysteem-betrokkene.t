#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
### Test header end

use Net::LDAP;
use Zaaksysteem::Betrokkene;

my $basedn          = 'o=zaaksysteem,dc=zaaksysteem,dc=nl';
my $customer_config = {
    start_config => {
        template    => 'zaak_v1',
        customer_id => 100,
        files       => 't/inc/files',
        publish     => 1,
        LDAP        => {basedn => 'o=zaaksysteem,dc=zaaksysteem,dc=nl'}
    },
};

my $config = {
    LDAP => {
        hostname => 'localhost',
        port     => '389',
        admin    => 'cn=admin,dc=zaaksysteem,dc=nl',
        password => 'zaaksysteem123',
    },
};

my $ldap = Net::LDAP->new();

my $model = Zaaksysteem::Betrokkene->new(
    customer => $customer_config,
    config   => $config,
    ldap     => $ldap,
    dbic     => $schema,
);
isa_ok($model, 'Zaaksysteem::Betrokkene');


sub create_natuurlijk_persoon_ok {
    my $create = {
        'np-voornamen'           => 'Anton Ano',
        'np-voorletters'         => 'A.A.',
        'np-geslachtsnaam'       => 'Zaaksysteem',
        'np-huisnummer'          => 42,
        'np-postcode'            => '1011PZ',
        'np-straatnaam'          => 'Muiderstraat',
        'np-woonplaats'          => 'Amsterdam',
        'np-geslachtsaanduiding' => 'M',
    };
    my $np = $model->create('natuurlijk_persoon', $create);
    ok($np, "Natuurlijk persoon is aangemaakt");

    return $np;
}


$zs->zs_transaction_ok(sub {
    my $id = create_natuurlijk_persoon_ok;

    ok $id, "Created natuurlijk_persoon-" . $id;
    # TODO: make it bork when no proper input is passed

    ok !$model->get({}, ""), "Simply returns if the id parameter doesn't have something remotely useful";

    # TODO figure out the scenarios for the type input

    ok $model->get({ type => 'natuurlijk_persoon' }, $id), "Found natuurlijk_persoon-$id";

    ok !$model->get({ type => 'natuurlijk_persoon' }, $id + 1), "Didnt find natuurlijk_persoon-" . ($id + 1);

    my $display_name = $model->get({}, 'betrokkene-natuurlijk_persoon-' .$id)->display_name;
    is $display_name, 'Anton Ano Zaaksysteem', "Display name retrieved properly";
}, "Get betrokkene display_name");



# my $BETROKKENE_TYPES = {
# 'medewerker'            => 'Medewerker',
# 'natuurlijk_persoon'    => 'NatuurlijkPersoon',
# 'org_eenheid'           => 'OrgEenheid',
# 'bedrijf'               => 'Bedrijf',
# };

$zs->zs_transaction_ok(
    sub {
        my $mw = $model->create('medewerker', {});
        ok(!$mw, "Medewerkers worden aangemaakt via LDAP!");
    },
    "Zaaksysteem::Betrokkene::Medewerker"
);

$zs->zs_transaction_ok(
    sub {
        my $create = {
            'np-voornamen'           => 'Anton Ano',
            'np-voorletters'         => 'A.A.',
            'np-geslachtsnaam'       => 'Zaaksysteem',
            'np-huisnummer'          => 42,
            'np-postcode'            => '1011PZ',
            'np-straatnaam'          => 'Muiderstraat',
            'np-woonplaats'          => 'Amsterdam',
            'np-geslachtsaanduiding' => 'M',
        };
        my $np = $model->create('natuurlijk_persoon', $create);
        ok($np, "Natuurlijk persoon is aangemaakt");
    },
    "Zaaksysteem::Betrokkene::NatuurlijkPersoon"
);

zs_done_testing();
