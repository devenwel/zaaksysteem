#! perl
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use_ok('Zaaksysteem::Backend::Sysin::Modules::KCC');


sub create_config_interface {
    my $params = shift || {};

    return $zs->create_named_interface_ok(
        {
            module => 'kcc',
            name   => "KCC koppeling met PBX",
            interface_config => {
                pbx_ip       => '1.2.3.4',
                call_timeout => 123,
                %{ $params },
            },
        }
    );
}

$zs->zs_transaction_ok(sub {
    my $interface = create_config_interface();

    my $transaction = $interface->process_trigger(
        "incoming_call",
        {
            client_ip   => '1.2.3.4',
            phonenumber => '0123456789',
            extension   => '1234',
        }
    );
    isa_ok($transaction, 'Zaaksysteem::Schema::Transaction', '$transaction');

    is(
        $transaction->input_data,
        'Gesprek van 0123456789 naar 1234',
        "->input_data is correct",
    );
    is_deeply(
        $transaction->processor_params,
        {
            client_ip   => '1.2.3.4',
            phonenumber => '0123456789',
            extension   => '1234',
            processor   => '_register_incoming_call',
        },
        '->processor_params are correct',
    );

    my $tr = $transaction->transaction_records->single();
    is($tr->output, 'Undecided', 'Transaction record output is correct');

    {
        my $active = $interface->process_trigger(
            "list_active_calls",
            { extension => '1234' }
        );
        is(@$active, 1, "One active phone call for the correct extension");

        is($active->[0]->id, $transaction->id, "And it's the same transaction");
    }

    {
        my $active = $interface->process_trigger(
            "list_active_calls",
            { extension => '4321' }
        );
        is(@$active, 0, 'No active phone calls for different extension');
    }

    $interface->process_trigger(
        'mark_call',
        {
            mark           => 'accepted',
            transaction_id => $transaction->id,
        },
    );

    # Make sure the next test gets clean data, straight outta Postgres
    $transaction->discard_changes();

    is_deeply(
        $transaction->processor_params,
        {
            client_ip   => '1.2.3.4',
            phonenumber => '0123456789',
            extension   => '1234',
            processor   => '_register_incoming_call',
            result      => 'accepted',
        },
        '->processor_params have been updated correctly',
    );

    $tr->discard_changes();
    is($tr->output, 'accepted', 'Transaction record output is correct');

}, 'Basic incoming phone call + handling');

$zs->zs_transaction_ok(sub {
    my $interface = create_config_interface();

    throws_ok(
        sub {
            $interface->process_trigger(
                "incoming_call",
                {
                    client_ip   => '4.3.2.1',
                    phonenumber => '0123456789',
                    extension   => '1234',
                }
            );
        },
        'Zaaksysteem::Exception::Base',
        'Wrong client IP triggers an exception',
    );

    throws_ok(
        sub {
            $interface->process_trigger(
                "incoming_call",
                {
                    client_ip   => '1.2.3.4',
                    phonenumber => '012345',
                    extension   => '1234',
                }
            );
        },
        'Zaaksysteem::Exception::Base',
        'Invalid phone number triggers an exception',
    );

    throws_ok(
        sub {
            $interface->process_trigger(
                "mark_call",
                {
                    transaction_id => 9999,
                    mark           => 'rejected',
                }
            );
        },
        'Zaaksysteem::Exception::Base',
        'Trying to mark a call that does not exist results in an exception',
    );
}, 'Doing it wrong');

zs_done_testing();
