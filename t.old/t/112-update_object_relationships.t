#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use Pg::hstore;

use Zaaksysteem::Object::Model;
use Zaaksysteem::Search::ZQL;


$zs->zs_transaction_ok(sub {
    my $case1 = $zs->create_case_ok();
    my $case2 = $zs->create_case_ok();
    my $case3 = $zs->create_case_ok();

    $case2->update({pid => $case1->id});
    $case3->update({pid => $case2->id});

    $case1->update_hstore();
    $case2->update_hstore();
    $case3->update_hstore();

    my $od1 = $schema->resultset("ObjectData")->find_or_create_by_object_id(case => $case1->id);
    my $od2 = $schema->resultset("ObjectData")->find_or_create_by_object_id(case => $case2->id);
    my $od3 = $schema->resultset("ObjectData")->find_or_create_by_object_id(case => $case3->id);

    my (@case2_object1_rels) = $od2->object_relationships_object1_uuids;
    my (@case2_object2_rels) = $od2->object_relationships_object2_uuids;

    is(@case2_object1_rels, 1, "One relationship on side '1'");
    is(@case2_object2_rels, 1, "One relationship on side '2'");

    is($case2_object1_rels[0]->type1, 'parent', "type1 of object relationship is 'parent'");
    is($case2_object1_rels[0]->type2, 'child',  "type2 of object relationship is 'child'");
    is($case2_object1_rels[0]->object2_uuid->id, $od3->id, "Linked (other) object in relationship");

    is($case2_object2_rels[0]->type1, 'parent', "type1 of object relationship is 'parent'");
    is($case2_object2_rels[0]->type2, 'child',  "type2 of object relationship 2 is 'child'");
    is($case2_object2_rels[0]->object1_uuid->id, $od1->id, "Linked (other) object in relationship");
}, 'Updating hstore_properties');

$zs->zs_transaction_ok(sub {
    my $case1 = $zs->create_case_ok;
    my $case2 = $zs->create_case_ok;

    $case1->update({ pid => $case2->id });

    $case1->update_hstore;
    $case2->update_hstore;

    my $rels = $schema->resultset('ObjectRelationships');

    is $rels->count, 1, 'Objects have exactly one relationship';

    $schema->resultset('ObjectData')->find({ object_id => $case1->id, object_class => 'case' })->delete;

    is $rels->count, 0, 'Objects have no more relationships after delete of one object';
}, 'Removing object_data rows cascade into relationships');

zs_done_testing();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

