package TestFor::Catalyst::Controller::API::V1::Case::Relation;
use base qw(ZSTest::Catalyst);

use TestSetup;

=head1 NAME

TestFor::Catalyst::Controller:API::V1::Case::Relation - Proves the boundaries of our API: Case/Relation

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/Case/Relation.pm

=head1 DESCRIPTION

These tests prove the interactions between the outside servicebus and our zaaksysteem. This test
uses the version 1 API of zaaksysteem, and proves the C<api/v1/case/UUID/relation> namespace.

=head1 USAGE

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF API.

=head2 Modification

=head3 Add a new relation

    # curl --anyauth -k -H "Content-Type: application/json" --data @file_with_request_json --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/case/32da49c7-d6d9-449e-9de7-18203049f181/relation/add

B<Request>

=begin javascript

{
    "related_id" : "b927dcbc-f43d-4615-ae41-989c746359fc",
}

=end javascript

B<Response>

See /api/v1/case/get

=end javascript

=cut

sub test_v1_case_add_relation : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        my $mech = $zs->mech;
        $mech->zs_login;

        my $case1 = $zs->create_case_ok();
        my $case2 = $zs->create_case_ok();

        ok($case1->object_data->uuid, 'Got case 1 uuid');
        ok($case2->object_data->uuid, 'Got case 2 uuid');

        my $json = $mech->post_json_ok(
            $mech->zs_url_base . '/api/v1/case/' . $case1->object_data->uuid . "/relation/add",
            {
                related_id => $case2->object_data->uuid
            },
        );

        is(
            $json->{result}{instance}{relations}{instance}{rows}[0]{reference},
            $case2->object_data->uuid,
            "Second case ID is returned as a reference",
        );

    });

}

sub test_v1_case_remove_relation : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        my $mech = $zs->mech;
        $mech->zs_login;

        my $case1 = $zs->create_case_ok();
        my $case2 = $zs->create_case_ok();

        ok($case1->object_data->uuid, 'Got case 1 uuid');
        ok($case2->object_data->uuid, 'Got case 2 uuid');

        my $json = $mech->post_json_ok(
            $mech->zs_url_base . '/api/v1/case/' . $case1->object_data->uuid . "/relation/add",
            {
                related_id => $case2->object_data->uuid
            },
        );

        is(
            $json->{result}{instance}{relations}{instance}{rows}[0]{reference},
            $case2->object_data->uuid,
            "Second case ID is returned as a reference",
        );

        $json = $mech->post_json_ok(
            $mech->zs_url_base . '/api/v1/case/' . $case1->object_data->uuid . "/relation/remove",
            {
                related_id => $case2->object_data->uuid
            },
        );

        is(
            scalar @{ $json->{result}{instance}{relations}{instance}{rows} },
            0,
            "Deleting relation works as expected",
        );
    });
}

sub test_v1_case_move_relation : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        my $mech = $zs->mech;
        $mech->zs_login;

        my $case1 = $zs->create_case_ok();
        my $case2 = $zs->create_case_ok();
        my $case3 = $zs->create_case_ok();

        ok($case1->object_data->uuid, 'Got case 1 uuid');
        ok($case2->object_data->uuid, 'Got case 2 uuid');
        ok($case3->object_data->uuid, 'Got case 3 uuid');

        $mech->post_json_ok(
            $mech->zs_url_base . '/api/v1/case/' . $case1->object_data->uuid . "/relation/add",
            {
                related_id => $case2->object_data->uuid
            },
        );
        $mech->post_json_ok(
            $mech->zs_url_base . '/api/v1/case/' . $case1->object_data->uuid . "/relation/add",
            {
                related_id => $case3->object_data->uuid
            },
        );

        my $json = $mech->post_json_ok(
            $mech->zs_url_base . '/api/v1/case/' . $case1->object_data->uuid . "/relation/move",
            {
                related_id => $case2->object_data->uuid
                # no after: put it first.
            },
        );

        # Everything is now in a "known" state: case1 -> [case2, case3]

        is(
            scalar @{ $json->{result}{instance}{relations}{instance}{rows} },
            2,
            "Two relations found",
        );
        is(
            $json->{result}{instance}{relations}{instance}{rows}[0]{reference},
            $case2->object_data->uuid,
            "First relation is correct",
        );
        is(
            $json->{result}{instance}{relations}{instance}{rows}[1]{reference},
            $case3->object_data->uuid,
            "Second relation is correct",
        );

        $json = $mech->post_json_ok(
            $mech->zs_url_base . '/api/v1/case/' . $case1->object_data->uuid . "/relation/move",
            {
                related_id => $case2->object_data->uuid,
                after => $case3->object_data->uuid
            },
        );

        is(
            $json->{result}{instance}{relations}{instance}{rows}[0]{reference},
            $case3->object_data->uuid,
            "After moving: First relation is correct",
        );
        is(
            $json->{result}{instance}{relations}{instance}{rows}[1]{reference},
            $case2->object_data->uuid,
            "After moving: Second relation is correct",
        );
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
