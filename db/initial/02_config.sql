BEGIN;
    INSERT INTO config(parameter, value) VALUES
        ('customer_info_naam',      'Ontwikkelomgeving'),
        ('customer_info_naam_kort', 'Ontwikkelomgeving'),
        ('customer_info_naam_lang', 'Ontwikkelomgeving Zaaksysteem'),
        ('customer_info_adres',     'Donker Curtiusstraat 7 - 521' ),
        ('customer_info_email',     'servicedesk@mintlab.nl'),
        ('customer_info_faxnummer', ''),
        ('customer_info_huisnummer', '7-521'),
        ('customer_info_latitude',   '52.378979'),
        ('customer_info_longitude',  '4.871620'),
        ('customer_info_postcode',   '1051 JL'),
        ('customer_info_straatnaam', 'Donker Curtiusstraat'),
        ('customer_info_website',    'http://www.mintlab.nl/'),
        ('customer_info_woonplaats', 'Amsterdam'),
        ('customer_info_zaak_email', 'no-reply@zaaksysteem.nl'),
        ('customer_info_telefoonnummer', '020 - 737 000 5'),
        ('customer_info_gemeente_id_url', 'http://www.mintlab.nl/'),
        ('customer_info_gemeente_portal', 'http://www.mintlab.nl/');
COMMIT;
