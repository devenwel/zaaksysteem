BEGIN;

ALTER TABLE object_relation ADD COLUMN object_id UUID REFERENCES object_data(uuid);

COMMIT;
