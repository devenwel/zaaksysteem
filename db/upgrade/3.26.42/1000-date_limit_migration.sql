BEGIN;

-- Contents of this field have been improved and migrated to properties in 3.26.0:
ALTER TABLE zaaktype_kenmerken DROP COLUMN date_fromcurrentdate;

COMMIT;
