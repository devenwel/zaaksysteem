version: '3.6'
services:
    backend:
        build:
            context: "."
            dockerfile: "docker/Dockerfile.backend"
        depends_on:
            - "smtpd"
            - "virusscanner"
            - "redis"
            - "database"
            - "converter"
            - "message-queue"
        networks:
            - "default"
        environment:
            - "CATALYST_DEBUG=1"
            - "CLAMAV_SERVER=clamav"
            - "CONNECTION_PER_INSTANCE_LIMIT=10"
            - "DOCUMENT_CONVERTER_URL=http://converter:5032"
            - "VIRUS_SCAN_SERVICE_HOST=virusscanner:5000"
            - "VIRUS_SCAN_SERVICE_TEMPLATE=http://{+virus_scan_service_host}/filestore"
            - "PM_MAX_REQUESTS=10"
            - "SMTP_PORT=2525"
            - "SMTP_SERVER=smtpd"
            - "ZAAKSYSTEEM_HOME=/opt/zaaksysteem"
            - "ZS_DISABLE_STUF_PRELOAD=1"
            - "ZS_NO_XSRF_TOKEN_CHECK=1"
            - "ZS_NO_CLIENT_CERT_CHECK=1"
            - "ZS_NUM_THREADS=3"
            - "ZS_JSON_PRETTY=1"
            - "DBIC_TRACE=0"
            - "MAX_PROCESS_SIZE=15%"
        expose:
            - "9083"
        user: "root"
        command:
            - "/opt/zaaksysteem/script/zaaksysteem_dev_server.sh"
        volumes:
            - "./db/testbase:/opt/testbase"
            - "./bin:/opt/zaaksysteem/bin"
            - "./dev-bin:/opt/zaaksysteem/dev-bin"
            - "./etc:/etc/zaaksysteem"
            - "./lib:/opt/zaaksysteem/lib"
            - "./root:/opt/zaaksysteem/root"
            - "./script:/opt/zaaksysteem/script"
            - "./share:/opt/zaaksysteem/share"
            - "./t:/opt/zaaksysteem/t"
            - "./xt:/opt/zaaksysteem/xt"
            - "filestore:/opt/filestore"
        logging:
            driver: "json-file"
            options:
                max-size: "200k"
                max-file: "10"
    api2csv:
        build:
            context: "."
            dockerfile: "docker/Dockerfile.api2csv"
        expose:
            - "1030"
        networks:
            - "default"
        volumes:
            - "./etc:/etc/zaaksysteem"
    frontend:
        build:
            context: "."
            dockerfile: "docker/Dockerfile.frontend"
            target: "development"
        ports:
            - "443:443"
        depends_on:
            - "backend"
            - "api2csv"
            - "swaggerui"
        networks:
            default:
                aliases:
                    - "dev.zaaksysteem.nl"
                    - "testbase.dev.zaaksysteem.nl"
                    - "testbase-instance.dev.zaaksysteem.nl"
        environment:
            - "RUN_BUILD_HELPERS=1"
            - "CONNECTION_PER_INSTANCE_LIMIT=10"
        volumes:
            - "./dev-bin:/opt/zaaksysteem/dev-bin"
            - "./frontend/zaaksysteem:/opt/zaaksysteem/frontend/zaaksysteem"
            - "./frontend/zaaksysteem/angular-index.js:/opt/zaaksysteem/frontend/zaaksysteem/angular-index.js"
            - "./frontend/zaaksysteem/gulpfile.js:/opt/zaaksysteem/frontend/zaaksysteem/gulpfile.js"
            - "./frontend/zaaksysteem/index.js:/opt/zaaksysteem/frontend/zaaksysteem/index.js"
            - "./frontend/zaaksysteem/webpack.config.js:/opt/zaaksysteem/frontend/zaaksysteem/webpack.config.js"
            - "./client/src:/opt/zaaksysteem/client/src"
            - "./client/test:/opt/zaaksysteem/client/test"
            - "./client/webpack:/opt/zaaksysteem/client/webpack"
            - "./client/package.json:/opt/zaaksysteem/client/package.json"
            - "./client/jest.config.js:/opt/zaaksysteem/client/jest.config.js"
            - "./root/images:/opt/zaaksysteem/root/images"
            - "./share/apidocs:/opt/zaaksysteem/apidocs"
            - "certificates:/etc/nginx/ssl"
            - "filestore:/opt/filestore"
    clamav:
        image: "quay.io/ukhomeofficedigital/clamav:latest"
        networks:
            - "default"
        volumes:
            - "./etc/clamav/clamd.conf:/usr/local/etc/clamd.conf"
    virusscanner:
        image: "registry.gitlab.com/zaaksysteem/zaaksysteem-virus_scanner-service:latest"
        environment:
            - "CLAMAV_SERVER=clamav"
        expose:
            - "5000"
        depends_on:
            - "clamav"
        networks:
            - "default"
        volumes:
            - "./etc/virus_scanner-service.conf:/etc/zaaksysteem-virus_scanner-service/zaaksysteem-virus_scanner-service.conf"
            - "certificates:/etc/nginx/ssl"
    message-queue:
        image: "rabbitmq:3-management-alpine"
        expose:
            - "5672"
        ports:
            - "15672:15672"
        networks:
            - "default"
        volumes:
            - "rabbitmq-data:/var/lib/rabbitmq"
    database:
        image: "postgres:9.6"
        ports:
            - "5432:5432"
        networks:
            - "default"
        environment:
            - "POSTGRES_USER=zaaksysteem"
            - "POSTGRES_PASSWORD=zaaksysteem123"
            - "POSTGRES_DB=zaaksysteem"
        volumes:
            - "dbdata:/var/lib/postgresql/data"
            - "./tmp:/var/tmp"
            - "./db:/opt/zaaksysteem/db"
            - "./db/initial:/docker-entrypoint-initdb.d"
            - "./db/template.sql:/docker-entrypoint-initdb.d/00_zaaksysteem.sql"
    syzygy-index:
        image: "docker.elastic.co/elasticsearch/elasticsearch-oss:6.2.2"
        expose:
            - "9200"
        ports:
            - "9201:9200"
        networks:
            - "default"
        environment:
            - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
            - cluster.name=syzygy-index
            - bootstrap.memory_lock=true
        ulimits:
            memlock:
                soft: -1
                hard: -1
        volumes:
            - syzygy-index-data:/usr/share/elasticsearch/data
    redis:
        image: "redis:3"
        expose:
            - "6379"
        networks:
            - "default"
    queue-runner:
        image: "registry.gitlab.com/zaaksysteem/queue-runner:latest"
        environment:
            - "REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca.crt"
        depends_on:
            - "frontend"
            - "message-queue"
        networks:
            - "default"
        volumes:
            - "certificates:/etc/ssl/certs"
        command:
            - "/opt/queue-runner/startup.sh"
            - "message-queue"
            - "5672"
            - "amqp://message-queue:5672/%2F"
            - "--debug"
    converter:
        image: "registry.gitlab.com/zaaksysteem/converter-service:latest"
        expose:
            - "5032"
    smtpd:
        build:
            context: "."
            dockerfile: "docker/Dockerfile.smtpd"
        environment:
            - "SMTPD_PORT=2525"
            - "SMTPD_MAILDIR=/var/mail/zaaksysteem"
        expose:
            - "2525"
        networks:
            - "default"
        volumes:
            - "email:/var/mail"
    imapd:
        build:
            context: "."
            dockerfile: "docker/Dockerfile.imapd"
        expose:
            - "143"
        ports:
            - "1143:143"
        networks:
            - "default"
        volumes:
            - "email:/var/mail"
    swaggerui:
        image: "swaggerapi/swagger-ui"
        networks:
            - "default"
        environment:
            - "BASE_URL=/apidocs"
            - "API_URL=/doc/api/v1/swagger/index.yaml"
volumes:
    dbdata:
    rabbitmq-data:
    filestore:
    certificates:
    email:
    syzygy-index-data:
networks:
    default:
