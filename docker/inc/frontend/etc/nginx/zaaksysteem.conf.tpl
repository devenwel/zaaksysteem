rewrite ^/tpl/zaak_v1/nl_NL/css/.*?common.css$ /page/minified/common/css/common.css last;
rewrite ^/html/(.*)$                           /html/nl/$1                          last;

### Backend
proxy_set_header X-Forwarded-For $remote_addr;
proxy_set_header Host $host;
proxy_set_header X-Real-IP $remote_addr;

# Allow client bodies ("file uploads") to be slightly over 2GB (so a 2GB file
# and some metadata can be uploaded at once)
client_max_body_size        2050M;

# Some generous buffers for request headers
large_client_header_buffers 4 512k;

resolver DNS_RESOLVER;

# { START Security-related headers
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy

    add_header X-Frame-Options        sameorigin    always;
    add_header X-Content-Type-Options nosniff       always;
    add_header Referrer-Policy        strict-origin always;
# } STOP Security-related headers

# { START backend

    location / {
        include fastcgi_params;
        fastcgi_param SCRIPT_NAME /;
        fastcgi_param HTTPS $fe_https;

        fastcgi_connect_timeout 60;
        fastcgi_send_timeout 3600;
        fastcgi_read_timeout 3600;
        fastcgi_buffer_size 128k;
        fastcgi_buffers 4 256k;
        fastcgi_busy_buffers_size 256k;
        fastcgi_temp_file_write_size 256k;
        fastcgi_intercept_errors on;

        ### Limit connections
        limit_conn_log_level notice;
        limit_conn_status 429;
        limit_conn perserver CONNECTION_PER_INSTANCE_LIMIT;

        set $proxyhost ZAAKSYSTEEM_HOST_API;
        fastcgi_pass $proxyhost:9083;
    }

    # This is a "UStore" download URL
    # See Zaaksysteem::Filestore::Engine::UStore for documentation
    location /download/ustore {
        internal;
        alias /opt/filestore;
    }
    
    # This is a "Swift" download URL
    # See Zaaksysteem::Filestore::Engine::Swift for documentation
    location ~ ^/download/swift/([A-Za-z0-9_]+)/(http|https)/([A-Za-z0-9\.\-]+)/([1-9][0-9]+)/(.*) {
        internal;
    
        proxy_set_header X-Auth-Token $1;

        # Original:
        #   proxy_pass $2://$3:$4/$5;
        #
        # But we like to force HTTPS:
        proxy_pass https://$3:443/$5;

        proxy_hide_header Content-Type;
        proxy_hide_header Accept-Ranges;
    }

# } END backend

# { START apps

    # API2CSV NodeJS App
    location /app/api2csv {
        set $proxyhost ZAAKSYSTEEM_HOST_CSV;
        proxy_pass http://$proxyhost:1030;
    }

    rewrite ^/apidocs$ /apidocs/ permanent;
    location /apidocs/ {
        set $proxyhost ZAAKSYSTEEM_HOST_SWAGGER;
        proxy_pass http://$proxyhost:8080;
    }

    location /doc/api {
        alias /opt/zaaksysteem/apidocs/;
    }

# } END apps

# { START internal locations

    location ~ ^/(?<app>intern|mor|wordaddin|pdc|vergadering) {
        root /opt/zaaksysteem/root/assets;
        try_files $uri $uri/ /$app/index.html;

        error_page 404 =200 /$app/index.html;
    }

    # Exclude static files from running through the backend.
    location ~ ^/(assets|css|data\/|error_pages|examples|favicon\.ico|flexpaper|fonts|html|images|js|offline|partials|pdf\.js-with-viewer|robots.txt|tpl|webodf) {
        root /opt/zaaksysteem/root;
    }

# } STOP internal locations

