# Zaaksysteem.nl

This is the main code repository for the Zaaksysteem framework.

For more information, visit our [website](http://www.zaaksysteem.nl/).

## Short technical introduction

All development for Zaaksysteem follows the following procedure when it comes
to getting the code in this codebase. Since this is open source software, you
are free to fork, modify, redistribute and open pull-requests, under the
limitations of the
[EUPL license](http://joinup.ec.europa.eu/software/page/eupl).

## Branches

* master
    The master branch contains the latest *stable* and released version of
    Zaaksysteem. This is the version most people want.

* quarterly
    The quarterly branch has a common ancestor at the current master
    branch, and contains all new features, bugfixes and other
    modifications done in a two week sprint This branch is considered
    *stable*, as it has been tested internally, but it has not seen a
    production environment yet.

* sprint
    This branch contains active and ongoing development modifications, it is
    *unstable* as it may contain modifications that have not been tested fully.
    It is part of our bi-weekly sprint cycle and will be reset at every
    sprint.

## Contributing

You are free to submit pull-requests for improvements to Zaaksysteem. Please
target those requests to our most recent sprint branch, and describe
liberally what the change does, why it does so, and what you believe the impact
will be.

When adding code, please try to make sure your editor understands the
[`.editorconfig`](http://editorconfig.org) found in the root of the repository.

# Running the development environment

Zaaksysteem uses [docker](http://www.docker.com/) to manage development
environments that are the same for every developer. To create a new
environment, you first need to install `docker` and `docker-compose`.
For docker please follow the installation instructions as found on the
[docker documenation page](https://docs.docker.com/engine/installation/).

Docker is very disk consuming, make sure you have sufficient space
somewhere for docker to use. One can tweak the default `/var/lib/docker`
to be elsewhere. On Debian/Ubuntu edit `/etc/default/docker` and add the
`-g` option: `DOCKER_OPTS="-g /path/to/free/space"`.  On Fedora/CentOS
one should edit `/etc/sysconfig/docker`, eg `other_args="-g
/path/to/free/space"`. For more information see the [docker forums]
(https://forums.docker.com/t/how-do-i-change-the-docker-image-installation-directory/1169).

You can reclaim disk space from old (unused) container images using:

`docker image prune`

You can (re)create all the containers by running this:
```
# Only rebuild frontend container
./dev-bin/docker-maintenance frontend

# Only rebuild frontend and backend container
./dev-bin/docker-maintenance frontend backend

# Rebuild all the containers
./dev-bin/docker-maintenance
```

## macOS

For macOS it is advised that you configure docker to to have at least 8Gb
of memory. Otherwise you may end up with build errors.

## Generate configuration files
When you have completed the instalation you need to create some
configuration files by running `dev-bin/generate_config.sh`.

## Start your docker

You can now start your development environment by running:

```
$ docker-compose up
```

This will build and start all the relevant containers.

You can connect to the development environment on https://dev.zaaksysteem.nl/
now.

## SSL certificates

After building the frontend container, you can extract the generated
development CA certificate from it using:

```
$ ./dev-bin/get_ca_certificate.sh > ca.crt
```

By importing that certificate on your system, all connections to
`dev.zaaksysteem.nl` will be trusted (there won't be certificate warnings to
ignore anymore).

## Override the docker-compose.yml file

In case you want to override certain `docker-compose.yml` entries but you
don't want to check them you can make use of the
`docker-compose.overide.yml` file, you can find a working example in
`docker-compose.overide.example`.

After editting this file you need to recreate the containers:
```
docker-compose rm -s -f <container>

docker-compose up --no-start <container>
docker-compose start <container>

# Or just
docker-compose up -d <container>
```

## Front-end development

When starting frontend development you will need the
`docker-compose.override.example` as your source of the your
`docker-compose.override.yml`. It defines the last stage of the build:
`npm-development`. See `docker/Dockerfile.frontend` for more
information.

### docker.override.yml
```
frontend:
  build:
    target: "npm-development"
```

### Run frontend run!

```
# Setup frontend container for development
# Done via npm-development build, but MacOS users may want this
# Linux devs may skip this part
./dev-bin/run-frontend.sh init

# Once you have a running container and want to start your day
./dev-bin/run-frontend.sh dev

# Clean up the development container
./dev-bin/run-frontend.sh clean
# or just
docker-compose rm -f -s frontend
docker-compose up frontend
```


## Backend development

Building a new Perl docker layer can be done by running:
`dev-bin/docker-perl`.

The base image will only be build if the checksum of the `cpanfile` and
`docker/Dockerfile.perl` does not match the reference in the
`docker/Dockerfile.backend`.

It is advised to pin versions of external modules in case you are using
git repositories. This is because the docker build otherwise uses the
cache. You could also force a non-cached build.

Pushing the new layer is done like so (this can only be done by Mintlab
developers): `dev-bin/docker-perl -p`

## Commit hooks

Feel free to use the following commit hooks:

```
./dev-bin/tests.hook
```

This hook allows for two environment variables: `ZS_PROVE` and `ZS_NPM`.
The former is to run the perl testsuite and the later is to run the JS
linter + testsuite. When no environment variable is found both suites
will be run. Running the backend testsuite requires a running instance
of redis and the backend container. The frontend testsuite requires a
running frontend container: `docker-compose start redis backend
frontend`.

You can put the hooks in `.git/hooks/pre-commit` or in
`.git/hooks/pre-push` depending on your own preferences.

## End-to-end testing (E2E)

Before starting with tests, make sure that docker, node and npm are installed. Then make sure that docker and webdriver are running:

```
$ docker-compose up
$ client/npm run e2e-server
```

When running the tests for the very first time, webdriver should be updated:

```
$ client/npm run e2e-update
```

To facilitate end-to-end testing, we maintain a database image in a secondary
repository. We've created a script to manage loading and dumping that test
database, and to manage the external repository (clone, commit, etc.) for test
runs:

```
$ dev-bin/testbase.sh
```

Running the script will show an explanation of the different options. Before
running tests it is recommended to load the latest database version into the
testbase-instance:

```
$ dev-bin/testbase.sh --pull
$ dev-bin/testbase.sh --load
$ dev-bin/testbase.sh --create
```

Please update your webdriver before hand:

```
$ client/npm run e2e-update
```

There are multiple ways to run tests.

Run all the tests.

```
$ client/npm run e2e-test
```

Optionally the tests can be restricted to those inside a specific folder and/or file.

```
$ client/npm run e2e-test --folder=folderName --file=fileNamed
```

Run testsuites. The names of the configured suites can be found in the config file.

```
$ client/npm run e2e-suite suiteName
```

Run sequences of tests in parallel. The test sequences can be found in the config file.
When the number of sessions is not defined it will run all sequences in parallel.
When the number of sessions is defined to be less than the sequences, it will simply
iterate to a next sequence upon finishing one.

```
$ client/npm run e2e-multi --e2esessions=3
```

## Connect with your database

To connect to the database. you can you can run the PostgreSQL client in the
database container:

```
$ docker-compose exec database psql -U zaaksysteem
```

The "db/" directory in the source is available as "/opt/zaaksysteem/db" in the
container.

It's also possible to connect to port 5432 on localhost, using a tool like
PgAdmin. The username is "zaaksysteem" and the password "zaaksysteem123"

## Contained email

Some parts of Zaaksysteem send email. As long as the backend is not configured
to use an external mail server (this is the default case), all email messages
are sent to a special mailbox using a small SMTP daemon. This mailbox is
available using IMAP:

* Protocol: IMAP
* Username: zaaksysteem
* Password: zaaksysteem
* Host: localhost
* Port: 1143

If you have the "mutt" email program installed, you can connect like this:

```
$ mutt -f imap://zaaksysteem:zaaksysteem@localhost:1143/
```

### Support

We only support the community version of the software through the
[wiki](http://wiki.zaaksysteem.nl/). For professional support, please contact
[Mintlab](http://www.zaaksysteem.nl/).

-- 
The [Mintlab](http://www.mintlab.nl/) Team
