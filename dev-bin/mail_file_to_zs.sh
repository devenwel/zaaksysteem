#!/usr/bin/env bash

log_error() {
    local msg="$@"
    echo $msg >&2
    exit 1;

}

if [ -z "$1" ]
then
    log_error "Usage: $(basename $0) mail.mime [ hostname ]"
fi

mime="$1"
if [ ! -f "$mime" ]
then
    log_error "file '$mime' does not exist!"
fi

if [ -n "$2" ]
then
    hostname=$2
else
    hostname=10.44.0.11
fi

echo "Sending mail '$mime' to '$hostname'"
./bin/mail-gate --url https://$hostname/api/mail/intake < $mime

if [ $? -ne 0 ]
then
    log_error "Failure sending mail"
fi

echo "Done";
