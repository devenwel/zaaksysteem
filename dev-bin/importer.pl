#!/usr/bin/env perl
use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Try::Tiny;
use Time::HiRes qw(gettimeofday tv_interval);
use Zaaksysteem::CLI;
use File::Basename;
use Zaaksysteem::Object::Import;

my $cli = Zaaksysteem::CLI->init;

$cli->do_transaction(
    sub {
        my ($self, $schema) = @_;

        my $path = $cli->options->{file};
        my ($name, undef, $suffix) = fileparse($path, '\.[^\.]*');

        my $file = $self->schema->resultset('File')->file_create(
            {
                name      => $name . $suffix,
                file_path => $path,
                db_params => { created_by => "ZS::CLI" },
            }
        );

        my $type = lc($cli->options->{type}||'');
        my $object_type;

        if ($type eq 'faq') {
            $object_type = 'qa';
        }
        elsif ($type eq 'product') {
            $object_type = 'product';
        }
        else {
            die "Unknown type $type";
        }

        my $provider = lc($cli->options->{provider} ||'');
        if ($provider eq 'sdu') {
            # ok
        }
        elsif ($provider eq 'sim' || $provider eq 'kluwer') {
            $provider = 'sim';
        }
        else {
            die "Unknown provider $provider";
        }

        my $filestore = $file->filestore_id;
        my $importer = Zaaksysteem::Object::Import->new(
            schema      => $schema,
            filestore   => $filestore,
            file_format => join('::', ucfirst($provider), ucfirst($type)),
            object_type => $object_type,
        );
        $importer->run();
    }
);

1;

__END__

=head1 NAME

importer.pl - An object importer for products and questions

=head1 SYNOPSIS

importer.pl OPTIONS -o type=faq|product -o provider=sdu|sim|kluwer -o file=foo.xml

=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * customer_d

The customer_d dir, defaults to the relative directory where zaaksysteem.conf is found.

=item * hostname

The hostname you want to touch cases for

=item * o

You can use this specify an option.

=back

=over 8

=item * type

The type of import you want to do, choose 'product' for products and services or 'faq' for questions and answers.

=item * provider

The import provider, choose 'sdu' for SDU imports and Kluwer imports you can either choose 'sim' or 'kluwer'.

=item * file

The file you want to import.

=back

=over

=item * no-dry

Run it and commit the changes. The default is NOT to commit any changes.

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
