
choice=$1

usage() {
    local rc=$1

    [ -z "$rc" ] && rc=0

    echo "Please supply a command: reset, dev or init" >&2;
    exit $rc

}
case $choice in
    init)
        docker-compose exec -T frontend \
            /opt/zaaksysteem/dev-bin/npm_container.sh
        ;;
    dev)
        docker-compose exec -T frontend \
            /opt/zaaksysteem/dev-bin/run-npm-start.sh
        ;;
    reset)
        docker-compose exec -T frontend \
            /opt/zaaksysteem/dev-bin/reset-frontend.sh
        ;;
    *) usage 1;;

esac




