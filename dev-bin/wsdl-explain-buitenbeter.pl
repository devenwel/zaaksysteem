#!/usr/bin/env perl

# generate example code for calling a SOAP service

use XML::Compile::WSDL11;
use XML::Compile::SOAP11;

use XML::Compile::Transport::SOAPHTTP;

my $wsdl = XML::Compile::WSDL11->new('../share/wsdl/buitenbeter/buitenbeter.wsdl');

my $dir = '../share/wsdl/buitenbeter/';
my $namespace = {
    0 => 'http://tempuri.org/',
    1 => 'http://schemas.microsoft.com/2003/10/Serialization/',
    2 => 'http://schemas.datacontract.org/2004/07/BuitenBeter.Interfaces.EMPS.Objects'
};
    for (qw/0 1 2/) {
        $wsdl->importDefinitions(
            $dir .'MeldingService-xsd' . $_ .'.svc',
            target_namespace     => $namespace->{$_},
            element_form_default => 'qualified',
        );
    }

# importDefinitions

$wsdl->compileCalls(port => 'BasicHttpBinding_IMeldingService');

use Data::Dumper;
print Dumper [map { $_->name } $wsdl->operations];

for my $op ($wsdl->operations) {
print "IN @{[$op->name]}\n";
print $wsdl->explain($op->name, PERL => 'INPUT', recurse => 1, port => 'BasicHttpBinding_IMeldingService');
print "OUT @{[$op->name]}\n";
print $wsdl->explain($op->name, PERL => 'OUTPUT', recurse => 1, port => 'BasicHttpBinding_IMeldingService');
print "---------------------------------\n";
}



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

