export const mouseOut = () => {
    browser
        .actions()
        .mouseMove($('body'), { x: -1, y: -1 })
        .perform();
};

export const mouseOver = element => {
    browser
        .actions()
        .mouseMove(element, { x: 0, y: 0 })
        .perform();

    browser
        .actions()
        .mouseMove(element, { x: 1, y: 1 })
        .perform();
};

export const mouseOverCreateButton = () => {
    mouseOver($('.top-bar-create-case'));
};

export const scroll = ( x, y ) => {
    browser.executeScript(`window.scrollTo(${x},${y});`);
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
