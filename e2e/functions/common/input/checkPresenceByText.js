export default ( textToLookUp, elementCss ) =>
    $$(elementCss)
    	.filter(name =>
        	name
        		.getText()
        		.then(text =>
            		text === textToLookUp
        		)
    	)
    	.then(filteredElements =>
        	filteredElements.length > 0
    	);

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
