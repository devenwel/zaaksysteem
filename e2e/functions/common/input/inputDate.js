export default ( attribute, date ) => {
    const dateToInput = date ? date : '13-01-2010';
    const dateSplit = dateToInput.split('-');
    const dayToInput = dateSplit[0];
    const monthToInput = dateSplit[1];
    const yearToInput = dateSplit[2];

    attribute.$('input').sendKeys(monthToInput);
    attribute.$('input').sendKeys(dayToInput);
    attribute.$('input').sendKeys(yearToInput);
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
