import {
    selectFirstSuggestion
} from './../../common/select';
import {
    inputAttributes
} from './../../common/input/caseAttribute';
import waitForElement from './../../common/waitForElement';

export const advanceButton = $('.phase-advance-button');

export const advance = () => {
    advanceButton.click();
};

export const isAdvancePossible = () =>
    advanceButton
        .getAttribute('disabled')
        .then(state =>
            !state
        );

export const getAdvanceButtonText = () =>
    $('.phase-advance-button-label')
        .getText()
        .then(text =>
            text.toLowerCase()
    );

export const getLockButtonText = () =>
    $('.phase-header-status button.phase-unlock-button')
        .getText()
        .then(text =>
            text.toLowerCase()
    );

export const checkObjectButtonPresence = objectTypeName =>
    $$(`[data-name="object.${objectTypeName}"] .case-mutation-add-item button`)
        .map(button =>
            button
                .getText()
                .then(text =>
                    text.toLowerCase()
                )
        );

export const startObjectMutation = ( objectTypeName, type ) => {
    const allButtons = $$(`[data-name="object.${objectTypeName}"] button`);

    allButtons
        .each(button =>
            button
                .getText()
                .then(text => {
                    if ( text.toLowerCase() === type ) {
                        button.click();
                    }
            })
        );
};

export const performObjectMutationAction = type => {
    const cancel = $$('zs-case-object-mutation-form .form-actions button:nth-child(1)');
    const addMutation = $$('zs-case-object-mutation-form .form-actions button:nth-child(2)');

    if ( type === 'cancel' ) {
        cancel.click();
    } else if ( type === 'add' ) {
        addMutation.click();
    }
};

export const createObjectMutationCreate = ( objectTypeName, type, data ) => {
    startObjectMutation(objectTypeName, type);
    inputAttributes(data);
    performObjectMutationAction('add');

};

export const createObjectMutationMutate = ( objectTypeName, type, objectName, data ) => {
    startObjectMutation(objectTypeName, type);
    selectFirstSuggestion($('zs-case-object-mutation-form').$('vorm-field input'), objectName);
    waitForElement('zs-case-object-mutation-form [data-name="objectmutatie_titel"]');
    inputAttributes(data);
    performObjectMutationAction('add');
};

export const createObjectMutationDelete = ( objectTypeName, type, objectName ) => {
    startObjectMutation(objectTypeName, type);
    selectFirstSuggestion($('zs-case-object-mutation-form').$('vorm-field input'), objectName);
    performObjectMutationAction('add');
};

export const editObjectMutation = ( objectTypeName, number, data ) => {
    $(`[data-name="object.${objectTypeName}"] .case-mutation:nth-child(${number}) button.edit`).click();
    inputAttributes(data);
    performObjectMutationAction('add');
};

export const deleteObjectMutation = ( objectTypeName, number ) => {
    $(`[data-name="object.${objectTypeName}"] .case-mutation:nth-child(${number}) button.delete`).click();
    $('.confirm-button').click();
};

export const unlock = () => {
    $('.phase-header-status button.phase-unlock-button').click();
    $('.confirm-button').click();
};

export const lock = () => {
    $('.phase-header-status button.phase-unlock-button').click();
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
