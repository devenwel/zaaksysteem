import {
    openPageAs
} from './../../../functions/common/navigate';
import {
    mouseOverCreateButton
} from './../../../functions/common/mouse';

const checkPlusMenuOptions = expectedOptions => {
    const plusMenuOptions = [
        'zaak',
        'contact',
        'contact-moment',
        'create_widget',
        'upload',
        'sjabloon',
        'betrokkene',
        'email',
        'geplande-zaak'
    ];

    plusMenuOptions.forEach(plusMenuOption => {
        const expected = expectedOptions.includes(plusMenuOption);
        const not = expected ? '' : 'not ';

        it(`the plusMenu should ${not}have the "${plusMenuOption}" option`, () => {
            expect($(`zs-contextual-action-menu .popup-menu-list [data-name="${plusMenuOption}"]`).isPresent()).toBe(expected);
        });
    });
};
const testDate = [
    {
        description: 'when on the dashboard as contactmanager',
        user: 'contactbeheerder',
        expectedOptions: ['zaak', 'contact', 'contact-moment', 'create_widget']
    },
    {
        description: 'when on the dashboard as employee without special system roles',
        user: 'plusknop',
        expectedOptions: ['zaak', 'contact-moment', 'create_widget']
    },
    {
        description: 'when in a new case without assignee with edit rights',
        user: 'plusknop',
        caseNumber: 105,
        expectedOptions: ['zaak', 'contact-moment', 'upload']
    },
    {
        description: 'when in a new case with assignee with edit rights',
        user: 'plusknop',
        caseNumber: 106,
        expectedOptions: ['zaak', 'contact-moment', 'upload', 'sjabloon', 'betrokkene', 'email', 'geplande-zaak']
    },
    {
        description: 'when in a new case without assignee with manage rights',
        user: 'plusknop',
        caseNumber: 107,
        expectedOptions: ['zaak', 'contact-moment', 'upload', 'sjabloon', 'betrokkene', 'email', 'geplande-zaak']
    },
    {
        description: 'when in an open case without template',
        user: 'plusknop',
        caseNumber: 108,
        expectedOptions: ['zaak', 'contact-moment', 'upload', 'betrokkene', 'email', 'geplande-zaak']
    },
    {
        description: 'when in a closed case',
        user: 'plusknop',
        caseNumber: 109,
        expectedOptions: ['zaak', 'contact-moment']
    }
];

testDate.forEach(thing => {
    const { description, user, caseNumber, expectedOptions } = thing;

    describe(description, () => {
        beforeAll(() => {
            openPageAs(user, caseNumber);
            mouseOverCreateButton();
        });

        checkPlusMenuOptions(expectedOptions);
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
