package Zaaksysteem::CLI::Bedrijf::Searchable;
use Moose;

extends 'Zaaksysteem::CLI';

use BTTW::Tools;
use DateTime;

around run => sub {
    my $orig = shift;
    my $self = shift;

    $self->$orig(@_);

    my $found = $self->schema->resultset('Bedrijf')->search(undef, { order_by => { '-asc' => 'deleted_on' }});
    while (my $bedrijf = $found->next) {
        $self->do_transaction(
            sub {
                my ($self, $schema) = @_;
                $self->log->info(
                    sprintf(
                        "Updating search terms for NNP %s with kvknummer %d",
                        $bedrijf->id, $bedrijf->dossiernummer
                    )
                );
                $bedrijf->update();
            }
        );
    }
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::CLI::Bedrijf::Searchable - Fix search terms for deleted companies

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
