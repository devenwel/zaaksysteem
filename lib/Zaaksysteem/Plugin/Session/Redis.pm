package Zaaksysteem::Plugin::Session::Redis;
use Moose;
use MooseX::NonMoose;
use namespace::autoclean;

extends qw/Catalyst::Plugin::Session::Store/;

=head1 NAME

Zaaksysteem::Plugin::Session::Redis - Redis session store using Sereal

=head1 DESCRIPTION

Catalyst session store plugin, for storing session data in Redis, serialized
using Sereal.

=cut

use Sereal::Encoder qw(SRL_SNAPPY);
use Sereal::Decoder;
use MIME::Base64 qw(encode_base64 decode_base64);
use Redis;
use Try::Tiny;

has _redis_connection => (
    is => 'rw',
    isa => 'Redis',
);

=head2 get_session_data

Retrieve session data from Redis and decode it.

=cut

sub get_session_data {
    my ($c, $key) = @_;

    $c->_ensure_redis_connection;

    if(my ($sid) = $key =~ /^expires:(.*)/) {
        $c->log->trace("Getting 'expires:' for session '$sid'");
        return $c->_redis_connection->get($key);
    } else {
        $c->log->trace("Getting session data: '$key'");

        my $data = $c->_redis_connection->get($key);
        if(defined($data)) {
            my $sereal = Sereal::Decoder->new();

            return try {
                $sereal->decode( decode_base64($data) )
            } catch {
                $c->log->error("Could not retrieve session data for '$key': '$_'");
                return;
            }
        }
    }

    return;
}

=head2 store_session_data

Save session data by encoding it using Sereal and storing in Redis.

=cut


sub store_session_data {
    my ($c, $key, $data) = @_;

    $c->_ensure_redis_connection;

    if(my ($sid) = $key =~ /^expires:(.*)/) {
        $c->log->trace("Setting expires key for $sid: $data");
        $c->_redis_connection->set($key, $data);
    } else {
        $c->log->trace("Setting session: '$key'");

        my $sereal = Sereal::Encoder->new({
            compress => SRL_SNAPPY,
            croak_on_bless => 0,
            dedupe_strings => 1,
        });

        $c->_redis_connection->set($key, encode_base64($sereal->encode($data)));

        my $json = JSON::XS->new->convert_blessed->allow_blessed->utf8;

        $c->_redis_connection->set("json:$key", encode_base64($json->encode($data)));
    }

    my $exp = $c->session_expires;
    $c->_redis_connection->expireat($key, $exp);
    $c->_redis_connection->expireat("json:$key", $exp);

    return;
}

=head2 delete_session_data

Delete a specific session from Redis.

=cut

sub delete_session_data {
    my ($c, $key) = @_;

    $c->_ensure_redis_connection;

    $c->log->trace("Deleting session: '$key'");
    $c->_redis_connection->del($key);
    $c->_redis_connection->del("json:$key");

    return;
}

=head2 delete_session_data

No-op, Redis does its own expiry.

=cut

sub delete_expired_sessions {
    # Redis does its own expiry
}

sub setup_session {
    my ($c) = @_;

    $c->maybe::next::method(@_);
}

=head2 _ensure_redis_connection

Ensures a Redis connection is available and ready for use.

=cut

sub _ensure_redis_connection {
    my ($c) = @_;

    my $cfg = $c->_session_plugin_config;

    try {
        $c->_redis_connection->ping;
    } catch {
        my $redis = Redis->new(
            server    => $cfg->{redis_server}    || '127.0.0.1:6379',
            debug     => $cfg->{redis_debug}     || 0,
            reconnect => $cfg->{redis_reconnect} || 0
        );

        $c->_redis_connection($redis);

        if ($c->_redis_connection && $cfg->{redis_db}) {
            $c->_redis_connection->select($cfg->{redis_db});
        }
    };
}

__PACKAGE__->meta->make_immutable;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
