package Zaaksysteem::SBUS::ResultSet::GenericImport;

use Moose;

extends 'DBIx::Class::ResultSet';

use Zaaksysteem::Constants;
use Zaaksysteem::SBUS::Constants;
use Zaaksysteem::SBUS::Logging::Object;
use Zaaksysteem::SBUS::Logging::Objecten;

use Data::Dumper;

use constant IMPORT_KERNGEGEVENS   => [qw/
    burgerservicenummer
    a_nummer
/];

use constant IMPORT_KERNGEGEVEN_LABEL   => 'burgerservicenummer';


sub import_entries {
    my $self        = shift;
    my $records     = shift;

    die('Records not ARRAYREF')
        unless UNIVERSAL::isa($records, 'ARRAY');

    my $logging = Zaaksysteem::SBUS::Logging::Objecten->new();

    for my $record (@{ $records }) {
        unless (
            UNIVERSAL::isa($record, 'HASH') ||
            !exists($record->{options}) ||
            !exists($record->{params})
        ) {
            die('Record not HASHREF or does not contain options and params');
        }

        my $result = $self->import_entry(
            $record->{params},
            $record->{options}
        );

        $logging->object($result);
    }

    return $logging;
}

sub import_entry {
    my $self        = shift;
    my $opts        = shift;
    my $raw_params  = $opts->{create};
    my $options     = $opts->{options};
    my $log         = $opts->{log};

    my $params;

    Params::Profile->register_profile(
        method  => 'import_entry',
        profile => $self->_import_entry_profile,
    );

    ### VALIDATION
    my $dv;
    {
        $dv = Params::Profile->check(
            params  => $raw_params,
        );

        $params = $dv->valid;

        ### XXX KERNGEGEVENS CHECK
    }

    ### Check mutatieType (create/delete/edit)
    $options->{mutatie_type} = $self->_check_mutatie_type( $params, $options );

    ### Die when mutatietype is T and not all options
    ### are given
    die(
        'Z::Import::GBA: invalid parameters:'
        . Dumper($dv)
    ) unless (
        (
            defined($options->{mutatie_type}) &&
            $options->{mutatie_type} ne 'T'
        ) || $dv->success
    );

    unless ($options->{logobject}) {
        $options->{logobject} = Zaaksysteem::SBUS::Logging::Object->new();
    }

    my $label   = $self->_import_kerngegeven_label;

    $options->{logobject}->mutatie_type(uc($options->{mutatie_type}));
    $options->{logobject}->object_type($self->_import_objecttype);
    $options->{logobject}->label($params->{ $label });
    $options->{logobject}->params($params);

    $log->info(
        'Import ' . $self->_import_objecttype
        . ' entry: ' . $params->{ $label }
    );

    ### Update or create entry

    eval {
        $self->result_source->schema->txn_do(sub {
            $self->_import_real_entry($params, $options);

            if (uc($options->{mutatie_type}) eq 'V') {
                $self->_delete_real_entry($params, $options);
            }
        });
    };

    if ($@) {
        $options->{logobject}->error('Import error: ' . $@);
        warn('IMPORT ERROR: ' . $@);
    }

    return $options->{logobject};
}


sub _get_kern_record {
    my ($self, $params, $options) = @_;

    my $IMPORT_KERNGEGEVENS = $self->_import_kerngegevens;

    my $search  = {
        map { $_ => $params->{ $_ } } @{ $IMPORT_KERNGEGEVENS }
    };

    ### Check against database
    my $records = $self->search($search);

    return $records->first;
}

sub _detect_changes {
    my ($self, $params, $options, $record) = @_;

    for my $param (keys %{ $params }) {
        my ($old, $new) = ('','');
        $old = $record->$param if $record && $record->$param;
        $new = $params->{$param} if $params->{$param};

        if ($old ne $new) {
            $options->{logobject}->change({
                column  => $param,
                old     => $old,
                new     => $new,
            });
        }
    }
}


sub _check_mutatie_type {
    my ($self, $params, $options) = @_;

    ### Verwijderd?
    if (
        (
            defined($params->{datum_overlijden}) &&
            $params->{datum_overlijden}
        ) ||
        (
            defined($options->{verhuisd}) ||
            $options->{verhuisd}
        ) ||
        (
            defined($options->{mutatie_type}) &&
            uc($options->{mutatie_type}) eq 'V'
        )
    ) {
        return $options->{mutatie_type}    = 'V';
    }

    ### Toevoegen of wijzigen?
    my $record = $self->_get_kern_record(
        $params,
        $options
    );

    if ($record) {
        return $options->{mutatie_type}    = 'W';
    }

    $options->{mutatie_type}        = 'T';

    return $options->{mutatie_type};
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 IMPORT_KERNGEGEVENS

TODO: Fix the POD

=cut

=head2 IMPORT_KERNGEGEVEN_LABEL

TODO: Fix the POD

=cut

=head2 import_entries

TODO: Fix the POD

=cut

=head2 import_entry

TODO: Fix the POD

=cut

