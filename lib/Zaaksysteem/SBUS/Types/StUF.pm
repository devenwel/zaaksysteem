package Zaaksysteem::SBUS::Types::StUF;

use strict;
use warnings;

use Zaaksysteem::Constants;
use Zaaksysteem::SBUS::Constants;

use Zaaksysteem::SBUS::Types::StUF::XML;

use XML::Twig;

use Params::Profile;
use Data::Dumper;

use XML::Tidy;

use FindBin qw/$Bin/;

use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::SBUS';

#sub sectormodellen {
#    my $self    = shift;
#
#    return $self->{_sectormodellen}
#        if $self->{_sectormodellen};
#
#    $self->{_sectormodellen} = {
#        'BG' => 1
#    };
#
#    return $self->{_sectormodellen};
#}
#
#sub _is_valid_sectormodel {
#    my ($self, $search, $opt) = @_;
#
#    die('STuF: Geen sectormodel gegeven') unless $opt->{'sectormodel'};
#    die('STuF: Onjuist sectormodel: ' . $opt->{'sectormodel'})
#        unless $self->sectormodellen->{uc($opt->{'sectormodel'})}
#}

use constant STUF_RESPONSES => {
    'bevestiging'   => '{http://www.egem.nl/StUF/StUF0204}bevestigingsBericht',
    'fout'          => '{http://www.egem.nl/StUF/StUF0204}foutBericht'
};

sub compile_response {
    my ($self, $params)     = @_;

    my ($key)               = keys %{ $params->{response} };

    my $response            = {
        STUF_RESPONSES->{ $key } =>
            $params->{response}->{ $key }
    };

    my $compiler        = Zaaksysteem::SBUS::Types::StUF::XML->new(
        perlin          => $response,
        home            => $self->config->{home}
    );

    my $xml             = $compiler->transform_to_xml;

    return $xml;
}

sub _prepare_input_params {
    my ($self, $params)     = @_;

    #my $stufxml             = $params->{input};

    ### Alter XML into PerlStructure
    if ($params->{input_raw}) {
        my $compiler        = Zaaksysteem::SBUS::Types::StUF::XML->new(
            xmlin   => $params->{input_raw},
            home    => $self->config->{home}
        );

        $params->{input}    = $compiler->transform_to_perl;

        $params->{input}    = $params->{input}->{content} if $params->{input};

        $params->{object}   = $params->{input}->{stuurgegevens}->{entiteittype};
    }

    return $params;
}


sub _generate_stuf_definition {
    my $self            = shift;
    my $type            = shift;
    my $search          = shift;
    my $opt             = shift;

    my $sbus_options    = $self->config;

    my $def     = {
        'stuurgegevens'     => {
            'berichtsoort'      => STUF_BERICHTSOORTEN->{ $type }->{berichtsoort},
            'zender'            => {
                'applicatie'        => $sbus_options->{default_zender},
            },
            'ontvanger'         => {
                'applicatie'        => $sbus_options->{default_ontvanger},
            },
        },
    };

    $def->{ stuurgegevens }->{ $_ } = $opt->{ $_ } for qw/
        entiteittype
        sectormodel
        versieStUF
        versieSectormodel
        referentienummer
        tijdstipBericht
    /;


    return $def;
}

sub _generate_question {
    my $self    = shift;
    my $search  = shift;
    my $opt     = shift;
    my $rv      = {};

    die('No entiteittype given, no idea how to define a question')
        unless $opt->{entiteittype};

    $rv->{ $opt->{entiteittype} } = [
        $search,
        $search
    ];

    return $rv;
}

sub kennisgeving {
    my $self            = shift;
    my ($search, $opt)  = @_;
    my ($def);

    die('Only possible with afnemerIndiciatie') unless
        defined($_[1]->{afnemerIndicatie});


    $def     = $self->_generate_stuf_definition(
        'kennisgeving',
        @_
    );

    $def->{'stuurgegevens'}->{'kennisgeving'} = {
        'mutatiesoort'      => ($_[1]->{afnemerIndicatie} ? 'T' : 'V'),
        'indicatorOvername' => 'I',
        'tijdstipMutatie'   =>  DateTime->now()->strftime('%Y%m%d%H%M%S00'),
    };

    $def->{body} = {
        'PRS'   => {
            soortEntiteit           => 'F',
            sleutelGegevensbeheer   => $search->{sleutelGegevensbeheer},
            verwerkingssoort        => 'I',
        }
    };

    return $def;
}

sub search {
    my $self            = shift;
    my ($search, $opt)  = @_;
    my ($def);

    if (
        defined($_[1]->{afnemerIndicatie})
    ) {
        $def     = $self->_generate_stuf_definition('afnemerSearch', @_);
    } else {
        $def     = $self->_generate_stuf_definition('search', @_);
    }

    $def->{'stuurgegevens'}->{'vraag'} = {
        'maximumAantal'     => 25
    };

    $def->{body} = {};

    ### Vraag
    my $question    = $self->_generate_question($search, $opt);

    my $columns     = {
        $opt->{entiteittype} => $self->xml_structure->{
            $opt->{entiteittype}
        }
    };


    my $body = $question;
    while (my ($column, $value) = each %{ $columns }) {
        if ($question->{$column}) {
            push (@{ $body->{$column} }, $value);
            next;
        }

        $body->{$column} = $value;
    }

    $def->{body}    = $body;

    return $def;
}

sub generate_return {
    return {
       'bevestiging'    => {
           'stuurgegevens'  => {
                'berichtsoort'      => 'Bv01',
                'referentienummer'  => '2322232323',
                'tijdstipBericht'   => '2009040717084815',
                'entiteittype'      => 'PRS',
                'zender'            => {
                    'applicatie'        => 'zaaksysteem.nl',
                    'organisatie'       => 'Baarn'
                },
                'ontvanger'         => {
                    'applicatie'        => 'ONBEKEND'
                },
                'kennisgeving'      => {
                    'indicatorOvername' => 'V',
                    'mutatiesoort'      => 'T'
                },
                'sectormodel'       => 'BG',
                'versieSectormodel' => '0204',
                'versieStUF'        => '0204',
            }
        }
    };
}

sub generate_error {
    my $self            = shift;
    my $options         = shift;
    my $request_params  = shift;
    my $error_params    = shift;

    my $stufxml         = $request_params->{input};

    my $sbus_options    = $self->config;

    my $stuurgegevens   = {};
    if ($stufxml) {
        $stuurgegevens  = $stufxml->{stuurgegevens};
    } else {
        die('Big OOPS: Invalid XML Received') unless
            $request_params->{input_raw};

        my ($mutatiesoort)    = $request_params->{input_raw}
            =~ /mutatiesoort>(.*?)</;

        $stuurgegevens  = {
            'zender'            => {
                'applicatie'        => $sbus_options->{default_zender},
            },
            'ontvanger'         => {
                'applicatie'        => $sbus_options->{default_ontvanger},
            },
            'kennisgeving'      => {
                mutatiesoort    => $mutatiesoort,
            },
        };

        ($stuurgegevens->{ $_ }) = $request_params->{input_raw}
            =~ /$_>(.*?)</ for
                qw/
                entiteittype
                sectormodel
                versieSectormodel
                versieStUF
            /;

    }

    $stuurgegevens->{referentienummer}    = $options->{traffic_object}->id;
    $stuurgegevens->{tijdstipBericht}     = $options->{traffic_object}
            ->created->strftime('%Y%m%d%H%M%S00');

    $stuurgegevens->{berichtsoort}        = 'Fo01';
    $stuurgegevens->{crossRefNummer}      =
        $stuurgegevens->{referentienummer};

    return {
        'fout' => {
            stuurgegevens   => $stuurgegevens,
            body            => {
                code            => 'StUF011',
                plek            => 'server',
                omschrijving    => 'Cannot parse request message: '
                    . 'see zaaksysteem for more information'
            }
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 STUF_BERICHTSOORTEN

TODO: Fix the POD

=cut

=head2 STUF_RESPONSES

TODO: Fix the POD

=cut

=head2 compile_response

TODO: Fix the POD

=cut

=head2 generate_error

TODO: Fix the POD

=cut

=head2 generate_return

TODO: Fix the POD

=cut

=head2 kennisgeving

TODO: Fix the POD

=cut

=head2 search

TODO: Fix the POD

=cut

