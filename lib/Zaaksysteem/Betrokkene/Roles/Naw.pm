package Zaaksysteem::Betrokkene::Roles::Naw;
use Moose::Role;

=head1 NAME

Zaaksysteem::Betrokkene::Roles::Naw - A role for betrokkene address functions

=head1 METHODS

=head2 get_volledig_huisnummer_from_adres

Make the house number human readable

=cut


sub get_volledig_huisnummer_from_adres {
    my ($self, $adres) = @_;

    return '' if (!$adres);

    my $huisnummer = $adres->huisnummer // '';
    if ($adres->huisletter) {
        $huisnummer .= " " . $adres->huisletter;
    }
    if ($adres->huisnummertoevoeging) {
        $huisnummer .= " - " . $adres->huisnummertoevoeging;
    }
    return $huisnummer;
}

sub get_volledig_huisnummer_from_bedrijf {
    my ($self, $type) = @_;

    my $prefix;
    if ($type eq 'residence') {
        my $huisnummer = $self->vestiging_huisnummer // '';
        if ($self->vestiging_huisletter) {
            $huisnummer .= " " . $self->vestiging_huisletter;
        }
        if ($self->vestiging_huisnummertoevoeging) {
            $huisnummer .= " - " . $self->vestiging_huisnummertoevoeging;
        }
        return $huisnummer;
    }
    else {
        my $huisnummer = $self->correspondentie_huisnummer // '';
        if ($self->correspondentie_huisletter) {
            $huisnummer .= " " . $self->correspondentie_huisletter;
        }
        if ($self->correspondentie_huisnummertoevoeging) {
            $huisnummer .= " - " . $self->correspondentie_huisnummertoevoeging;
        }
        return $huisnummer;
    }
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
