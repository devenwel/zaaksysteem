package Zaaksysteem::View::JSON;
use Moose;

use JSON::MaybeXS ();
use BTTW::Tools;

BEGIN { extends 'Catalyst::View::JSON'; }

has 'url_navigation' => (
    is => 'rw',
    isa => 'HashRef[Maybe[Str]]',
    lazy => 1,
    clearer => '_clear_nav',
    default => sub {
        { next => undef, prev => undef };
    }
);

has additional_changes => (
    is        => 'rw',
    isa       => 'ArrayRef[Str]',
    lazy      => 1,
    default   => sub { return [] },
);

=head2 process

Extends the default process sub to extract some pager information

This featurette was created to inject into JSON responses for ResultSets
or other paged data URLs that the frontend can directly use to navigate
the resource. Should probably be extended further (with stuff like
C<latest_results_url> for the timeline, or perhaps refresh URLs for datasets
that are prone to change while being viewed).

=cut

around 'process' => sub {
    my $orig = shift;
    my $self = shift;
    my $c = shift;

    # Extract the required navigation URLs and store them as local state
    # > yeah, maybe not such a great idea...
    # > also implicitly relies on the value under *_url is an URI object.
    if($c->stash->{ next_url }) {
        $self->url_navigation->{ next } = $c->stash->{ next_url }->as_string;
    }

    if($c->stash->{ prev_url }) {
        $self->url_navigation->{ prev } = $c->stash->{ prev_url }->as_string;
    }

    if ($c->stash->{additional_changes}) {
        $self->additional_changes($c->stash->{additional_changes});
    }

    if (not exists $c->stash->{json} or not defined $c->stash->{json}) {
        $c->log->error("No JSON data on stash, but JSON view used.");

        for my $error (@{ $c->error }) {
            $c->log->info("Error on stack: $error");
        }
    }

    my $retval = try {
        $self->$orig($c, @_);
    } catch {
        my $error = sprintf(
            'Exception during JSON view processing: "%s"',
            $_
        );

        $c->log->error($error);
        throw('view/json/process_failure', $error);
    };

    # This little gem is a workaround that fixes a bug where the View object
    # holds on to links from a previous request. Naive implementation, View
    # objects are persistant across requests. TODO: Remove the custom state,
    # keep a reference to $c->stash->{ *_url } so that encode_json can pick
    # up on it.
    $self->_clear_nav;

    if ($c->stash->{ json_content_type }) {
        $c->res->headers->header(
            'Content-Type' => $c->stash->{ json_content_type }
        );
    }

    # http://docs.angularjs.org/api/ng.$http @ JSON Vulnerability Protection
    #$c->res->body(sprintf(")]}',\n%s", $c->res->body));

    return $retval;
};

=head2 encode_json

Extends the default encode_json to add some default wrapping for DBIx::Class.
objects.

=cut

sub encode_json {
    my($self, $c, $data) = @_;

    if ($data && ref($data) eq 'HASH' && exists $data->{json}) {
        $data = $data->{json};
    }

    if(blessed($data) && $data->isa('DBIx::Class::ResultSet')) {
        $data = $self->prepare_json_resultset($data);
    }
    # Handle single DBIx::Class rows
    elsif (blessed($data) && $data->isa('DBIx::Class::Row')) {
        $data = $self->prepare_json_row($data);
    }
    # Check if arrays contain DB-rows. Does not check the entire array. Mixing
    # rows with other stuff is rather silly.
    elsif (ref($data) eq 'ARRAY') {
        $data = $self->prepare_json_rows($data);
    }

    my $encoder = JSON::MaybeXS->new(
        utf8            => 1,
        pretty          => 1,
        allow_nonref    => 1,
        allow_blessed   => 1,
        convert_blessed => 1,
    );

    return $encoder->encode($data);
}

=head2 prepare_json_resultset

Function to prepare DBIx::Class ResultSets for JSON.

=cut

sub prepare_json_resultset {
    my ($self, $rs) = @_;

    my @all = $rs->all;
    return {
        at       => undef,
        rows     => undef,
        num_rows => scalar @all,
        result   => \@all,
        next     => $self->url_navigation->{ next },
        prev     => $self->url_navigation->{ prev },
        additional_changes => $self->additional_changes,
    }

}

=head2 prepare_json_row

Function to prepare a single DBIx::Class Row for JSON.

=cut

sub prepare_json_row {
    my ($self, $row) = @_;

    return {
        at       => undef,
        rows     => undef,
        num_rows => 1,
        result   => [$row],
        next     => $self->url_navigation->{ next },
        prev     => $self->url_navigation->{ prev },
        comment  => 'This is a single DBIx::Class::Row, it has no paging.',
        additional_changes => $self->additional_changes,
    }
}

=head2 prepare_json_rows

Function to prepare DBIx::Class Rows for JSON.

=cut

sub prepare_json_rows {
    my ($self, @rows) = @_;

    return {
        at       => undef,
        rows     => undef,
        num_rows => scalar @rows,
        result   => @rows,
        next     => $self->url_navigation->{ next },
        prev     => $self->url_navigation->{ prev },
        comment  => 'This is not a DBIx::Class::ResultSet and does NOT support paging.',
        additional_changes => $self->additional_changes,
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
