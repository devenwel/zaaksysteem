package Zaaksysteem::Backend::Roles::NatuurlijkPersoon::ObjectSubscription;
use Moose::Role;

use BTTW::Tools;

define_profile add_object_subscription => (
    required => {
        external_id  => 'Str',
        interface_id => 'Int',
    },
    optional => {
        authenticatedby => 'Str',
    },
    defaults => {
        authenticatedby => 'gba',
    }

);

sub add_object_subscription {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    my %args = (
    );

    my $subscription = $self->subscription_id;

    if ($subscription && $subscription->get_column('interface_id') ne $params->{interface_id}) {
        $subscription = undef;
    }

    if ($subscription && $subscription->external_id ne $params->{external_id}) {
        die "Subscription ID mismatch";
    }

    my $rs = $self->result_source->schema->resultset('ObjectSubscription');

    if (!$subscription) {
        $subscription = $rs->create(
            {
                local_table  => 'NatuurlijkPersoon',
                local_id     => $self->id,
                external_id  => $params->{external_id},
                interface_id => $params->{interface_id},
            }
        );
    }

    $self->update(
        {
            authenticated   => 1,
            authenticatedby => $params->{authenticatedby},
        }
    );
    $subscription->discard_changes;
    $self->discard_changes;

    return $subscription;
}


1;

__END__

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Roles::ObjectSubscription - Add an object subscription to something

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

