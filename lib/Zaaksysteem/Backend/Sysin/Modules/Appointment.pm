package Zaaksysteem::Backend::Sysin::Modules::Appointment;
use Moose;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
/;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Appointment - Interface with appointment providers

=head1 DESCRIPTION

An interface module that handles all of the different "appointment management systems"
that a L<Zaaksysteem::AppointmentProvider> plugin is available for.

=cut

use List::Util qw(first);
use Module::Pluggable::Object;
use Zaaksysteem::Constants::Users qw(:all);
use Zaaksysteem::Object::Types::Sysin::RemoteCallResponse;
use BTTW::Tools;
use Zaaksysteem::ZAPI::Form::Field;

my @PROVIDERS = Module::Pluggable::Object->new(
    search_path => 'Zaaksysteem::AppointmentProvider',
    except      => qr/^Zaaksysteem::AppointmentProvider::Roles::.*/,
    require     => 1
)->plugins;

my $INTERFACE_ID = 'appointment';

my $INTERFACE_CONFIG_FIELDS = [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_plugin',
        type        => 'select',
        label       => 'Afspraak-applicatie',
        required    => 1,
        description => 'Selecteer hier welke afspraak-applicatie gebruikt moet worden.',
        data => {
            options => [
                sort {
                    $a->{label} cmp $b->{label}
                } map {
                    +{
                        value => $_->shortname,
                        label => $_->label,
                    };
                } @PROVIDERS
            ],
        },
    ),
    (
        # Gather together the configuration items for each available
        # appointment provider plugin.
        map {
            my @config_items = $_->configuration_items;

            # Only show these config items if the plugin they're from is selected.
            for my $ci (@config_items) {
                if ($ci->when) {
                    $ci->when(sprintf('interface_plugin == "%s" && (%s)', $_->shortname, $ci->when));
                } else {
                    $ci->when(sprintf('interface_plugin == "%s"', $_->shortname));
                }
            }

            @config_items;
        } @PROVIDERS
    ),
];

my $INTERFACE_DESCRIPTION = <<'EOD';
<p>
    Hier kunt u de koppeling tussen Zaaksysteem en een van de ondersteunde
    afspraken-applicaties instellen.
</p>
<p>
    Voor meer informatie kunt u terecht op de
    <a href="http://wiki.zaaksysteem.nl/wiki/Redirect_koppelprofiel_afspraken">
        Zaaksysteem Wiki
    </a>
</p>
EOD

my $MODULE_SETTINGS = {
    name             => $INTERFACE_ID,
    interface_config => $INTERFACE_CONFIG_FIELDS,
    description      => $INTERFACE_DESCRIPTION,

    label            => 'Afspraken',
    direction        => 'outgoing',
    manual_type      => [],
    is_multiple      => 1,
    is_manual        => 0,
    has_attributes   => 0,
    retry_on_error   => 0,
    is_casetype_interface         => 0,
    allow_multiple_configurations => 1,

    trigger_definition            => {
        get_product_list => {
            method => 'get_product_list',
            api    => 1,
            api_is => 'ro',
            api_allowed_users => REGULAR,
        },
        get_location_list => {
            method => 'get_location_list',
            api    => 1,
            api_is => 'ro',
            api_allowed_users => REGULAR,
        },
        get_dates => {
            method => 'get_dates',
            api    => 1,
            api_is => 'ro',
            api_allowed_users => REGULAR,
        },
        get_timeslots => {
            method => 'get_timeslots',
            api    => 1,
            api_is => 'ro',
            api_allowed_users => REGULAR,
        },
        book_appointment => {
            method => 'book_appointment',
            api    => 1,
            api_is => 'rw',
            api_allowed_users => REGULAR,
        },
        delete_appointment => {
            method => 'delete_appointment',
            api    => 1,
            api_is => 'rw',
            api_allowed_users => REGULAR,
        },
    },

    test_interface  => 1,
    test_definition => {
        description => qq{
            Om te controleren of de applicatie goed geconfigureerd is, kunt u
            hieronder een aantal tests uitvoeren. Hiermee controleert u de
            verbinding met uw afspraken-applicatie.
        },
        tests => [
            {
                id => 1,
                label => "Test verbinding met server",
                name => "plugin_test",
                method => "plugin_test",
                description => "Test verbinding met de server van de afspraken-applicatie.",
            }
        ],
    },
};

=head1 METHODS

=head2 BUILDARGS

Wrapper to inject the module settings into newly-created instances.

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %$MODULE_SETTINGS );
};

=head2 _get_model

Create a new instance of the configured L<Zaaksysteem::AppointmentProvider>
plugin and return it.

=cut

sub _get_model {
    my ($self, $opts) = @_;

    my $schema = $opts->{schema};

    my $config = $opts->{interface}->get_interface_config;
    my $type = $config->{plugin};

    my $plugin = first { $type eq $_->shortname } @PROVIDERS;

    my @file_config_fields =
        map  { ($_->name =~ /^interface_(.*)$/) }
        grep { $_->type eq 'file' }
        @{ $self->interface_config };

    for my $file_config_field (@file_config_fields) {
        if (defined $config->{ $file_config_field } && @{ $config->{ $file_config_field } } > 0) {
            # Replace "file" configuration fields with the actual file it points to
            $config->{ $file_config_field } = $schema->resultset('Filestore')->find(
                $config->{ $file_config_field }[0]{id}
            )->get_path;
        }
    }

    return $plugin->new(%$config);
}

=head2 get_location_list

Retrieve a list of "locations" from the provider.

=cut

sub get_location_list {
    my ($self, $params, $interface) = @_;

    my $data = $interface->model->get_location_list();

    return Zaaksysteem::Object::Types::Sysin::RemoteCallResponse->new(
        call => 'get_location_list',
        data => $data
    );
}

=head2 get_product_list

Retrieve a list of "products" ("appointment types") from the provider, for a given
location.

=cut

sub get_product_list {
    my ($self, $params, $interface) = @_;

    my $location_id = $params->{request_params}{location_id};

    my $data = $interface->model->get_product_list($location_id);

    return Zaaksysteem::Object::Types::Sysin::RemoteCallResponse->new(
        call => 'get_product_list',
        data => $data
    );
}

=head2 get_dates

Retrieve a list of possible dates for appointments.

=head3 Arguments

=over

=item * product_id

Product for which to get a list of dates.

=item * location_id

Location for which to get a list of dates.

=back

=cut

sub get_dates {
    my ($self, $params, $interface) = @_;

    my $product_id  = $params->{request_params}{product_id};
    my $location_id = $params->{request_params}{location_id};

    my $data = $interface->model->get_dates($product_id, $location_id);

    return Zaaksysteem::Object::Types::Sysin::RemoteCallResponse->new(
        call => 'get_dates',
        data => $data
    );
}

=head2 get_timeslots

Retrieve a list of open appointment slots for a given date.

=head3 Arguments

=over

=item * date

Date for which to retrieve a list of available time slots.

=item * product_id

Product for which to get a list of time slots.

=item * location_id

Location for which to get a list of time slots.

=back

=cut

sub get_timeslots {
    my ($self, $params, $interface) = @_;
    
    my $date     = DateTime::Format::ISO8601->parse_datetime($params->{request_params}{date});
    my $product  = $params->{request_params}{product_id};
    my $location = $params->{request_params}{location_id};

    my $data = $interface->model->get_timeslots($date, $product, $location);

    return Zaaksysteem::Object::Types::Sysin::RemoteCallResponse->new(
        call => 'get_timeslots',
        data => $data
    );
}

=head2 book_appointment

Create a new appointment in the configured appointment system, using the
specified appointment details.

=cut

sub book_appointment {
    my ($self, $params, $interface) = @_;

    throw(
        'appointment/no_requestor',
        "Required requestor object not specified",
    ) unless $params->{request_params}{references}{requestor};

    my $appointment_data = $params->{request_params}{appointment_data};

    $appointment_data->{start_time} = DateTime::Format::ISO8601->parse_datetime(
        $appointment_data->{start_time}
    );

    $appointment_data->{end_time} = DateTime::Format::ISO8601->parse_datetime(
        $appointment_data->{end_time}
    );

    my $appointment = $interface->model->book_appointment(
        $appointment_data,
        $params->{request_params}{references}{requestor},
    );

    my $entities = $params->{ request_params }{ security_entities } || [];

    $appointment->permit($_, qw[read write]) for @{ $entities };

    return $params->{object_model}->save(object => $appointment);
}

=head2 delete_appointment

Delete an existing appointment from the configured appointment system.
The appointment to delete is specified by the appointment details.

=cut

sub delete_appointment {
    my ($self, $params, $interface) = @_;

    my $ref = $params->{object_model}->inflate(hash => $params->{request_params}{appointment});
    my $appointment = $params->{object_model}->get(ref => $ref);

    my $data = $interface->model->delete_appointment($appointment);

    $params->{object_model}->delete(object => $appointment);

    return Zaaksysteem::Object::Types::Sysin::RemoteCallResponse->new(
        call => 'delete_appointment',
        data => $data,
    );
}

=head2 plugin_test

Call the "test_config" method on the configured plugin, to check if it's set up
correctly.

=cut

sub plugin_test {
    my ($self, $interface) = @_;

    $interface->model->test_connection();

    return;
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
