package Zaaksysteem::SAML2;
use Moose;

use v5.24;

=head1 NAME

Zaaksysteem::SAML2 - SAML2 implementation of Zaaksysteem

=head1 SYNOPSIS

    my $saml = Zaaksysteem::SAML2->new();


=head1 DESCRIPTION

=cut

use BTTW::Tools;

use Crypt::PK::RSA;
use Crypt::Cipher::AES;
use Crypt::Mode::CBC;
use JSON;
use MIME::Base64;
use Moose::Util::TypeConstraints;
use URI;
use URI::QueryParam;
use XML::XPath;
use XML::LibXML;

use Zaaksysteem::Constants ':SAML_TYPES';
use Zaaksysteem::Types qw(NonEmptyStr SAMLNameIDFormat);
use Zaaksysteem::SAML2::SP;
use Zaaksysteem::SAML2::IdP;
use Zaaksysteem::SAML2::Binding::SOAP;
use Zaaksysteem::SAML2::Binding::Redirect;
use Zaaksysteem::SAML2::Protocol::ArtifactResolve;
use Zaaksysteem::SAML2::Protocol::AuthnRequest;
use Zaaksysteem::SAML2::Protocol::Assertion;
use Zaaksysteem::SAML2::Protocol::LogoutRequest;
use Zaaksysteem::SAML2::Protocol::LogoutResponse;

with 'MooseX::Log::Log4perl';

class_type('Zaaksysteem::SAML2::IdP');
class_type('Zaaksysteem::SAML2::SP');
class_type('Zaaksysteem::SAML2::Spoof');

use constant SAML_STATUS_REQUESTER => 'urn:oasis:names:tc:SAML:2.0:status:Requester';
use constant SAML_STATUS_SUCCESS   => 'urn:oasis:names:tc:SAML:2.0:status:Success';

has context_id => (
    is  => 'rw',
    isa => 'Str',
);

has sp => (
    is  => 'ro',
    isa => 'Zaaksysteem::SAML2::SP|Zaaksysteem::SAML2::Spoof',
);

has idp => (
    is  => 'ro',
    isa => 'Zaaksysteem::SAML2::IdP|Zaaksysteem::SAML2::Spoof',
);

has uri => (is => 'ro');

has authenticated_identifier => (
    is      => 'rw',
    isa     => 'HashRef',
);

has authenticated_assertion => (
    is      => 'rw',
    isa     => 'Net::SAML2::Protocol::Assertion',
);

=head1 CONSTRUCTORS

=head2 new_from_interfaces

Builds a new SAML2 instance with information derived from one or two
L<Zaaksysteem::Backend::Sysin::Interface> definitions. Supplying the
C<sp> parameter is optional, as it can be found using the IdP provided
in C<idp>.

=cut

define_profile new_from_interfaces => (
    required => [qw[idp]],
    optional => [qw[sp]],
    typed => {
        sp => 'Zaaksysteem::Model::DB::Interface',
        idp => 'Zaaksysteem::Model::DB::Interface'
    }
);

sub new_from_interfaces {
    my ($class, %params) = @_;
    my $opts  = assert_profile(\%params)->valid;

    if($opts->{idp}->jpath('$.saml_type') eq 'spoof') {
        require Zaaksysteem::SAML2::Spoof;
        my $sp  = Zaaksysteem::SAML2::Spoof->new_from_interface(interface => $opts->{ sp });
        my $idp = Zaaksysteem::SAML2::Spoof->new_from_interface(interface => $opts->{ idp });

        my $idp_entity_id = $opts->{ idp }->jpath('$.idp_entity_id');
        $sp->id($idp_entity_id) if $idp_entity_id;
        return $class->new(sp => $sp, idp => $idp);
    }

    unless($opts->{ sp }) {
        my $schema = $opts->{ idp }->result_source->schema;
        my $interfaces = $schema->resultset('Interface');

        ($opts->{ sp }) = $interfaces->find_by_module_name('samlsp');
    }

    my $idp = Zaaksysteem::SAML2::IdP->new_from_interface(interface => $opts->{ idp });
    my $sp = Zaaksysteem::SAML2::SP->new_from_interface(
        interface => $opts->{sp},
        idp       => $opts->{idp},
    );

    my $idp_entity_id = $opts->{ idp }->jpath('$.idp_entity_id');
    $sp->id($idp_entity_id) if $idp_entity_id;

    return $class->new(sp => $sp, idp => $idp);
}

=head1 Methods

=head2 handle_response($artifact_resolved_xml)

Return value: L<Net::SAML2::Protocol::Assertion> or error on failure

=cut

sub _ssl_verify_xml {
    my ($self, $xml) = @_;

    my $certs = $self->idp->cert('signing');

    unless (defined $certs && ref $certs eq 'ARRAY') {
        throw(
            'saml2/ssl_verify_xml/no_signing_cert',
            'Could not find signing certificate of IDP in metadata',
            {
                response => $xml
            }
        );
    }

    my $xmlfile  = $self->_save_content_to_fh($xml, '.xml');

    my $urn = 'urn:oasis:names:tc:SAML:2.0:protocol:';
    if ($xml =~ /ArtifactResponse/) {
        $urn .= 'ArtifactResponse';
    } else {
        $urn .= 'Response';
    }

    for my $cert (@{ $certs }) {
        my $certfile = $self->_save_content_to_fh($cert, '.crt');

        system(
            '/usr/bin/xmlsec1',
            '--verify',
            '--id-attr:ID',
            $urn,
            '--id-attr:ID',
            'Assertion',
            '--pubkey-cert-pem',
            $certfile->filename,
            $xmlfile->filename,
        );

        # Loop until we verify or run out of certs
        next if $?;

        return 1;
    }

    throw(
        'saml2/handle_response/verification_of_signature_failed',
        'Response from IDP invalid, signature validation failed',
        {
            response => $xml,
        }
    );
}

sub _save_content_to_fh {
    my $self                = shift;
    my $content             = shift;
    my $ext                 = shift || '.tmp';

    my $fh = File::Temp->new(
        UNLINK => 1,
        SUFFIX => $ext
    );

    print $fh $content;
    close($fh);

    return $fh;
}

sub handle_response {
    my $self                = shift;
    my $responsexml         = shift;

    if ($responsexml && $responsexml !~ /xml/) {
        ### Probably base64 encoded? Ranzy check
        $responsexml        = decode_base64($responsexml);
    }

    $self->_log_xml("Incoming SAML authentication response XML", $responsexml);

    $self->_ssl_verify_xml($responsexml);

    $responsexml = $self->_decrypt_response($responsexml);

    my $xmlp = XML::XPath->new(xml => $responsexml);
    $xmlp->set_namespace('samlp', 'urn:oasis:names:tc:SAML:2.0:protocol');

    my $nodeset = $xmlp->find('//samlp:Response/samlp:Status/samlp:StatusCode');

    unless ($nodeset && $nodeset->get_nodelist) {
        throw(
            'saml2/handle_response/no_status_set',
            'No Status found in SOAP Response',
            {
                response => $responsexml
            }
        );
    }

    my ($status)            = $nodeset->get_nodelist;

    if($status->getAttribute('Value') ne SAML_STATUS_SUCCESS) {
        my ($substatus) = grep { $_->isa('XML::XPath::Node::Element'); } $status->getChildNodes;

        my $givenstatus = (
            $substatus
                ? $substatus->getAttribute('Value')
                : $status->getAttribute('Value')
        );

        throw(
            'saml2/handle_response/invalid_status',
            'Invalid status received from SOAP Response: ' . $givenstatus,
            {
                # SAML errors are usually "nested" ("Responder ->
                # RequestDenied" for instance, means that the responder in this
                # transaction (the IdP) denied the login request). For proper
                # error message generation, both levels are needed.
                topstatus => $status->getAttribute('Value'),
                status => $givenstatus,
                response => $responsexml
            }
        );
    }

    ### Validate XML response
    #my ($subject, $saml)    = $response->handle_response($saml);

    my $assertion   = Zaaksysteem::SAML2::Protocol::Assertion->new_from_xml(
        xml => $responsexml,
    );

    my $authenticated_identifier = eval { $self->_load_authenticated_identifier($assertion) };

    if ($@) {
        my $object = $@->object || {};
        throw(
            $@->type,
            $@->message,
            {
                %$object,
                response => $responsexml,
            }
        )
    }

    if ($authenticated_identifier) {
        $self->authenticated_assertion($assertion);

        $self->authenticated_identifier(
            $authenticated_identifier
        );
    }

    unless ($self->authenticated_assertion) {
        throw(
            'saml2/handle_response/invalid_assertion',
            'Invalid assertion',
            {
                response => $responsexml
            }
        );
    }

    ### Retrieve subject from correct saml module
    return $self->authenticated_assertion;
}

sub _load_authenticated_identifier {
    my $self                        = shift;
    my $assertion                   = shift;

    return unless $assertion;

    my $config = $self->idp->interface->get_interface_config;
    my $saml_type = $config->{saml_type};

    my %identifier = (
        used_profile  => $saml_type,
        success       => 1,

        nameid        => $assertion->nameid,
        nameid_format => $assertion->nameid_format,
        session_index => $assertion->session,
    );

    if ($saml_type eq SAML_TYPE_LOGIUS) {
        my %namespec = split m[:], $assertion->nameid;

        $identifier{uid}     = $namespec{ s00000000 }; # Sectorcode for BSN
    } elsif ($saml_type eq SAML_TYPE_KPN_LO) {
        my $kvk_identifier = $self->_get_eherkenning_identifier($assertion->{attributes});

        $identifier{uid}     = $kvk_identifier; # KvK + Vestigingsnummer
    } elsif ($saml_type eq SAML_TYPE_EIDAS) {

        %identifier = ( %identifier, %{$self->_get_eidas_identifier($assertion)} );

    } elsif ($saml_type eq SAML_TYPE_ADFS) {
        my %attributes = $self->_extract_adfs_attributes($assertion);
        @identifier{keys %attributes} = values %attributes;

        if ($config->{use_upn}) {
            if (!exists $identifier{upn}) {
                # With our default claims rule this is sorta impossable tho
                throw('saml2/entity_id/upn',
                    "Required attribute 'upn' is missing from assertion.",
                );
            }
            # Move to different layer?
            $identifier{uid} = delete $identifier{upn};
        }
    } elsif ($saml_type eq SAML_TYPE_MINIMAL) {
        my $email;
        if ($identifier{nameid} =~ /\@/) {
            $identifier{email} = $identifier{nameid};
            $identifier{uid} = ($identifier{nameid} =~ s/\@.*$//r);
        } else {
            $identifier{email} = undef;
            $identifier{uid} = $identifier{nameid};
        }
    }
    else {
        throw("saml2/saml_type/unkown", "Unknown SAML type: $saml_type");
    }

    return \%identifier;
}

=head2 authentication_redirect

Generates a authnrequest and returns the redirect url for use

=cut

define_profile authentication_redirect => (
    required    => [],
    optional    => [qw[relaystate]],
);

sub authentication_redirect {
    my ($self, %params) = @_;
    my $opts = assert_profile(\%params)->valid;

    my $auth_request;
    my %sids;

    my $interface_config = $self->idp->interface->get_interface_config;

    my $saml_type = $interface_config->{saml_type};
    my $acs_index = $interface_config->{idp_acs_index} || 0;

    # Enable spoofmode
    if($saml_type eq SAML_TYPE_SPOOF) {
        return '/auth/saml/prepare-spoof';
    }

    # Depending on the IdP supplier, we need to adjust how we signal
    # which service urls should be used and not.
    if($saml_type eq SAML_TYPE_KPN_LO || $saml_type eq SAML_TYPE_EIDAS) {
        %sids = (
            AssertionConsumerServiceIndex => 1,
            AttributeConsumingServiceIndex => $acs_index
        );
    } elsif($saml_type eq SAML_TYPE_LOGIUS || $saml_type eq SAML_TYPE_ADFS || $saml_type eq SAML_TYPE_MINIMAL) {
        %sids = (
            AssertionConsumerServiceURL => $self->sp->url . '/consumer-post'
        );
    } else {
        throw('saml2/authn_request', 'Unable to decide on service identifiers, non-recognized saml type');
    }

    # Wrap potential die() with a proper exception so we can stacktrace this
    try {
        my %nameid_policy_format;
        if ($saml_type eq SAML_TYPE_ADFS && $interface_config->{saml_request_nameid} ne '') {
            $nameid_policy_format{nameid_policy_format} = $interface_config->{saml_request_nameid};
        }

        $auth_request = Zaaksysteem::SAML2::Protocol::AuthnRequest->new(
            issuer               => $self->resolve_entity_id,
            base_url             => $self->sp->url . '/saml',
            interface            => $self->idp->interface,
            destination          => $self->idp->sso_url('urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect'),
            service_identifiers  => \%sids,
            nameid_format        => $self->idp->format('entity'),
            %nameid_policy_format,
        );
    } catch {
        throw('saml2/authn_request', 'Unable to construct AuthnRequest, intercepted error: ' . $_);
    };

    ## Redirect
    my $redirect = Zaaksysteem::SAML2::Binding::Redirect->new(
        key => $self->sp->cert,
        url => $self->idp->sso_url('urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect'),
        signature_algorithm => $self->signature_algorithm
    );

    my $xml = $auth_request->as_xml;

    $self->_log_xml("Outgoing SAML AuthnRequest XML", $xml);

    return $redirect->sign(
        request => "$xml",
        relaystate => $opts->{ relaystate }
    );
}

=head2 handle_logout_request

Handle a SAML LogoutRequest message: IdP initiated logout

=cut

define_profile handle_logout_request => (
    required => {
        query_string => NonEmptyStr,
        nameid => NonEmptyStr,
        nameid_format => SAMLNameIDFormat,
        session => NonEmptyStr,
    }
);

sub handle_logout_request {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $binding = Zaaksysteem::SAML2::Binding::Redirect->new(
        param => 'SAMLRequest',
        cert => $self->idp->cert('signing'),
    );

    my ($result, $relaystate) = $binding->verify($opts->{query_string});

    $self->_log_xml("Incoming SAML Logout Request XML", $result);

    my $request = Zaaksysteem::SAML2::Protocol::LogoutRequest->new_from_xml(
        xml => $result
    );

    my $logout_status = SAML_STATUS_SUCCESS;
    for my $key (qw(nameid nameid_format session)) {
        if ($request->$key ne $opts->{$key}) {
            my $message = sprintf(
                "Logout request failed: '%s' = '%s', session has '%s'",
                $key,
                $request->$key,
                $opts->{$key},
            );
            $self->log->debug($message);

            $logout_status = SAML_STATUS_REQUESTER;
        } else {
            $self->log->trace("Logout request: $key matches");
        }
    }

    my $response = Zaaksysteem::SAML2::Protocol::LogoutResponse->new(
        issuer      => $self->resolve_entity_id,
        destination => $self->idp->slo_url('urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect'),
        status      => $logout_status,
        response_to => $request->id,
    );
    my $xml = $response->as_xml;

    my $redirect = Zaaksysteem::SAML2::Binding::Redirect->new(
        key => $self->sp->cert,
        url => $self->idp->slo_url('urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect'),
        signature_algorithm => $self->signature_algorithm,
        param => 'SAMLResponse',
    );

    $self->_log_xml("Outgoing SAML Logout Response XML", $xml);

    return (
        1,
        $redirect->sign(request => "$xml")
    );
}

=head2 handle_logout_response

Handle a SAML LogoutResponse message: response to SP-initiated logout

=cut

sub handle_logout_response {
    my ($self, $query) = @_;

    my $binding = Zaaksysteem::SAML2::Binding::Redirect->new(
        param => 'SAMLResponse',
        cert => $self->idp->cert('signing'),
    );

    my ($result, $relaystate) = $binding->verify($query);

    $self->_log_xml("Received SAML Logout Response XML:", $result);

    my $response = Zaaksysteem::SAML2::Protocol::LogoutResponse->new_from_xml(
        xml => $result
    );

    if ($response->status ne SAML_STATUS_SUCCESS) {
        $self->log->error("SAML Logout failed");
        throw(
            'saml2/logout_failure',
            'SAML2 logout failed: ' . $response->status,
            {
                status => $response->status,
            }
        );
    } else {
        $self->log->debug("SAML Logout successful");
    }

    return 1;
}

=head2 logout_redirect

Return a redirection URL for the SingleLogoutService on the IdP for the
session, nameid specified.

=cut

define_profile logout_redirect => (
    required => {
        nameid => NonEmptyStr,
        nameid_format => SAMLNameIDFormat,
        session_index => NonEmptyStr,
    }
);

sub logout_redirect {
    my ($self, %params) = @_;
    my $opts = assert_profile(\%params)->valid;

    my $logout_request = Zaaksysteem::SAML2::Protocol::LogoutRequest->new(
        issuer        => $self->resolve_entity_id,
        destination   => $self->idp->slo_url('urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect'),
        nameid        => $opts->{nameid},
        nameid_format => $opts->{nameid_format},
        session       => $opts->{session_index},
    );

    my $redirect = Zaaksysteem::SAML2::Binding::Redirect->new(
        key => $self->sp->cert,
        url => $self->idp->slo_url('urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect'),
        signature_algorithm => $self->signature_algorithm
    );

    my $xml = $logout_request->as_xml;

    $self->_log_xml("Outgoing SAML Logout Request XML", $xml);

    return $redirect->sign(
        request => "$xml",
    );
}

sub _log_xml {
    my ($self, $msg, $xml) = @_;

    my $id = $self->context_id // '<unknown context id>';

    $self->idp->interface->process_trigger(
        'log_saml_xml',
        {
            context_id => $id,
            message    => $msg,
            xml        => $xml,
        }
    );

    if ($self->log->is_trace) {
        $self->log->trace($msg);
        while ($xml =~ m/(.{0,500})/g) {
            $self->log->trace($1) if defined($1);
        }
    }
    return 1;
}

=head2 signature_algorithm

Getter for the XMLDSig signature algorithm to be used. Defaults to
L<http://www.w3.org/2000/09/xmldsig#rsa-sha1>.

If the C<saml_type> of the configured IdP is L<SAML_TYPE_KPN_LO>, the returned
URI will b L<http://www.w3.org/2001/04/xmldsig-more#rsa-sha256>.

Nothing is generically supported here tho, this is here just to keep the cruft
at bay.

=cut

sub signature_algorithm {
    my $self = shift;

    my $type = $self->idp->interface->jpath('$.saml_type');

    if($type eq SAML_TYPE_KPN_LO || $type eq SAML_TYPE_ADFS) {
        return  'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256';
    }

    # Default to SHA1, this is required for Logius at least at this moment (2013-12-10)
    return 'http://www.w3.org/2000/09/xmldsig#rsa-sha1';
}

=head2 resolve_entity_id

Getter for the configured entity_id. By default this will return the id
configured on the SP, which defaults to a URI that identifies it. The
returned id can be overridden by specifying an idp_entity_id in the IdP
configuration

=cut

sub resolve_entity_id {
    my $self = shift;

    my $config = $self->idp->interface->get_interface_config;
    my $idp_entity_id = $config->{idp_entity_id};

    unless (defined($idp_entity_id) && length($idp_entity_id)) {
        $idp_entity_id = $self->sp->id;
    }

    return $idp_entity_id;
}


=head2 resolve_artifact

Contacts the IdP using the given SAML Artificat

=cut

sub resolve_artifact {
    my $self = shift;

    my $resolve_request = Zaaksysteem::SAML2::Protocol::ArtifactResolve->new(
        artifact => shift,
        issuer => $self->sp->id,
        destination => $self->idp->art_url('urn:oasis:names:tc:SAML:2.0:bindings:SOAP')
    );

    my $redirect = Zaaksysteem::SAML2::Binding::SOAP->new(
        url => $self->idp->art_url('urn:oasis:names:tc:SAML:2.0:bindings:SOAP'),
        key => $self->sp->cert,
        cert => $self->sp->cert,
        idp_cert => $self->idp->cert('signing'),
        cacert => $self->idp->cacert
    );

    my $xml = $resolve_request->as_xml;
    my $req = $redirect->request($xml);

    if ($self->log->is_trace) {
        $self->log->trace("Resolve artifact XML: \n$xml");
    }

    return $req;
}

sub _get_eidas_identifier {
    my ($self, $assertion) = @_;

    my %mapping = (
        date_of_birth     => 'urn:etoegang:1.9:attribute:DateOfBirth',
        surname           => 'urn:etoegang:1.9:attribute:FamilyName',
        firstname         => 'urn:etoegang:1.9:attribute:FirstName',
        acting_subject_id => 'urn:etoegang:core:ActingSubjectID',
    );

    my %id = map { $_ => $assertion->{attributes}{$mapping{$_}}[0] } keys %mapping;
    return \%id;

}

sub _get_eherkenning_identifier {
    my ($self, $attributes) = @_;

    my ($kvk_number, $kvk_vestigingsnr);

    my $version;
    my %versions = (
        '1.5' => {
            coc_number          => 'urn:nl:eherkenning:1.0:EntityConcernedID',
            coc_location_number => 'urn:nl:eherkenning:1.2:EntityConcernedSubID',
        },
        '1.7' => {
            coc_number          => 'urn:nl:eherkenning:1.7:EntityConcernedID:KvKnr',
            coc_location_number => 'urn:nl:eherkenning:1.7:EntityConcernedID:Vestigingsnr',
        },
        '1.9' => {
            coc_number          => 'urn:etoegang:1.9:EntityConcernedID:KvKnr',
            coc_location_number => 'urn:etoegang:1.9:ServiceRestriction:Vestigingsnr',
        },
    );

    foreach my $v (keys %versions) {
        my $attr = $versions{$v}{coc_number};
        if (exists $attributes->{$attr}) {
            $version = $v;
            $kvk_number = $attributes->{$attr}[0];
            last;
        }
    }

    if (!$kvk_number) {
        throw('saml2/eherkenning', "Unable to find chamber of commerce number in SAML response");
    }

    my $prefix = substr($kvk_number, 0, 8);

    unless ($prefix == 3) {
        throw('saml/entity_id', sprintf('Unable to determine KvK, unsupported prefix: %s', $prefix));
    }

    $kvk_number = substr($kvk_number, 8, 8);

    my $attr = $versions{$version}{coc_location_number};
    if (exists $attributes->{$attr}) {
        $kvk_vestigingsnr = $attributes->{$attr}[0];
        $prefix = substr($kvk_vestigingsnr, 0, 8);

        unless ($prefix == 6) {
            throw('saml/entity_id',
                "Unable to determine KvK - vestigingsnummer, unsupported prefix: $prefix"
            );
        }
        $kvk_vestigingsnr = substr($kvk_vestigingsnr, -12);
    }

    return join("", $kvk_number, $kvk_vestigingsnr // '');
}

sub _extract_adfs_attributes {
    my $self = shift;
    my $assertion = shift;

    my $msbase = 'http://schemas.microsoft.com/ws/2008/06/identity/claims';
    my $soapbase = 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims';

    my %extracted_attributes;
    my %required_attributes = (
        uid       => "$msbase/windowsaccountname",
        email     => "$soapbase/emailaddress",
        name      => "$soapbase/name",
        givenname => "$soapbase/givenname",
        surname   => "$soapbase/surname",
    );
    my %optional_attributes = (
        initials  => "$soapbase/initials",
        phone     => "$soapbase/otherphone",
        upn       => "$soapbase/upn",
        title     => "$soapbase/title",
    );

    while (my ($attr, $name) = each %required_attributes) {
        if (!exists $assertion->{attributes}{ $name }) {
            throw(
                'saml/entity_id',
                sprintf(
                    "Required attribute '%s' is missing from assertion.",
                    $name,
                ),
            );
        }

        $extracted_attributes{$attr} = $assertion->{attributes}{ $name }[0];
    }

    while (my ($attr, $name) = each %optional_attributes) {
        next unless exists $assertion->{attributes}{ $name };
        $extracted_attributes{$attr} = $assertion->{attributes}{ $name }[0];
    }

    return %extracted_attributes;
}

sub _decrypt_response {
    my $self = shift;
    my $xml_string = shift;

    # In an ideal world, this would be able to use separate certs for signing and encryption
    my $cert = $self->sp->cert;
    return unless $cert;
    my $cert_name = $self->sp->cert_name;

    my $xml = XML::LibXML->load_xml(string => $xml_string);
    my $xpc = XML::LibXML::XPathContext->new();

    $xpc->registerNs('xenc' => 'http://www.w3.org/2001/04/xmlenc#');
    $xpc->registerNs('ds'   => 'http://www.w3.org/2000/09/xmldsig#');

    my @encrypted_nodes = $xpc->findnodes(
        '//*[xenc:EncryptedData]',
        $xml->documentElement,
    );

    my $parser = XML::LibXML->new();
    for my $encrypted_node (@encrypted_nodes) {
        my $algorithm = $xpc->findvalue('./xenc:EncryptedData/xenc:EncryptionMethod/@Algorithm', $encrypted_node);
        assert_symmetric_algorithm($algorithm);

        my $symmetric_key_uri = $xpc->findvalue('./xenc:EncryptedData/ds:KeyInfo/ds:RetrievalMethod/@URI', $encrypted_node);
        my $symmetric_key_id = $symmetric_key_uri =~ s/^#//r;
        
        # Maybe use documentElement instead?
        my ($encrypted_key_block) = $xpc->findnodes("//xenc:EncryptedKey[\@Id=\"$symmetric_key_id\"]", $encrypted_node);
        my $key_encryption_algorithm = $xpc->findvalue('./xenc:EncryptionMethod/@Algorithm', $encrypted_key_block);
        assert_asymmetric_algorithm($key_encryption_algorithm);

        my $key_encryption_digest = $xpc->findvalue('./xenc:EncryptionMethod/ds:DigestMethod/@Algorithm', $encrypted_key_block);
        my $digest_name = assert_encryption_digest($key_encryption_digest);

        my $key_name = $xpc->findvalue('./ds:KeyInfo/ds:KeyName', $encrypted_key_block);

        # We don't know key $key_name - don't try to decrypt.
        if($key_name ne $cert_name) {
            $self->log->info("Unknown encryption key used: '$key_name'; expected '$cert_name'");
            next;
        }

        my $encrypted_key_data_b64 = $xpc->findvalue('./xenc:CipherData/xenc:CipherValue', $encrypted_key_block);
        my $encrypted_key_data = decode_base64($encrypted_key_data_b64);

        my $rsa_key = Crypt::PK::RSA->new($cert);
        my $aes_key = $rsa_key->decrypt($encrypted_key_data, 'oaep', $digest_name, '');

        my $encrypted_data_b64 = $xpc->findvalue('./xenc:EncryptedData/xenc:CipherData/xenc:CipherValue', $encrypted_node);
        my $encrypted_data = decode_base64($encrypted_data_b64);

        my $iv = substr($encrypted_data, 0, 16, '');
        
        # For now, we support AES, without padding
        my $cbc = Crypt::Mode::CBC->new('AES', 0);
        my $decrypted_data = $cbc->decrypt($encrypted_data, $aes_key, $iv);

        my $last_byte = substr($decrypted_data, -1, 1);
        if (ord($last_byte) <= (128/8)) { # AES blocksize = 128 bits
            substr($decrypted_data, -1 * ord($last_byte)) = '';
        }

        my $fragment = $parser->parse_balanced_chunk($decrypted_data);
        $encrypted_node->replaceNode($fragment);
    }

    return $xml->toString();
}

sub assert_symmetric_algorithm {
    my $algo = shift;

    state $ALLOWED = {
        'http://www.w3.org/2001/04/xmlenc#aes128-cbc' => 1,
        'http://www.w3.org/2001/04/xmlenc#aes192-cbc' => 1,
        'http://www.w3.org/2001/04/xmlenc#aes256-cbc' => 1,
    };

    throw('saml2/symmetric_encryption_method')
        unless $ALLOWED->{ $algo };
}

sub assert_asymmetric_algorithm {
    my $algo = shift;

    state $ALLOWED = {
        'http://www.w3.org/2001/04/xmlenc#rsa-oaep-mgf1p' => 1,
    };

    throw('saml2/asymmetric_encryption_method')
        unless $ALLOWED->{ $algo };
}

sub assert_encryption_digest {
    my $algo = shift;

    state $ALLOWED = {
        'http://www.w3.org/2000/09/xmldsig#sha1' => 'SHA1',
        'http://www.w3.org/2001/04/xmlenc#sha256' => 'SHA256',
    };

    throw('saml2/digest_method')
        unless $ALLOWED->{ $algo };

    return $ALLOWED->{ $algo };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
