package Zaaksysteem::Controller::Gegevensmagazijn::NatuurlijkPersoon;
use Moose;

=head1 NAME

Zaaksysteem::Controller::Gegevensmagazijn::NatuurlijkPersoon  - Gegevensmagazijn controller for Natuurlijk persoon

=head1 DESCRIPTION

Implements actions which you can do for Natuurlijk Personen in the gegevensmagazijn

=cut

use Moose::Util::TypeConstraints;
use namespace::autoclean;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

sub base : Chained('/') : PathPart('gegevensmagazijn/np') : CaptureArgs(0) : ZAPI {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');
    $c->stash->{zapi} = [{ foo => 'bar' }];
}

=head2 delete

Deletes object subscriptions for natuurlijk persoon from our system
or inactivates/deletes natuurlijk personen based on if they have cases
or not

=head3 URI

/gegevensmagazijn/np/delete

=cut

sub delete : Chained('base') : PathPart('delete') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    my $total = $self->_handle_object_subscription_for_np($c,
        { action => 'disable', label => 'Verwijder' });

    $c->stash->{zapi} = [{ queue_items => $total }];
}


=head2 create

Creates object subscriptions for natuurlijk persoon from our system

=head3 URI

/gegevensmagazijn/np/create

=cut

sub create : Chained('base') : PathPart('create') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    my $total = $self->_handle_object_subscription_for_np($c,
        { action => 'enable', label => 'Voeg toe' });

    $c->stash->{zapi} = [{ queue_items => $total }];
}


define_profile _handle_object_subscription_for_np => (
    required => {
        action => subtype('Str' => where { $_ =~ /^(?:disable|enable)$/ }),
        label  => 'Str',
    }
);

sub _handle_object_subscription_for_np {
    my ($self, $c)  = @_;
    my $options = assert_profile($_[2] || {})->valid;

    throw('httpmethod/post', 'Invalid HTTP Method, no POST', [])
        unless lc($c->req->method) eq 'post';

    my $params = $c->req->params;
    my $gm     = $c->model("Gegevensmagazijn::NatuurlijkPersoon");

    my $search_opts = {};
    my $search      = {};

    push(@{ $search_opts->{prefetch} }, 'adres_id');
    $search->{'me.deleted_on'} = undef;
    if ($params->{selection_type} eq 'subset') {
        $search->{'me.id'} = { 'in' => $params->{selection_id} };
    }

    my $np = $gm->search($search, $search_opts, %$params);
    my (@disable_persons, @enable_persons, @rows);

    my $rs = $gm->schema->resultset("ObjectSubscription")->search(
        {
            'local_id::integer' => { in => $np->get_column('id')->as_query },
            local_table => 'NatuurlijkPersoon'
        }
    );

    while (my $os = $rs->next) {
        if ($os->date_deleted)  {
            if ($options->{action} eq 'disable') {
                push(
                    @disable_persons,
                    {
                        priority => 100,
                        label    => $options->{label}
                            . ' natuurlijk persoon met ID '
                            . $os->local_id,
                        data => { natuurlijk_persoon_id => $os->local_id, }
                    }
                );
            }
            else {
                push(
                    @enable_persons,
                    {
                        priority => 100,
                        label    => $options->{label}
                            . ' natuurlijk persoon met ID '
                            . $os->local_id,
                        data => { natuurlijk_persoon_id => $os->local_id, }
                    }
                );
            }
        }
        else {
            push(
                @rows,
                {
                    priority => 100,
                    label    => $options->{label}
                        . ' afnemerindicatie voor extern ID '
                        . $os->external_id,
                    data => {
                        subscription_id => $os->id,
                        interface_id    => $os->get_column('interface_id'),
                        config_interface_id =>
                            $os->get_column('config_interface_id'),
                    }
                }
            );
        }
    }

    my $q = $c->model("DB::Queue");
    if ($options->{action} eq 'disable' && ( $params->{object_subscription} eq '' || $params->{object_subscription} == 2 )) {
        # old skool legacy support, delete/inactivate them
        $np->reset; $rs->reset;
        $np = $np->search_rs({ 'me.authenticated' => [ 0, undef ] });
        while (my $n = $np->next) {
            push(
                @disable_persons,
                {
                    priority => 100,
                    label    => $options->{label}
                        . ' natuurlijk persoon met ID '
                        . $n->id,
                    data => { natuurlijk_persoon_id => $n->id, }
                }
            );
        }

        $q->create_items($options->{action} . '_natuurlijk_persoon', @disable_persons);
    }

    if ($options->{action} eq 'enable') {
        $q->create_items($options->{action} . '_natuurlijk_persoon', @enable_persons);

    }

    $q->create_items($options->{action} . '_object_subscription', @rows);

    return (scalar @rows + scalar @disable_persons);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
