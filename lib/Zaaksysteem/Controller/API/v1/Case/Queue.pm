package Zaaksysteem::Controller::API::v1::Case::Queue;

use Moose;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Case::Queue - APIv1 controller for case object queue items

=head1 DESCRIPTION

=cut

use BTTW::Tools;

sub BUILD {
    my $self = shift;

    $self->add_api_control_module_type('api', 'app');
    $self->add_api_context_permission('intern');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/case/[CASE_UUID]/queue> namespace.

=cut

sub base : Chained('/api/v1/case/instance_base') : PathPart('queue') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{ queue } = $c->stash->{ case }->queues->search(undef, {
        order_by => { -desc => 'date_created' }
    })
}

=head2 instance_base

Reserves the C</api/v1/case/[CASE_UUID]/queue/[QUEUE_ITEM_UUID]> namespace.

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    my $item = $c->stash->{ queue }->find($uuid);

    unless (defined $item) {
        throw('api/v1/case/queue/item/not_found', sprintf(
            'No queue item with id "%s" could be found',
            $uuid
        ));
    }

    $c->stash->{ item } = $item;
}

=head2 list

Returns the serialized set of queued items.

=head3 URL path

C</api/v1/case/[CASE_UUID]/queue>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = Zaaksysteem::API::v1::ResultSet->new(
        iterator => $c->stash->{ queue },
    )->init_paging($c->request);
}

=head2 get

=head3 URL path

C</api/v1/case/[CASE_UUID]/queue/[QUEUE_ITEM_UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ item };
}

=head2 handle

=head3 URL Path

C</api/v1/case/[CASE_UUID]/queue/[QUEUE_ITEM_UUID]/run>

=cut

sub handle : Chained('instance_base') : PathPart('run') : Args(0) : RW {
    my ($self, $c) = @_;

    # Sync instance to DB row state
    $c->stash->{ result } = $c->stash->{ item }->discard_changes;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
