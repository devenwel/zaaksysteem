package Zaaksysteem::Controller::API::v1;

use Moose;
use namespace::autoclean;
use IO::All;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1 - Provides 'base' chain for API v1

=head1 SYNOPSIS

    package Zaaksysteem::API::v1::MyController;

    use Moose;

    BEGIN { extends 'Zaaksysteem::Controller::API::v1' }

    # This action can be called by users configured to have read-only
    # permissions in the API configurations
    sub listing : Chained('/api/v1/base') : PathPart('list') : Args(0) : RO {
        ...
    }

    # This action is implied read-write, and won't be reached by users
    # configured to only have read-only permissions.
    sub my_action : Chained('/api/v1/base') : PathPart('my_action') : Args(0) {
        ...
    }

=cut

use BTTW::Tools;
use List::Util qw[first any];
use Zaaksysteem::API::v1::Message::Pong;
use Zaaksysteem::Constants::Users qw(:all);

=head1 CONTROLLER ATTRIBUTES

Inside the different API controllers. Certain attributes could be set. Documentation of
these attributes below.

=head2 api_capabilities

=over 4

=item Content: ArrayRef of capabilities

=item Return value: ArrayRef of Str

=back

    $self->api_capabilities;

    # Returns:
    [ 'intern', 'public_access', 'extern' ]

Returns a list of capabilities for the chosen API.

B<Capabilities>

=over 4

=item intern

Internal API. Works for logged in users only. Mainly used by Angular for logged in "behandelaren"

=item public_access

A publicly accessible API. No authentication required

=item extern

An externally available API. These API's can be called from an external source, but an API Koppelprofiel
must be configured.

=back

=cut

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('public_access');

    return;
}

=head1 ACTIONS

=head2 default

This action matches on any path not used by one of the chained sub-actions
of L</base>. It always triggers an C<api/v1>-typed exception, with a link to
the current API documentation.

=cut

sub default : Chained('base') : PathPart('') {
    my ($self, $c) = @_;

    throw('api/v1', 'Invalid request. For more information, see ' . $c->uri_for('/man/Zaaksysteem::API::V1'), 400);
}

=head2 base

This base action will verify the client is authorized to access the API
infrastructure, and the specific action being called.

Access via this base action is L<logged|Zaaksysteem::DB::ResultSet::Logging>
under the event type C<api/request>.

The L<interface|Zaaksysteem::Backend::Sysin::Interface::Component> used to
authenticate the client can be found in the L<stash|Catalyst/c-stash> under
the key C<interface>.

The L<user|Zaaksysteem::Backend::Auth::UserEntity::Component> as whom the
client is authorized to access the API can be found under the C<api_user>
stash key.

=cut

=head2 base

API modules expect a few things to be set in the stash. The base will set these according to
the given API interface etc.

B<stash>

The stash will be filled with the below attributes

=over 4

=item has_full_access

Type: Bool

Defines whether this user has full access to the information from the API. This is true for
every logged in user, because casetype restrictions are already in place, and logged in users
may see every attribute in a case.

=item api_user

The logged in user for this request

=item api_interface

The API control interface

=back

B<capabilities>

Every module is able to set a list of capabilities via the Moose accessor: C<api_capabilities>. A
list follows.

=over 4

=item public_access

Access to this api is possible without authenticaiton

=item extern

Not only logged in users may access this API, also external B<authenticated> users are allowed
to access.

=item intern

Only internal use of this api is allowed

=item allow_pip

Tells us to allow calls from the PIP

=back

=cut

sub base : Chained('/') : PathPart('api/v1') : CaptureArgs(0) {
    my ($self, $c) = @_;

    # Access via platform key, allow anything
    return if $c->stash->{ platform_access };

    ### We need to get the API capabilities from the implemented api, to know
    ### whether this API is able to be called external vs internal, etc
    my $capabilities = $self->_get_permitted_api_contexts($c);

    my $interface = $self->_get_interface_by_id($c);
    $self->_assert_ro_interface($c, $interface);

    # Public access API's don't require a user.
    my $user   = $capabilities->{public_access} ? undef : $self->_get_user_by_interface($c, $interface);
    my $is_pip = $capabilities->{allow_pip} && $c->session->{pip};

    $c->stash->{api_user}  = $user;
    $c->stash->{interface} = $interface;

    $c->stash->{has_full_access} = $self->_is_full_access_interface(
        $interface
    );

    # Regular logged in user.
    if ($user && !$user->is_external_api) {
        $c->stash->{api_user}        = $user;
        $c->stash->{has_full_access} = 1;
    }
    elsif ($capabilities->{extern} && $user && $user->is_external_api) {
        $c->assert_user(API);
    }
    elsif ($capabilities->{intern}) {
        # does the same check as the above if statement, but also throws
        # an exception
        $c->assert_user;
    }
    elsif ($capabilities->{public_access} && !$is_pip) {
        $self->log->trace("Requested public access API, pass through");
    }
    elsif (!$capabilities->{public_access} || $is_pip) {
        if (!$c->stash->{interface}) {
            throw(
                'api/v1/error/api_interface_id',
                'Please set the API-Interface-Id header or consult your '
                . 'Zaaksysteem engineer to validate the interface configuration',
            );
        }
        elsif (!$is_pip && !$c->stash->{api_user}) {
            throw(
                'api/v1/error/no_user',
                'Unable to authenticate API user',
                { http_code => 401 }
            );
        }
    }

    $self->_log_post_for_interface($c, $interface);
}

=head2 ping

This action emulates the ping/pong behavior that external systems are expected
to implement.

=cut

sub ping : Chained('/') : PathPart('api/v1/ping') : Args(0) {
    my ($self, $c) = @_;

    unless ($c->req->params->{ api_version } eq 'v1') {
        throw(
            'api/v1/ping/invalid_version',
            'This interface only supports v1 requests'
        );
    }

    my $message = $c->req->params->{ message };

    unless (ref $message eq 'HASH') {
        throw('api/v1/ping/message_expected', sprintf(
            'Unexpected or missing message content, got %s',
            ref $message || $message
        ));
    }

    unless ($message->{ type } eq 'message') {
        throw('api/v1/ping/message_type_invalid', sprintf(
            'Expected instance of message type, got %s',
            $message->{ type }
        ));
    }

    my $instance = $message->{ instance };

    unless ($instance->{ type } eq 'ping') {
        throw('api/v1/ping/instance_of_ping_expected', sprintf(
            'Expected message instance of type ping, got %s',
            $instance->{ type }
        ));
    }

    $c->stash->{ result } = Zaaksysteem::API::v1::Message::Pong->new(
        payload => $instance->{ payload }
    );
}

=head1 PRIVATE METHODS

=cut

sub _log_post_for_interface {
    my ($self, $c, $interface) = @_;

    if ($interface && $interface->module eq 'api' && lc($c->req->method) eq 'post') {
        my $body = {};
        if ($c->req->content_type eq 'application/json') {
            $body = $self->_get_body_as_string($c->req->body) || '{}';
        }

        $c->stash->{ request_event } = $interface->process_trigger('log_mutation', {
            client_ip       => $c->get_client_ip,
            request_id      => $c->stash->{ request_id },
            request_method  => lc($c->req->method),
            request_call    => lc($c->req->path),
            request_body    => $body,
        });

        return 1;
    }

    return;
}

sub _is_full_access_interface {
    my ($self, $interface) = @_;

    return 0 if !$interface;
    if (   $interface->module_object->can('api_full_access')
        && $interface->module_object->api_full_access)
    {
        return 1;
    }

}

=head2 _get_configuration_interfaces

Returns a list of configuration interfaces which match the end controller

=cut

sub _get_configuration_interfaces_for_action {
    my ($self, $c)  = @_;

    my $controller = $c->component($c->action->class);

    my @module_types = $controller->api_control_module_types;

    unless (scalar @module_types) {
        # Default module type for all controllers
        push @module_types, 'api';
    }

    my @possible_interfaces = $c->model('DB::Interface')->search_active(
        { module => ['api', 'bbvapp', 'controlpanel'] },
        { order_by => { -asc => 'id' } },
    )->all;

    ### Get matching module type
    my @interfaces;
    for my $interface (@possible_interfaces) {
        my $module      = $interface->module_object;

        if (!$module) {
            $self->log->error(sprintf('Unable to get module "%s" with name "%s"', $interface->module, $interface->name));
            next;
        }
        my %type_map    = map { $_ => 1 } @{ $module->module_type };

        ### apiv1 needs to be set on the module
        next unless $type_map{apiv1};

        ### Check whether the module_types match with the controller
        next unless any { $type_map{ $_ } } @module_types;

        push @interfaces, $interface;
    }

    return @interfaces;
}

=head2 _get_body_as_string

Returns a File::Temp object as string

=cut

sub _get_body_as_string {
    my $self        = shift;
    my $body        = shift;

    return '' unless UNIVERSAL::isa($body, 'File::Temp');
    return scalar io($body->filename)->slurp;
}

sub _get_interface_by_id {
    my ($self, $c) = @_;

    my $id = $c->req->header('API-Interface-Id') // '';

    # Logged in user do not require an interface by id unless they ask
    # for it.
    if (!$id && $c->user_exists && !$c->user->is_external_api) {
        return;
    }

    my @interfaces  = $self->_get_configuration_interfaces_for_action($c);
    if ($id) {
        return first { $_->id eq $id } @interfaces;
    }
    elsif (@interfaces == 1) {
        return $interfaces[0];
    }

    if ($id) {
        $self->log->warn("No public interface found with API-Interface-Id '$id'");
    }
    else {
        $self->log->warn("No public interface configured");
    }
    return;
}

sub _get_user_by_interface {
    my ($self, $c, $interface) = @_;

    # If you are logged in, short cut everything, unless you are an API user, which should auth on every call.
    if ($c->user_exists) {
        if ($c->user->is_external_api) {
            $c->logout;
            $c->delete_session;
        } else {
            return $c->user;
        }
    }

    # PIP users are not users;
    return undef if $c->session->{pip};

    if ($interface && $interface->module_object->can('api_no_auth') && $interface->module_object->api_no_auth) {
        return $c->model('DB::Subject')->find($interface->jpath('$.medewerker.id'));
    }

    if ($c->req->header('Disable-Digest-Authentication')) {
        throw(
            "api/v1/authentication/digest/disabled",
            "Unable to authorize user via digest authentication",
            { http_code => 401 }
        );
    }

    return $c->authenticate({ interface => $interface }, 'api');
}

sub _assert_ro_interface {
    my ($self, $c, $interface) = @_;
    return if (!$interface);
    # 'rw' is the intersting bit, any other value is implied read-only.
    if ($interface->name eq 'api' && ($interface->jpath('$.access') || 'ro') ne 'rw') {
        # check the action is explicitly defined to be read-only.
        unless (exists $c->action->attributes->{ RO }) {
            throw('api/v1/forbidden', 'This action has been disabled by the API Security Policy', {
                http_code => 403
            });
        }
    }
}

sub _get_permitted_api_contexts {
    my ($self, $c)  = @_;

    my $controller = $c->component($c->action->class);

    my %contexts = map { $_ => 1 } $controller->permitted_api_contexts;

    unless (scalar keys %contexts) {
        throw('api/v1/exception', sprintf(
            'Controller "%s" is missing permitted API contexts',
            ref $controller
        ));
    }

    return \%contexts;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
