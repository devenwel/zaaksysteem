package Zaaksysteem::Controller::API::v0::Case;

use Moose;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::v0::Case

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use List::Util qw[all];
use MooseX::Types::Moose qw[Int];

=head1 ACTIONS

=head2 base

Reserves the C<api/v0/case> URI namespace.

=cut

sub base : Chained('/api/base') : PathPart('v0/case') : CaptureArgs(0) {
    my ($self, $c) = @_;

    return;
}

=head2 instance_base

Reserves the C<api/v0/case/:number> URI namespace.

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $number) = @_;

    $c->set_log_context('zs.case.number', $number);

    Int->check($number) or throw('api/v0/case/invalid_case_number', sprintf(
        '"%s" is not a valid case number, must be serial id',
        $number
    ));

    $c->stash->{ case_number } = $number;

    my $zaak = $c->model('DB::Zaak')->find_v0($number);

    # Breakup long lines, read as $cond1 && $cond2 && ...
    my $found = all { $_ } (
        defined $zaak,
        !$zaak->is_deleted,
        $c->check_any_given_zaak_permission(
            $zaak,
            qw[zaak_read zaak_beheer zaak_edit]
        )
    );

    unless ($found) {
        throw('api/v0/case/not_found', sprintf(
            'Case number "%d" not found',
            $number
        ));
    }

    $c->stash->{ case_v0 } = $zaak;

    return;
}

=head2 get

Retrieves a 'v0' style serialization of a case given by the case number.

Note that this serialization is a stripped down variant of the API found on
the C</api/case/:number> endpoint for the purpose of enhancing the response
times of this often used call.

=head3 URI Path

C</api/v0/case/:number>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    my $case_data = $c->stash->{ case_v0 }->TO_JSON_V0;

    # Following code does some Zaaksysteem-Catalyst instance dependent
    # injections

    # This injection relies on prefetched zaak_authorisations
    $case_data->{ case }{ permissions } = $c->retrieve_list_of_zaak_permissions($c->stash->{ case_v0 });

    $c->stash->{ zapi } = [ $case_data ];
}

=head2 checklists

Retrieves the checklist resources for a given case, per phase, as a ZAPI reponse.

    "results": [
        {
            "id": 123,
            "milestone": 2,
            "items": [
                {
                    "checked": true,
                    "id": 456,
                    "label": "I'm a checklist item",
                    "user_defined": false
                },
                ...
            ]
        },
        ...
    ]

=head3 URI Path

C</api/v0/case/:number/checklists>

=cut

sub checklists : Chained('instance_base') : PathPart('checklists') : Args(0) {
    my ($self, $c) = @_;

    my $checklists = $c->stash->{ case_v0 }->checklists->search(undef, {
        order_by => 'case_milestone',
        prefetch => 'ordered_checklist_items',
    });

    $c->stash->{ zapi } = [ $checklists->all ];
}

=head2 actions

Retrieves the action resources for a given case, per phase, as a ZAPI response.

    "results": [
        {
            "milestone_id": 123,
            "milestone": 1,
            "items": [
                {
					"automatic" : false,
					"data" : { ... }
					"description" : "Tooltip description of the action",
					"id" : 456,
					"label" : "Sort title",
					"tainted" : false,
					"type" : "my_type",
					"url" : "/zaak/789/action?id=456"
                },
                ...
            ]
        },
        ...
    ]

=head3 URI Path

C</api/v0/case/:number/actions>

=cut

sub actions : Chained('instance_base') : PathPart('actions') : Args(0) {
    my ($self, $c) = @_;

    my $node = $c->stash->{ case_v0 }->zaaktype_node_id;
    my $milestones = $node->zaaktype_statussen->search(undef, {
        order_by => 'status'
    });

    my $action_rs = $c->stash->{ case_v0 }->case_actions_cine;

    my @action_groups;

    while (my $milestone = $milestones->next) {
        push @action_groups, {
            milestone_id => $milestone->id,
            milestone => $milestone->status,
            items => [ $action_rs->milestone($milestone->status)->sorted ]
        };
    }

    $c->stash->{ zapi } = \@action_groups;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
