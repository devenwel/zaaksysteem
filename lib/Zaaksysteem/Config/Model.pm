package Zaaksysteem::Config::Model;

use Moose;
use namespace::autoclean;

# We apply roles at the bottom of this module

=head1 NAME

Zaaksysteem::Config::Model - Config item/definition interaction model

=head2 DESCRIPTION

A model that stores and retreives configuration items for api/v1/config

=head2 SYNOPIS

    use Zaaksysteem::Config::Model;

    my $model = Zaaksysteem::Config::Model->new(
        schema                      => $schema,
        item_rs                     => $resultset,
        definition_store            => $definition_store,
        bibliotheek_notificaties_rs => $resultset,
        subject_model               => Zaaksysteem::BR::Subject->new(),
        groups_rs                   => $resultset,
        roles_rs                    => $resultset,
    );

=cut

use BTTW::Tools;
use JSON qw(decode_json encode_json);
use Zaaksysteem::Object::Types::Config::Panel;
use Zaaksysteem::Object::Types::Config::Item;
use Zaaksysteem::Object::Query;
use Zaaksysteem::Search::ObjectQueryResultSetShim;
use Zaaksysteem::Types qw[Store];

=head1 ATTRIBUTES

=head2 schema

Reference to a L<Zaaksysteem::Schema> instance

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

=head2 item_rs

Reference to a L<Zaaksysteem::Backend::Config::ResultSet> instance.

=cut

has item_rs => (
    is => 'rw',
    isa => 'Zaaksysteem::Backend::Config::ResultSet',
    required => 1
);

=head2 bibliotheek_notificaties_rs

Reference to a L<Zaaksysteem::Schema::BibliotheekNotificaties> resultset.

=cut

has bibliotheek_notificaties_rs => (
    is       => 'rw',
    isa      => 'DBIx::Class::ResultSet',
    required => 1
);

=head2 definition_store

L<Zaaksysteem::Instance::Store> instance containing C<config/definition>
objects.

=cut

has definition_store => (
    is => 'rw',
    isa => Store,
    required => 1
);

=head2 subject_model

A L<Zaaksysteem::BR::Subject> object

=cut

has subject_model => (
    is       => 'ro',
    isa      => 'Zaaksysteem::BR::Subject',
    required => 1,
);


=head2 groups_rs

A L<Zaaksysteem::Schema::Groups::ResultSet> object

=cut

has groups_rs => (
    is       => 'ro',
    isa      => 'DBIx::Class::ResultSet',
    required => 1,
);

=head2 roles_rs

A L<Zaaksysteem::Schema::Roles::ResultSet> object

=cut

has roles_rs => (
    is       => 'ro',
    isa      => 'DBIx::Class::ResultSet',
    required => 1,
);

=head1 METHODS

=head2 panel_instance

Returns a L<Zaaksysteem::Object::Types::Config::Panel> instance

=cut

sub panel_instance {
    my $self = shift;

    return Zaaksysteem::Object::Types::Config::Panel->new(
        name        => 'Systeemconfiguratie',
        items       => [ $self->item_store->search(qb('config/item')) ],
        definitions => [
            $self->definition_store->search(qb('config/definition'))
        ],
    );
}

=head2 item_store

Returns a new L<Zaaksysteem::Search::ObjectQueryResultSetShim> instance that
can be used to inflate/deflate C<config/item> objects.

=cut

sub item_store {
    my $self = shift;

    return Zaaksysteem::Search::ObjectQueryResultSetShim->new(
        resultset => $self->item_rs,
        type      => 'config/item',
        inflator  => sub {
            my $item = shift;

            my $definition
                = $self->definition_store->retrieve($item->definition_id);

            my $value = $self->inflate($definition, $item->value);

            return Zaaksysteem::Object::Types::Config::Item->new(
                id         => $item->uuid,
                name       => $item->parameter,
                value      => $value,
                definition => $definition->_ref,
            );
        },
        deflator => sub {
            my $item = shift;

            my $definition
                = $self->definition_store->retrieve($item->definition_id);

            my $value = $self->deflate($definition, $item->value);

            return {
                parameter     => $item->name,
                value         => $value,
                definition_id => $definition->id,
            };
        },
    );
}

=head2 search_items_by_uuid

Search for multiple items based on the UUID

    my $items = $self->search_items_by_uuid(@uuids);

=cut

sub search_items_by_uuid {
    my $self = shift;
    my @uuid = map { qb_lit('uuid', $_) } @_;

    my @existing_items = $self->item_store->search(
        qb('config/item', { cond => qb_in(uuid => \@uuid) })
    );
    return \@existing_items;
}

=head2 update_item

Update a single config item

=cut

sig update_item => 'Zaaksysteem::Object::Types::Config::Item';

sub update_item {
    my $self = shift;
    my $item = shift;

    my $qb = qb('config/item', { cond => qb_eq(uuid => qb_lit('uuid', $item->id)) });

    my $definition = $self->definition_store->retrieve($item->definition->id);

    unless ($definition->mutable) {
        throw(
            'config/item/immutable',
            sprintf(
                "Unable to mutate config item [%s] it is not mutable",
                $item->id
            ),
        );
    }

    my $value = $self->deflate($definition, $item->value);

    my ($id) = $self->item_store->update($qb, {
        value => $value
    });

    return $self->item_store->retrieve($id);
}

=head2 update_items

Update multiple config items based on a C<HashRef> filled with UUID => value
pairs as submitted via the JSON API.

    my $updated_items = $self->update_items(\@items, { uuid => $value });

=cut

sig update_items => 'ArrayRef[Zaaksysteem::Object::Types::Config::Item],HashRef'
    . ' => ArrayRef[Zaaksysteem::Object::Types::Config::Item]';

sub update_items {
    my ($self, $items, $update) = @_;

    try {
        $self->schema->txn_do(sub { $self->_update_items($items, $update) });
    }
    catch {
        $self->log->info($_);
        die $_;
    };
    return $items;
}

sub _update_items {
    my ($self, $items, $update) = @_;

    foreach my $item (@$items) {

        my $update = $update->{ $item->id };
        foreach (keys %$update) {

            $self->log->trace("Updating item with id " . $item->id);
            my $attr = $item->meta->find_attribute_by_name($_);
            unless ($attr) {
                throw(
                    "item/update/attribute/unknown",
                    sprintf(
                        'Unknown attribute "%s" for item "%s"',
                        $_, $item->id
                    )
                );
            }

            if (defined $update->{$_}) {
                my $m = $attr->get_write_method;
                if (!$m) {
                    throw(
                        "item/update/attribute/ro",
                        sprintf(
                            'Unable to update "%s" for %s, no writer found',
                            $_, $item->id
                        )
                    );
                }
                $item->$m($update->{$_});
            }
            elsif ($attr->has_clearer) {
                $attr->clear_value($item);
            }
            else {
                throw(
                    "item/update/attribute/null",
                    sprintf(
                        'Unable to clear "%s" for %s',
                        $_, $item->id
                    )
                );

            }
        }
        $item = $self->update_item($item);
    }
    return $items;
}


with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Config::Roles::Item
);

__PACKAGE__->meta->make_immutable

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
