package Zaaksysteem::Config::Roles::Item;
use Moose::Role;

use BTTW::Tools;
use JSON;
use URI;

use Zaaksysteem::API::v1::ArraySet;
use Zaaksysteem::Types qw(UUID);

=head1 NAME

Zaaksysteem::Config::Roles::Item - A C<config/item> role

=head1 DESCRIPTION

This role implements logic that the model can/should use when it works with
config-items and -defintions.

=head1 SYNOPSIS

    package Foo;
    use Moose;
    use namespace::autoclean;

    with 'Zaaksysteem::Config::Roles::Item';

=head1 REQUIRES

Modules implementing this role must provide the following methods:

=over

=item * bibliotheek_notificaties_rs

=item * groups_rs

=item * log

=item * roles_rs

=item * subject_model

=back

=cut

requires qw(
    bibliotheek_notificaties_rs
    groups_rs
    log
    roles_rs
    subject_model
);

=head1 METHODS

=head2 get_value_type

Get the value type from the definition

=cut

sub get_value_type {
    my ($self, $definition) = @_;
    my $value_type = $definition->has_value_type_name
           ? $definition->value_type_name
           : $definition->value_type->{parent_type_name};

    if (!defined $value_type) {
        $self->log->warn(
            sprintf(
                "Empty value_type for '%s' with id '%s' ",
                $definition->config_item_name,
                $definition->id
            )
        );
        $value_type = '' ;
    }
    return $value_type;
}

=head2 get_type_options

Get the value type options from the definition

=cut

sub get_type_options {
    my ($self, $definition) = @_;

    return unless $definition->has_value_type;
    return $definition->value_type->{options} // {};
}

=head2 inflate

Inflate the values from the database

=cut

sub inflate {
    my ($self, $definition, $value) = @_;

    if ($definition->mvp) {
        try {
            $value = (decode_json($value));
        }
        catch {
            $self->log->info("Unable to decode JSON: $value");
        };
    }

    $value = $self->_flat_to_values($definition, $value);
    my $value_type = $self->get_value_type($definition);

    if ($definition->mvp && $value_type eq 'object_ref') {
        $value = Zaaksysteem::API::v1::ArraySet->new(
            is_paged => 0,
            content  => $value,
        );
    }
    return $value;
}

sub _flat_to_values {
    my ($self, $definition, $value) = @_;

    my $value_type   = $self->get_value_type($definition);
    my $type_options = $self->get_type_options($definition);

    if ($value_type eq 'uri') {
        return URI->new($value)->as_string;
    }
    elsif ($value_type eq 'boolean') {
        return $value ? \1 : \0;
    }
    elsif ($value_type eq 'object_ref') {

        my $object_type = $type_options->{object_type_name};

        if ($object_type eq 'email_template') {
            return $self->_inflate_email_template($value);
        }
        elsif ($object_type eq 'subject') {
            return $self->_get_subject_from_value($value);
        }
        elsif ($object_type eq 'group') {
            return $self->_get_group_from_value($value);
        }
        elsif ($object_type eq 'role') {
            return $self->_get_role_from_value($value, $type_options);
        }
        elsif ($object_type eq 'municipality_code') {
            return $self->_get_municipality_from_value($value, $type_options);
        }

        throw("config/role/items/inflate/object",
            "Unable to inflate object with type '$object_type'");
    }
    elsif ($value_type eq 'number') {
        $value = $value + 0;

        if ($type_options && exists $type_options->{type}) {
            if ($type_options->{type} ne 'real') {
                return int($value);
            }
        }
        return $value;
    }
    return $value;
}

=head2 deflate

Deflate the values from an item

=cut

sub deflate {
    my ($self, $definition, $value) = @_;

    $value = $self->_flat_to_values($definition, $value);

    my $value_type   = $self->get_value_type($definition);
    my $type_options = $self->get_type_options($definition);

    if (defined $value && $value_type eq 'object_ref') {

        if ($definition->mvp) {
            $value = [
                map $self->_object_ref_to_value($_, $type_options), @$value
            ];
        }
        else {
            $value = $self->_object_ref_to_value($value, $type_options);
        }
    }

    if ($definition->mvp) {
        try {
            $value = encode_json($value);
        }
        catch {
            $self->log->info(
                sprintf("Unable to encode '%s': %s", dump_terse($value), $_));
            die $_;
        };
    }

    return $value;
}

sub _object_ref_to_value {
    my ($self, $value, $type_options) = @_;

    if (!exists $type_options->{type}) {
        return $value->uuid;
    }
    elsif ($type_options->{type} eq 'legacy_id') {
        return $value->id;
    }
    elsif ($type_options->{type} eq 'label') {
        if ($value->can('name')) {
            return $value->name;
        }
        elsif ($value->can('label')) {
            return $value->label;
        }
    }
    return $value;
}

sub _get_municipality_from_value {
    my ($self, $value) = @_;

    my @result;
    foreach (@$value) {
        if ($_ =~ /^[0-9]+$/) {
            push(
                @result,
                Zaaksysteem::Object::Types::MunicipalityCode
                    ->new_from_dutch_code(
                    $_)
            );
        }
        elsif (UUID->check($_)) {
            push(
                @result,
                Zaaksysteem::Object::Types::MunicipalityCode->new_from_uuid(
                    $_)
            );
        }
        else {
            push(
                @result,
                Zaaksysteem::Object::Types::MunicipalityCode->new_from_name(
                    $_)
            );
        }
    }
    return \@result;

}

sub _get_group_from_value {
    my ($self, $value) = @_;

    # Don't even try to inflate undefined values
    return unless $value;
    if ($value =~ /^[0-9]+$/) {
        return $self->groups_rs->find($value);
    }
    elsif (UUID->check($value)) {
        return $self->groups_rs->find({uuid => $value});
    }
    else {
        return $self->groups_rs->find({name => $value});
    }
}

sub _get_role_from_value {
    my ($self, $value, $options) = @_;

    my $rs = $self->roles_rs;
    if ($options->{constraints}{system_roles}) {
        $rs = $rs->search_rs({ 'me.system_role' => 1});
    }

    # Don't even try to inflate undefined values
    return unless $value;
    if ($value =~ /^[0-9]+$/) {
        return $rs->find($value);
    }
    elsif (UUID->check($value)) {
        return $rs->find({uuid => $value});
    }
    else {
        return $rs->find({"LOWER(me.name)" => lc($value)});
    }
}

sub _get_subject_from_value {
    my ($self, $value) = @_;

    # Don't even try to inflate undefined values
    return unless $value;
    if ($value =~ /^[0-9]+$/) {
        return $self->subject_model->find($value);
    }
    elsif (UUID->check($value)) {
        return $self->subject_model->find({uuid => $value});
    }
    else {
        throw("subject/syntax/error", "Subject ID syntax is invalid!");
    }
}

sub _inflate_email_template {
    my ($self, $value) = @_;

    # Don't even try to inflate undefined values
    return unless $value;

    if ($value =~ /^[0-9]+$/) {
        return $self->bibliotheek_notificaties_rs->find($value);
    }
    elsif (UUID->check($value)) {
        return $self->bibliotheek_notificaties_rs->find({uuid => $value});
    }
    else {
        throw("email_template_id/syntax/error", "Email ID syntax is invalid!");
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
