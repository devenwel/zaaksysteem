package Zaaksysteem::SAML2::Protocol::Assertion;

use Moose;
use MooseX::Types::Moose qw/ Str /;

extends 'Net::SAML2::Protocol::Assertion';

has nameid_format => (isa => Str, is => 'ro', required => 1);

=head1 METHODS

=cut

=head2 new_from_xml( ... )

Constructor. Creates an instance of the Assertion object, parsing the
given XML to find the attributes, session and nameid.

=cut

sub new_from_xml {
    my ($class, %args) = @_;

    my $xpath = XML::XPath->new( xml => $args{xml} );
    $xpath->set_namespace('saml', 'urn:oasis:names:tc:SAML:2.0:assertion');

    my $attributes = {};
    for my $node ($xpath->findnodes('//saml:Assertion/saml:AttributeStatement/saml:Attribute')) {
        my @values = $xpath->findnodes('saml:AttributeValue', $node);
        $attributes->{$node->getAttribute('Name')} = [
            map { $_->string_value } @values
        ];
    }

    my $not_before = DateTime::Format::XSD->parse_datetime(
        $xpath->findvalue('//saml:Conditions/@NotBefore')->value
    );
    my $not_after = DateTime::Format::XSD->parse_datetime(
        $xpath->findvalue('//saml:Conditions/@NotOnOrAfter')->value
    );

    my $self = $class->new(
        attributes     => $attributes,
        session        => $xpath->findvalue('//saml:AuthnStatement/@SessionIndex')->value,
        nameid         => $xpath->findvalue('//saml:Subject/saml:NameID')->value,
        nameid_format  => $xpath->findvalue('//saml:Subject/saml:NameID/@Format')->value,
        audience       => $xpath->findvalue('//saml:Conditions/saml:AudienceRestriction/saml:Audience')->value,
        not_before     => $not_before,
        not_after      => $not_after,
    );

    return $self;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

