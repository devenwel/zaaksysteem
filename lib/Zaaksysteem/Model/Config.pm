package Zaaksysteem::Model::Config;

use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

__PACKAGE__->config(
    class => 'Zaaksysteem::Config::Model',
    constructor => 'new'
);

=head1 NAME

Zaaksysteem::Model::Object - Catalyst model factory for
L<Zaaksysteem::Config::Model>.

=cut

use BTTW::Tools;
use Syzygy::Object::Model;
use Zaaksysteem::Object::ConstantTables qw[
    ZAAKSYSTEEM_CONFIG_DEFINITIONS
];
use Zaaksysteem::Object::Reference;
use Zaaksysteem::Object::Types::Config::Definition;
use Zaaksysteem::Tools::SimpleStore;

=head2 prepare_arguments

Implements L<Catalyst::Model::Adaptor/prepare_arguments>.
See also L<Catalyst::Model::Factory>

=cut

sub prepare_arguments {
    my ($self, $c, @args) = @_;

    my $definition_store = Zaaksysteem::Tools::SimpleStore->new(
        object_type => 'config/definition'
    );

    for my $definition_spec (@{ ZAAKSYSTEEM_CONFIG_DEFINITIONS() }) {
        my $definition_args = { %{ $definition_spec } };

        # Bind definition_id to silence BTTW::Tools::sig retval checks
        my $definition_id = $definition_store->create(
            Zaaksysteem::Object::Types::Config::Definition->new($definition_args)
        );
    }

    return {
        schema           => $c->model('DB')->schema,
        item_rs          => $c->model('DB::Config'),
        subject_model    => $c->model('BR::Subject'),
        definition_store => $definition_store,

        bibliotheek_notificaties_rs => $c->model('DB::BibliotheekNotificaties'),

        groups_rs        => $c->model('DB::Groups'),
        roles_rs         => $c->model('DB::Roles'),
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

:aaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
