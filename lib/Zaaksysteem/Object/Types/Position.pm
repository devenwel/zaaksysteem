package Zaaksysteem::Object::Types::Position;

use Moose;

extends 'Zaaksysteem::Object';

=head1 NAME

Zaaksysteem::Object::Types::Position - Abstract the notion of a 'position'
within an organisation

=head1 DESCRIPTION

This class wraps an instance of L<Zaaksysteem::Object::Types::Group> and
L<Zaaksysteem::Object::Types::Role> into a single entity.

=head1 SYNOPSIS

    use Zaaksysteem::Object::Types::Position;

    my $position = Zaaksysteem::Object::Types::Position->new(
        group => Zaaksysteem::Object::Types::Group->new(...),
        role  => Zaaksysteem::Object::Types::Role->new(...)
    );

=head1 ATTRIBUTES

=head2 group

Reference to a L<Zaaksysteem::Object::Types::Group> instance which represents
the organisational unit of the position.

=cut

has group => (
    is => 'rw',
    type => 'group',
    embed => 1,
    label => 'Department',
    traits => [qw[OR]],
    required => 1
);

=head2 role

Reference to a L<Zaaksysteem::Object::Types::Role> instance which represents
the role of the position.

=cut

has role => (
    is => 'rw',
    type => 'role',
    embed => 1,
    label => 'Role',
    traits => [qw[OR]]
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
