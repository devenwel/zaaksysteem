package Zaaksysteem::Object::Types::Config::Panel;

use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

=head1 NAME

Zaaksysteem::Object::Types::Config::Panel - Data wrapper for a 'configuration
panel' structure

=head1 DESCRIPTION

Objects of this type represent a 'configuration panel' concept. This concept
binds configuration item definitions, values, and categories in one cohesive
whole.

=cut

use BTTW::Tools;

=head1 METHODS

=head2 type

Returns the object type string C<config/panel>.

=cut

override type => sub { return 'config/panel' };

=head1 ATTRIBUTES

=head2 name

Name of the panel

=cut

has name => (
    is => 'rw',
    isa => 'Str',
    label => 'Interfacenaam',
    traits => [qw[OA]],
    required => 1
);

=head2 definitions

An array of L<Zaaksysteem::Types::Object::Config::Definition>, handles L<add_definition>
and L<all_definitions> for you.

=cut

has definitions => (
    is => 'rw',
    type => 'config/definition',
    label => 'Collection of configuration definitions',
    embed => 1,
    traits => [qw[OR Array]],
    isa_set => 1,
    default => sub { return [] },
    handles => {
        add_definition => 'push',
        all_definitions => 'elements'
    }
);

=head2 items

An array of L<Zaaksysteem::Types::Object::Config::Item>, handles L<add_item>
and L<all_items> for you.

=cut

has items => (
    is => 'rw',
    type => 'config/item',
    label => 'Collection of configuration item objects',
    embed => 1,
    isa_set => 1,
    traits => [qw[OR Array]],
    default => sub { return [] },
    handles => {
        add_item => 'push',
        all_items => 'elements'
    }
);

__PACKAGE__->meta->make_immutable

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
