package Zaaksysteem::Object::Types::SavedSearch;

use Moose;

extends 'Zaaksysteem::Object';
with 'Zaaksysteem::Object::Roles::Security';

use Zaaksysteem::Types qw[Boolean NonEmptyStr];
use Zaaksysteem::Search::ZQL;

use JSON;

=head1 NAME

Zaaksysteem::Object::Types::SavedSearch - Represent user-defined ZQL searches

=head1 DESCRIPTION

=head1 PUBLIC ATTRIBUTES

This is the list of public interfaces to objects of this type

=head2 title

A generic title for the saved search

=cut

has title => (
    is => 'rw',
    isa => NonEmptyStr,
    traits => [qw[OA]],
    label => 'title',
);

=head2 query

This attribute contains a object-tree of keys and values used by the frontend
to hydrate the object search screen, it is 1-1 equivalent to the ZQL also
stored in this object.

=cut

has query => (
    is => 'rw',
    isa => NonEmptyStr,
    traits => [qw[OA]],
    required => 1,
    label => 'query',
);

=head2 public

This attribute indicates if the object can be used by externally coupled
systems.

=cut

has public => (
    is => 'rw',
    isa => Boolean,
    coerce => 1,
    traits => [qw[OA]],
    type => 'checkbox',
    label => 'public',
);

=head2 query_data

JSON-decoded data structure of the embedded query.

=cut

has query_data => (
    is => 'ro',
    isa => 'HashRef',
    lazy => 1,
    builder => 'build_query_data'
);

=head2 zql

L<Zaaksysteem::Search::ZQL> instance of the embedded query's ZQL.

=cut

has zql => (
    is => 'ro',
    isa => 'Zaaksysteem::Search::ZQL',
    lazy => 1,
    builder => 'build_zql'
);

=head1 METHODS

=head2 build_query_data

See L</query_data>.

=cut

sub build_query_data {
    my $self = shift;

    # Prevent unicode-interpretation of character data with utf8(0)
    my $decoder = JSON->new->utf8(0);

    return $decoder->decode($self->query);
}

=head2 build_zql

See L</zql>.

=cut

sub build_zql {
    my $self = shift;

    return Zaaksysteem::Search::ZQL->new($self->query_data->{ zql });
}

=head2 TO_STRING

Returns the title of the saved search.

See L<Zaaksysteem::Object/TO_STRING>.

=cut

override TO_STRING => sub {
    return shift->title;
};

=head2 index_attributes

Return just the title, because the query JSON isn't for human consumption.

=cut

override index_attributes => sub {
    return lc(shift->title);
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

