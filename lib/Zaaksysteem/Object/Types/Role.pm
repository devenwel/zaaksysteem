package Zaaksysteem::Object::Types::Role;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

use BTTW::Tools;
use Zaaksysteem::Types qw(NonEmptyStr JSONBoolean);

=head1 NAME

Zaaksysteem::Object::Types::Role - Built-in type to represent "roles"

=head1 DESCRIPTION

Roles are used in assigning cases to people.

=head1 ATTRIBUTES

=head2 role_id

The "legacy" role ID.

=cut

has role_id => (
    is => 'ro',
    isa => 'Int',
    traits => [qw(OA)],
    label => 'Role ID',
    required => 1,
    unique => 1,
);

=head2 name

The name of the role

=cut

has name => (
    is => 'ro',
    isa => NonEmptyStr,
    traits => [qw(OA)],
    label => 'Role name',
    required => 1,
    unique => 1,
);

=head2 description

A short description of the role

=cut

has description => (
    is => 'ro',
    isa => 'Str',
    traits => [qw(OA)],
    label => 'Role description',
    required => 1,
);

=head2 description

A short description of the role

=cut

has system_role => (
    is => 'ro',
    isa => JSONBoolean,
    traits => [qw(OA)],
    label => 'System role',
    coerce => 1,
    required => 1,
);

=head1 METHODS

=head2 TO_STRING

Overrides L<Zaaksysteem::Object/TO_STRING>.

=cut

override TO_STRING => sub {
    return shift->name;
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
