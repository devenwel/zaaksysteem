=encoding UTF-8

=head1 NAME

Zaaksysteem::Manual::API::V1::CountryCodes - Country code retreival

=head1 Description

Get a list of country codes from Zaaksysteem

=head2 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 URL

The base URL for this API is:

    /api/v1/general/country_codes

Make sure you use the HTTP Method C<GET> for retrieving.
You need to be logged in to Zaaksysteem to speak to this API.

=head1 Retrieve data

=head2 List all country codes

   /api/v1/general/country_codes

This API differs from most API's as the country codes do not have a UUID, they are not stored in our database.
In other API's paging is set in stone, this API endpoint does allow it by use of a paging attribute.
You can retrieve all entries by upping the paging attribute to 400. At time of writing there are 385 country codes

    /api/v1/general/country_codes/?paging=400

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab_sprint-bcc00d-65bd61",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "total_rows" : 385,
            "next" : "https://sprint.zaaksysteem.nl/api/v1/general/country_codes?page=2",
            "prev" : null,
            "pages" : 39,
            "page" : 1,
            "rows" : 10
         },
         "rows" : [
            {
               "reference" : null,
               "instance" : {
                  "alpha_one" : null,
                  "dutch_code" : "9076",
                  "label" : "Abessinië",
                  "code" : null,
                  "alpha_two" : null
                  "alpha_three" : null
               },
               "type" : "country_code"
            },
            {
               "type" : "country_code",
               "instance" : {
                  "alpha_two" : null,
                  "code" : null,
                  "label" : "Aboe Dhabi",
                  "dutch_code" : "9061",
                  "alpha_one" : null
                  "alpha_three" : null
               },
               "reference" : null
            }
         ]
      }
   }
}

=end javascript

Country codes are a special kind of object. The Dutch government has RGBZ codes, which are displayed as 'dutch_code'. The ISO 3166 standard is using a multitude of codes, known as alpha-1 and alpha-2 and also has a numeric code. These are not supplied yet, but the attributes have been included in the JSON response.

=head2 get

   /api/v1/general/legal_entity_type/01

In our API you should only retrieve objects based on their UUID, the 'reference' as found in the JSON. Because country codes do no have a UUID, you cannot get one by using a UUID.
You can however get one by using the 'code' found in the JSON response. You are however not advised to use this, as we may change this behaviour in the future.

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab_sprint-bcc00d-b0a093",
   "development" : false,
   "result" : {
      "instance" : {
         "alpha_one" : null,
         "alpha_two" : null,
         "alpha_three" : null,
         "code" : null,
         "dutch_code" : "6030",
         "label" : "Nederland"
      },
      "reference" : null,
      "type" : "country_code"
   },
   "status_code" : 200
}

=end javascript

=head1 Mutate data

Mutations for country codes are not supported at this moment.

=head1 Support

The data in this document is supported by the following test. Please make sure you use the API as described
in this test. Any use of this API outside the scope of this test is B<unsupported>

L<TestFor::Catalyst::API::V1::General::CountryCodes>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
