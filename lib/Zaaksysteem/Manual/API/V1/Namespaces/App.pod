=head1 NAME

Zaaksysteem::Manual::API::V1::Namespaces::App - App API reference
documentation

=head1 NAMESPACE URL

    /api/v1/app

=head1 DESCRIPTION

This page documents the endpoints within the C<app> API namespace.

=head1 ENDPOINTS

=head2 Core endpoints

=head3 C<GET /[interface:module]>

Returns a L<interface|Zaaksysteem::Manual::API::V1::Types::Interface>
instance.

=head2 BBV endpoints

=head3 C<POST /bbvapp/[case:id]/update_attributes>

Returns a L<case|Zaaksysteem::Manual::API::V1::Types::Case> instance.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
