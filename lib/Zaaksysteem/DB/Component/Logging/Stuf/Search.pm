package Zaaksysteem::DB::Component::Logging::Stuf::Search;

use Moose::Role;

### Searches:
my @search_columns = (
    qw/
        burgerservicenummer
        geslachtsnaam
        geboortedatum
        postcode
        straatnaam
        huisnummer
    /
);

sub onderwerp {
    my $self            = shift;
    my $params          = $self->data->{params};

    my $onderwerp       = 
        ($self->data->{via_form} ? 'Webformulier: ' : 'Behandelaar: ')
        . 'Via StUF gezocht naar: ';

    my $count           = 0;
    my @searches;
    for my $col (@search_columns) {
        next unless $params->{$col};
        $count++;

        last if $count > 3;

        push(@searches, $params->{$col});
    }

    $onderwerp          .= join(', ', @searches);

    $onderwerp          .= ', reden: "registreren zaak"';

    return substr($onderwerp, 0, 255);
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 onderwerp

TODO: Fix the POD

=cut

