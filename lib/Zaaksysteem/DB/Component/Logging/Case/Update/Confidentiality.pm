package Zaaksysteem::DB::Component::Logging::Case::Update::Confidentiality;

use Moose::Role;
use Zaaksysteem::Constants qw/ZAAKSYSTEEM_CONSTANTS/;

sub onderwerp {
    my $self = shift;

    my $old = ZAAKSYSTEEM_CONSTANTS->{confidentiality}->{$self->data->{old}};
    my $new = ZAAKSYSTEEM_CONSTANTS->{confidentiality}->{$self->data->{new}};

    return 'Vertrouwelijkheid gewijzigd van "' . $old . '" naar "' . $new . '"';
}

sub event_category { 'case-mutation'; }

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 event_category

TODO: Fix the POD

=cut

=head2 onderwerp

TODO: Fix the POD

=cut

