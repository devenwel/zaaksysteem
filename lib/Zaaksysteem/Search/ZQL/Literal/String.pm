package Zaaksysteem::Search::ZQL::Literal::String;

use Moose;

extends 'Zaaksysteem::Search::ZQL::Literal';

has '+value' => (
    isa => 'Str'
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2018 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
