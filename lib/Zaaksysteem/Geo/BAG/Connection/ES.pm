package Zaaksysteem::Geo::BAG::Connection::ES;
use Moose;

with 'MooseX::Log::Log4perl', 'Zaaksysteem::Geo::BAG::Connection';

use BTTW::Tools;
use JSON::XS;
use Search::Elasticsearch;
use Text::Unidecode qw(unidecode);
use Zaaksysteem::Types qw(NonEmptyStr);

=head1 NAME

Zaaksysteem::Geo::BAG::Connection::ES - Search for BAG objects using Elasticsearch

=head1 SYNOPSIS

    my $connection = Zaaksysteem::Geo::BAG::Connection::ES->new(
        es => Search::Elasticsearch->new(...)
    );

    my $result = $connection->search("1234AA 94");

=head1 ATTRIBUTES

=head2 es

Array reference containing the ES nodes to use for the search. 

See L<Search::Elasticsearch> for the required format.

=cut 

has es => (
    is       => 'ro',
    does     => 'Search::Elasticsearch::Role::Client',
    required => 1,
);

=head2 openbareruimte_index

The Elasticsearch index to use for C<openbareruimte> searches.

Defaults to C<bag_openbareruimte>

=cut

has openbareruimte_index => (
    isa     => 'Str',
    is      => 'ro',
    default => 'bag_openbareruimte',
);

=head2 nummeraanduiding_index

The Elasticsearch index to use for C<nummeraanduiding> searches.

Defaults to C<bag_nummeraanduiding>

=cut

has nummeraanduiding_index => (
    isa     => 'Str',
    is      => 'ro',
    default => 'bag_nummeraanduiding',
);

=head2 search

Execute a BAG query, in the form returned by
L<Zaaksysteem::Geo::BAG::Model/_parse_query>.

=cut

define_profile search => (
    required => {
        type  => 'Str',
        query => 'Str',
    }
);

sub search {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    if (defined $args->{type} && !ref $args->{type}) {
        $args->{type} = [$args->{type}];
    }

    my $ascii_query = unidecode($args->{query});

    my @indices;
    for my $type (@{ $args->{type} }) {
        push @indices, $self->_get_index_by_type($type);
    }

    my $query = $self->_parse_query($ascii_query);

    return $self->_search(
        \@indices,
        {
            query => $query,
            # Highest score wins. If scores are equal, sort by town, house number, etc.
            sort  => [
                '_score',
                { "plaats" => "asc" },
                { "huisnummer" => "asc" },
                { "huisletter" => "asc" },
                { "huisnummer_toevoeging" => "asc" },
            ],
        }
    );
}

=head2 get_exact

Search the BAG for an object that matches the given fields (full string/number match).

Accepts the following named arguments:

=over

=item * fields

A hash reference containing the fields to search for.

=back

=cut

define_profile get_exact => (
    required => {
        type   => 'Str',
        fields => 'HashRef',
    },
);

sub get_exact {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $index = $self->_get_index_by_type($args->{type});
    my %query = (
        bool => {
            filter => [
                { term => { 'postcode'              => ($args->{fields}{postcode}              || '') } },
                { term => { 'huisnummer'            => ($args->{fields}{huisnummer}            || '') } },
                { term => { 'huisletter'            => ($args->{fields}{huisletter}            || '') } },
                { term => { 'huisnummer_toevoeging' => ($args->{fields}{huisnummer_toevoeging} || '') } },
            ],

            # Sort current addresses over non-current ones
            should => {
                bool => {
                    must_not => {
                        term => { status => "Naamgeving ingetrokken" }
                    }
                }
            },
        },
    );

    return $self->_search($index, { query => \%query });
}

=head2 get

Retrieve a single BAG item by type and identifier.

=cut

define_profile get => (
    required => {
        type => 'Str',
        id   => 'Num',
    },
);

sub get {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $index = $self->_get_index_by_type($args->{type});

    my $doc = try {
        $self->es->get(
            index => $index,
            type  => '_doc',
            id    => $args->{id},
        );
    } catch {
        $self->log->info("Error retrieving BAG data from ES: '$_'");
        return;
    };

    return $doc->{_source} if $doc;
    return;
}

=head2 find_nearest

Find the BAG object of the specified type that's nearest to the specified location

=cut

my $NEAREST_OFFSET       = "1m";
my $NEAREST_SCALE        = "50m";
my $NEAREST_MAX_DISTANCE = "500m";

define_profile find_nearest => (
    required => {
        type      => 'Str',
        latitude  => 'Num',
        longitude => 'Num',
    },
);

sub find_nearest {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $index = $self->_get_index_by_type($args->{type});
    my $query = {
        "size" => 1,
        "query" => {
            "bool" => {
                "must" => {
                    "function_score" => {
                        "gauss" => {
                            "geo_lat_lon" => {
                                "origin" => {
                                    "lat" => $args->{latitude},
                                    "lon" => $args->{longitude}
                                },
                                "offset" => $NEAREST_OFFSET,
                                "scale"  => $NEAREST_SCALE,
                            }
                        }
                    }
                },
                "filter" => {
                    "geo_distance" => {
                        "distance" => $NEAREST_MAX_DISTANCE,
                        "geo_lat_lon" => {
                            "lat" => $args->{latitude},
                            "lon" => $args->{longitude}
                        }
                    }
                }
            }
        }
    };

    return $self->_search($index, $query);
}

=head2 _get_index_by_type

Given a BAG object type, returns the ES index to search in.

=cut

sub _get_index_by_type {
    my $self = shift;
    my $type = shift;

    if ($type eq 'nummeraanduiding') {
        return $self->nummeraanduiding_index;
    } elsif ($type eq 'openbareruimte') {
        return $self->openbareruimte_index;
    }

    throw(
        'geo/bag/connection/es/unknown_index',
        "No index known for BAG type: '$type'",
    );
}

=head2 _search

Execute a search on the configured Elasticsearch cluster, and return the
results ("hits.hits") as an array reference.

=cut

sub _search {
    my $self  = shift;
    my $index = shift;
    my $query = shift;

    if ($self->log->is_trace) {
        my $json = JSON::XS->new->canonical->encode($query);
        $self->log->trace("Performing ES query: $json");
    }

    my $t0 = Zaaksysteem::StatsD->statsd->start;
    my $results = try {
        $self->es->search(
            index => $index,
            body  => $query,
        );
    } catch {
        $self->log->error("Error searching for BAG in Elasticsearch: '$_'");
        return;
    };

    Zaaksysteem::StatsD->statsd->end('bag_search.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('bag_search.count', 1);

    return [] unless $results;
    return [
        map { $_->{_source} } @{ $results->{hits}{hits} }
    ];
}

=head2 _parse_query

Parses the query passed to L</search> into a data structure that looks like:

    query => {
        should => {
            exact => [
                # If a postal code is found in the search string:
                { key => 'postcode', value => '1114AD', boost => 2 },

                # For every word in the search string that consists of digits
                { key => 'huisnummer', value => '90' },

                # For every "priority_gemeentes":
                { key => 'gemeente', value => 'Amsterdam', boost => 2 ,
            ],
            prefix => [
                # For every word (including numeric ones) in the search string:
                { key => 'plaats',     value => 'Wenckebach' },
                { key => 'straatnaam', value => 'Wenckebach' },
            ],
        },
    }

This can be sent to Elasticsearch to find matching BAG objects.

=cut

sub _parse_query {
    my $self = shift;
    my $query = shift;

    my @search_terms;

    if (NonEmptyStr->check($query)) {
        push @search_terms, {
            match_phrase_prefix => {
                search_index => $query
            },
        };
    }

    my %query = (
        bool => { should => \@search_terms }
    );

    if (@{ $self->priority_gemeentes }) {
        my @boost_gemeente_terms;
        for my $gemeente (@{ $self->{priority_gemeentes} }) {
            next unless $gemeente;
            push @boost_gemeente_terms, {
                "term" => { "gemeente" => { value => $gemeente, boost => 1.1 } }
            };
        }

        %query = (
            bool => { must => { %query } }
        );

        if (@boost_gemeente_terms) {
            if ($self->priority_only) {
                $query{bool}{filter} = {
                    bool => { should => \@boost_gemeente_terms }
                },
            } else {
                $query{bool}{should} = \@boost_gemeente_terms,
            }
        }
    }

    # Ensure the search only returns currently active records
    # (old records hang around in ES so older cases can retrieve them by id if
    # necessary)
    $query{bool}{must_not} = { term => { status => "Naamgeving ingetrokken" } };

    return \%query;
}

__PACKAGE__->meta->make_immutable;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
