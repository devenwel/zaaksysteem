package Zaaksysteem::Zaken::ResultSetZaakKenmerk;
use Moose;

extends 'DBIx::Class::ResultSet';

=head1 NAME

Zaaksysteem::Zaken::ResultSetZaakKenmerk - The resultset class of ZaakKenmerk

=cut

use Array::Compare;
use BTTW::Tools;
use Clone qw/clone/;
use DateTime::Format::DateParse;
use DateTime::Format::ISO8601;
use DateTime::Format::Strptime;
use List::Util qw(any);
use Params::Profile;
use Zaaksysteem::Constants;
use Zaaksysteem::Types qw(UUID);

extends 'DBIx::Class::ResultSet';

with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Roles::Timer
);

has numeric_types => (
    is      => 'ro',
    isa     => 'HashRef',
    default => sub {
        return { map { $_ => 1 }
                qw(numeric valuta valutain valutaex valutain6 valutaex6 valutain21 valutaex21)
        };
    },
    lazy => 1,
);

=head2 _format_date

Format a date value from ISO8601 to dd-mm-yyy

=cut

sub _format_date {
    my $self = shift;
    my $str = shift;

    unless ($str) {
        $self->log->trace("No value in date kenmerk");
        return "";
    }

    my $f = DateTime::Format::Strptime->new(pattern => '%d-%m-%Y');

    my $dt = try {
        $f->parse_datetime($str);
    }
    catch {
        $self->log->trace("Unable to parse $str with Strptime: $_");
        return;
    };

    if (!$dt) {
        my $i = DateTime::Format::ISO8601->new();
        $dt = try {
            $i->parse_datetime($str);
        }
        catch {
            $self->log->warn("Unable to parse $str with ISO8601: $_");
            return;
        };
    }

    if ($dt) {
        $dt->set_formatter($f);
        return $dt . "";
    }
    return "";
}



{
    Params::Profile->register_profile(
        method  => 'create_kenmerk',
        profile => {
            required => [ qw/zaak_id bibliotheek_kenmerken_id values/],
        }
    );

    sub _sanitize_attribute {
        my $self = shift;
        my $params = {@_};

        my $schema = $self->result_source->schema;
        my $bibliotheek_kenmerk = $schema->resultset('BibliotheekKenmerken')->find($params->{bibliotheek_kenmerken_id});
        if (!$bibliotheek_kenmerk) {
            throw("sanitize_attribute/attribute/not_found",
                "Unable to find attribute with ID '$params->{bibliotheek_kenmerken_id}'"
            );
        }
        my $value_type = $bibliotheek_kenmerk->value_type;

        my $values;

        if ($self->numeric_types->{$value_type}) {
            $values = $bibliotheek_kenmerk->filter( $params->{values} );
        }
        elsif ($value_type eq 'date') {
            my $ref = ref $params->{values};
            if ($ref eq ''|| $ref eq 'DateTime') {
                $params->{values} = [ $params->{values} ];
            }
            my @values;
            foreach (@{$params->{values}}) {
                push(@values, $self->_format_date($_));
            }
            $values = \@values;
        }
        else {
            $values = $params->{values};
        }

        if (ref $values ne 'ARRAY') {
            $values = [ $values ];
        }

        ### Make sure we only save the first entry UNLESS type_multiple="meervoudige kenmerken" or
        ### veldoptie[multiple] is set (checkboxes for instance)
        if (!$bibliotheek_kenmerk->can_have_multiple_values) {
            $values = [ $values->[0] ];
        }

        my @values = grep { length($_) && !ref $_ } @$values;
        if (@values) {
            return \@values;
        }
        return;
    }

    sub create_kenmerk {
        my ($self, $params) = @_;
        $self->replace_kenmerk($params);
    }
}

sub log_field_update {
    my ($self, $params) = @_;

    my $values = $params->{values} || [];
    # value_string is always defined as the grep returns an empty array
    my $value_string = join(", ", grep { defined($_) } @$values);

    my $raw_data;

    # Convert nummeraanduiding to human readable.
    if ($value_string =~ /^(nummeraanduiding|openbareruimte|woonplaats)-(\d+).*$/) {
        $raw_data = $value_string;

        $value_string = $self->get_bag_hr(@$values);
    }

    my $match_date = qr[\d{4}\-\d{2}\-\d{2}T\d{2}:\d{2}:\d{2}(?:Z|\+\d{4})];

    if ($value_string =~ m/^($match_date);($match_date);\d+$/) {
        my $date1 = $1;
        my $date2 = $2;

        my $bibliotheek_kenmerken = $self->result_source->schema->resultset('BibliotheekKenmerken');
        my $bibliotheek_kenmerk = $bibliotheek_kenmerken->find($params->{bibliotheek_kenmerken_id});

        if ($bibliotheek_kenmerk->value_type eq 'calendar') {
            $raw_data = $value_string;

            my $start_date = DateTime::Format::DateParse->parse_datetime($date1);
            my $start_time = DateTime::Format::DateParse->parse_datetime($date2);

            # Ensure local time
            $start_date->set_time_zone('Europe/Amsterdam');
            $start_time->set_time_zone('Europe/Amsterdam');

            my $fmt_date = DateTime::Format::Strptime->new(pattern => '%d-%m-%Y');
            my $fmt_time = DateTime::Format::Strptime->new(pattern => '%R');

            $value_string = sprintf(
                'Afspraak op %s om %s',
                $fmt_date->format_datetime($start_date),
                $fmt_time->format_datetime($start_time),
            );
        }
        elsif ($bibliotheek_kenmerk->value_type eq 'calendar_supersaas') {
            $raw_data = $value_string;

            my $start = DateTime::Format::DateParse->parse_datetime($date1);
            my $end   = DateTime::Format::DateParse->parse_datetime($date2);

            # Ensure local time
            $start->set_time_zone('Europe/Amsterdam');
            $end->set_time_zone('Europe/Amsterdam');

            my $fmt = DateTime::Format::Strptime->new(
                pattern => '%d-%m-%Y %R'
            );

            $value_string = sprintf(
                '%s - %s',
                $fmt->format_datetime($start),
                $fmt->format_datetime($end)
            );
        }
        # Not qmatic, AND not SuperSaaS? Then it must be a manual entry. Don't bother manipulating the value.
    }

    my $data = {
        case_id         => $params->{zaak}->id,
        attribute_id    => $params->{bibliotheek_kenmerken_id},
        attribute_value => $value_string,
        $raw_data ? (attribute_value_raw => $raw_data) : (),
    };

    my $logging = $self->result_source->schema->resultset('Logging');

    my $event = $logging->find_recent(
        {
            event_type   => 'case/attribute/update',
            component    => 'kenmerk',
            component_id => $data->{attribute_id},
            zaak_id      => $data->{case_id},
        }
    );

    if ($event) {
        $event->data($data);
        $event->restricted(1) if ($params->{zaak}->confidentiality ne 'public');
        $event->update;
    }
    else {
        $event = $params->{zaak}->trigger_logging(
            'case/attribute/update',
            {
                component    => 'kenmerk',
                data         => $data,
                component_id => $data->{attribute_id},
                zaak_id      => $data->{case_id},
            }
        );
    }
    return $event;
}

{
    Params::Profile->register_profile(
        method  => 'replace_kenmerk',
        profile => 'Zaaksysteem::Zaken::ResultSetZaakKenmerk::create_kenmerk',
    );

    sub replace_kenmerk {
        my ($self, $params) = @_;

        try {
            $self->result_source->schema->txn_do(
                sub {

                    my $values = $self->_sanitize_attribute(%$params);

                    my %args = (
                        zaak_id                  => $params->{zaak_id},
                        bibliotheek_kenmerken_id => $params->{bibliotheek_kenmerken_id},
                    );

                    my $rs    = $self->result_source->resultset();
                    my $found = $rs->search_rs(\%args)->first;

                    if ($found) {
                        if ($values) {
                            $found->update({ 'value' => $values });
                        }
                        else {
                            $found->delete;
                        }
                    }
                    else {
                        if ($values) {
                            $rs->create(
                                {
                                    %args,
                                    'value' => $values,
                                }
                            );
                        }
                        else {
                            $self->log->debug("Will not create empty attributes with undef values");
                        }
                    }
                }
            );
        }
        catch {
            $self->log->info(
                sprintf(
                    "Unable to update attributes %s: %s",
                    dump_terse($params), $_
                )
            );
            throw(
                "replace_attribute/update/error",
                "Unable to update attribute"
            );
        };
        return 1;
    }
}




{
    Params::Profile->register_profile(
        method  => 'get',
        profile => {
            required => [ qw/bibliotheek_kenmerken_id/ ]
        }
    );


    sub get {
        my ($self, $params) = @_;

        my $dv = Params::Profile->check(params  => $params);
        die "invalid options for get" unless $dv->success;

        my $bibliotheek_kenmerken_id = $params->{bibliotheek_kenmerken_id};

        my $kenmerken   = $self->search(
            {
                bibliotheek_kenmerken_id => $bibliotheek_kenmerken_id
            },
            {
                prefetch        => [
                    'bibliotheek_kenmerken_id'
                ],
            }
        );

        return $kenmerken->first();
    }
}

=head2 update_fields_authorized

    my $success = $zaak->zaak_kenmerken->update_fields_authorized(
        {
            new_values  => {
                text_value     => 'New value',
                radio_beer     => 'Heineken'
            },
            zaak        => $schema->resultset('Zaak')->find(44),
        }
    );

Will only update values when user has proper rights to edit this case.

=cut

define_profile update_fields_authorized => (
    required => {
        new_values => 'HashRef',
        zaak => 'Zaaksysteem::Zaken::ComponentZaak',
    },
    optional => {
        user => 'Any',
        ignore_log_update => 'Bool'
    },
);

sub update_fields_authorized {
    my $self        = shift;
    my $params      = assert_profile(shift)->valid;
    my $new_values  = $params->{ new_values };
    my $zaak        = $params->{ zaak };
    my $user        = ($params->{ user } || $self->result_source->schema->current_user);

    my @groups      = @{ $user->primary_groups };
    my @roles       = @{ $user->roles };

    ### 1: Retrieve all kenmerken according to given magic strings
    my @kenmerken   = $zaak->zaaktype_node_id->zaaktype_kenmerken->search(
        {
            'bibliotheek_kenmerken_id.magic_string' => { -in => [ keys %$new_values ] },
        },
        {
            join    => 'bibliotheek_kenmerken_id',
        }
    )->all;

    ### 2: Loop over the database kenmerken, to check for valid permissions. Also, prepare
    ### the new_values hash.
    my %fields;
    for my $kenmerk (@kenmerken) {
        my $permissions     = $kenmerk->format_required_permissions;

        ### First check for propery permission on the attribute when attribute is set
        if (@$permissions) {
            my $ok;
            for my $permission (@$permissions) {
                ### Check for group
                if (!$permission->{group} || !grep { $permission->{group}->id == $_->id } @groups) {
                    next;
                }

                ### Now check for role
                if (!$permission->{role} || !grep { $permission->{role}->id == $_->id } @roles) {
                    next;
                }

                $ok = 1
            }

            if (!$ok) {
                throw(
                    'case/update_fields_authorized/field_forbidden',
                    "You do not have the proper rights to update kenmerk: " . $kenmerk->magic_string
                );
            }
        }

        ### Permissions check out, continue
        ### We do not support 'opplusbare velden' yet, deref first item.
        $fields{ $kenmerk->get_column('bibliotheek_kenmerken_id') } = $new_values->{ $kenmerk->magic_string }->[0];
    }

    return $self->update_fields(
        {
            %$params,
            new_values  => \%fields,
        }
    );
}

=head2 _process_rules_updated_fields

    $self->_process_rules_updated_fields({
        case    => $schema->resultset('Zaak')->find(24),
        values  => {
            '2280' => [
                        'Vul 3 in'
                      ],
            '2281' => [
                        'Onwijzigbaar 3'
                      ]
        },
    })

Will get all current fields in this case via C<field_values> and merges it with the given C<values>. It will
feed it to the "ruler", and make sure the "hidden fields" will be emptied in the C<$return_values>. It will
also fill values with "vul_waarde_in" when the rule engine requests it.

=cut

define_profile _process_rules_updated_fields => (
    required => {
        case   => 'Zaaksysteem::Model::DB::Zaak',
        values => 'HashRef',
    },
    optional => { set_values_except_for_attrs => 'Any', },
    defaults => { set_values_except_for_attrs => sub { return [] }, }
);

sub _process_rules_updated_fields {
    my $self    = shift;
    my $params  = assert_profile(shift || {})->valid;

    my $rules = $params->{case}->rules;

    if (!$rules->has_rules) {
        $self->log->trace("No rules to process");
        return $params->{values};
    }

    my $case_values = $params->{case}->field_values;

    my $old_values = $self->_process_rules(
        rules  => $rules,
        values => $case_values
    );

    my $values = { %$case_values, %{$params->{values} }};
    my %copy = %{$values};

    # Exclude user input fields from being updated. It makes no sense to
    # update the field according to a rule when the attribute you want
    # to save it touched by a rule.
    push(@{$params->{set_values_except_for_attrs}}, keys %{$params->{values}});

    my $new_values = $self->_process_rules(
        rules  => $rules,
        values => \%copy,
        set_values_except_for_attrs => $params->{set_values_except_for_attrs},
    );

    # Validate the results of the rule execution:
    # If the old values do not match the case values
    # and the new values equals the old value, the value may
    # not be changed.
    my ($old, $new, $case);
    foreach (keys %$new_values) {
        $old  = $old_values->{$_} // '';
        $new  = $new_values->{$_} // '';
        $case = $case_values->{$_};

        if ($case && $old eq $new && $old ne $case || $case && $case eq $new) {
            delete $new_values->{$_};
        }
    }

    if (!$params->{force_rules} && $self->strip_identical_fields($params->{case}, $new_values)) {
        return;
    }
    return $new_values;
}

define_profile _process_rules => (
    required    => {
        rules  => 'Zaaksysteem::Backend::Rules',
        values => 'HashRef',
    },
    optional    => {
        set_values_except_for_attrs => 'Any',
    },
    defaults => {
        set_values_except_for_attrs => sub { return [] },
    }
);

sub _process_rules {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    my $rules  = $params->{rules};
    my $values = $params->{values};

    my $val_obj = $rules->validate({
        %{$rules->rules_params},
        %{$values}
    });

    return $val_obj->process_params({
        engine    => $rules,
        values    => $values,
        keyformat => 'bibliotheek_kenmerken_id',
        set_values_except_for_attrs =>
            $params->{set_values_except_for_attrs},
    });
}

define_profile update_fields => (
    required => {
        new_values => 'HashRef',
        zaak       => 'Zaaksysteem::Zaken::ComponentZaak',
    },
    optional => {
        ignore_log_update           => 'Bool',
        set_values_except_for_attrs => 'Any',
        skip_touch                  => 'Bool',
        force_rules                 => 'Bool',
    },
    defaults => {
        skip_touch        => 0,
        force_rules       => 0,
        ignore_log_update => 0,
    }
);

sub update_fields {
    my $self = shift;

    my $params = assert_profile(shift)->valid;

    my $zaak = $params->{ zaak };
    my $zaak_id = $zaak->id;

    my $error = 0;

    my $new_values = $self->_process_value_types($zaak, $params->{new_values});

    # Empty updates...
    if (!$params->{force_rules} && $self->strip_identical_fields($zaak, $new_values)) {
        $self->log->debug("Nothing to update, skipping updates");
        return;
    }

    $new_values = $self->_process_rules_updated_fields(
        {
            case   => $zaak,
            values => $new_values,
            set_values_except_for_attrs =>
                $params->{set_values_except_for_attrs},
            force_rules => $params->{force_rules},
        }
    );

    
    # From Zaaksysteem::Backend::Rules::CasetypeRule
    #   if action type eq 'set_value' and  action kenmer eq 'case_result'
    #   then
    #   we will return a ZaaktypeResultaten
    #
    # which we will unpack into normal 'new_values attributes'
    do {
        my $result = $new_values->{'case.result'};
        $result = $result->[0] if ref $result eq 'ARRAY';
        if (
            blessed $result
            &&
            $result->isa('Zaaksysteem::Model::DB::ZaaktypeResultaten')
        ) {
            $new_values->{'case.result_obj'} = $result;
            $new_values->{'case.result_id'}  = $result->id;
            $new_values->{'case.result'}     = $result->resultaat;
        }
    };

    $self->log->debug(
        sprintf(
            "Updating attributes %s for case %d changed by rules to: %s",
            dump_terse($params->{new_values}), $zaak_id, dump_terse($new_values)
        )
    );

    if (!$new_values) {
        $self->log->debug("Nothing to update, skipping updates");
        return;
    }

    my $result       = $new_values->{'case.result'};
    my $result_id    = $new_values->{'case.result_id'};
    my $result_obj   = $new_values->{'case.result_obj'};
    my $confidential = $new_values->{'case.confidentiality'};
    my $price        = $new_values->{'case.price'};

    $self->result_source->schema->txn_do(sub {
        try {
            if ($result_obj) {
                $zaak->set_result_by_type($result_obj);
                $zaak->update;
            }
            elsif ($result_id) {
                $zaak->set_result_by_id(
                    ref $result_id eq 'ARRAY' ? $result_id->[0] : $result_id);
                $zaak->update;
            }
            elsif ($result) {
                $zaak->set_resultaat(
                    (ref $result eq 'ARRAY' ? $result->[0] : $result));
                $zaak->update;
            }

            if ($confidential) {
                $zaak->set_confidentiality((ref $confidential eq 'ARRAY' ? $confidential->[0] : $confidential));
                $zaak->update;
            }

            if (defined $price) {
                $price = ref $price eq 'ARRAY' ? $price->[0] : $price;
                # case.price is guaranteed to be xx,xx
                $price =~ s/,/./g;
                $zaak->update({ payment_amount => $price });
            }

            my @keys = grep { $_ =~ /^[0-9]+$/ } keys %$new_values;
            {
                my $rs = $zaak->zaaktype_node_id->zaaktype_kenmerken->search(
                    {
                        'me.bag_zaakadres'            => 1,
                        'bibliotheek_kenmerken_id.id' => { -in => \@keys }
                    },
                    {
                        prefetch => 'bibliotheek_kenmerken_id',
                        order_by => 'me.id'
                    }
                );

                $self->_process_case_location($rs, $new_values, $zaak);
            }

            for my $bibid (@keys) {
                my $create_kenmerk_params = {
                    bibliotheek_kenmerken_id    => $bibid,
                    zaak_id                     => $zaak_id,
                    values                      => $new_values->{$bibid}
                };

                $self->replace_kenmerk($create_kenmerk_params);

                unless ($params->{ ignore_log_update }) {
                    my $values = $create_kenmerk_params->{values};

                    $create_kenmerk_params->{values} =
                        UNIVERSAL::isa($values, 'ARRAY') ? $values : [$values];

                    delete $create_kenmerk_params->{zaak_id};
                    $create_kenmerk_params->{zaak} = $zaak;

                    $self->log_field_update($create_kenmerk_params);
                }
            }

            $zaak->discard_changes;
            if (!$params->{skip_touch}) {
                $zaak->touch;
            }
        }
        catch {
            $error++;
            $self->log->error($_);
            throw('rs/case.attribute/update_fields', "Could not update attribute " . $_);
        }

    });

    # We've have updated the case, invalidate the cache
    delete($zaak->{cached_field_values});
    return scalar keys %{$new_values};
}

=head2 strip_identical_fields

Strip the identical fields from the input so we know if we need to update the case or not.
Returns a boolean value:

1 if the all the identical fields are stripped
0 if there are fields left for updating

=cut

sub strip_identical_fields {
    my ($self, $zaak, $new_values) = @_;

    my $values = $zaak->field_values;

    $values->{'case.result'}          = [$zaak->resultaat];
    $values->{'case.confidentiality'} = [$zaak->confidentiality];

    $values->{'case.price'}           = $zaak->payment_amount;

    my $compare = Array::Compare->new();

    foreach (keys %$new_values) {
        my $old_values = $values->{$_};
        my $value      = $new_values->{$_};
        # Sometimes values are not what they seem to be
        if (ref $value ne 'ARRAY') {
            $value = [ $value ];
            $new_values->{$_} = $value;
        }

        if (defined $old_values) {
            if (ref $old_values) {
                if ($compare->compare($old_values, $value)) {
                    delete $new_values->{$_};
                }
            }
            elsif ($old_values eq $value) {
                delete $new_values->{$_};
            }
        }
        # if the first element is undef and case value is also undef,
        # delete it, because.. no updates needed, same for scalar values
        elsif (!defined $value) {
            delete $new_values->{$_};
        }
        elsif (ref $value && !defined $value->[0]) {
            delete $new_values->{$_};
        }
    }

    return keys %$new_values ? 0 : 1;
}


=head2 get_bag_hr

Convert bag strings to something human readable.

=cut

sub get_bag_hr {
    my ($self) = shift;
    my @values = @_;

    my @hr;
    my $value_string;
    for my $bag_id (@values) {
        my ($type, $id) = split /-/, $bag_id;

        my $result;
        if ($type && $id) {
            $result = $self->result_source->schema->bag_model->get($type => $id);
        }

        if ($result) {
            push @hr, $result->to_string;
        } else {
            push @hr, "Onbekend BAG-object: '$bag_id'";
        }
    }

    $value_string = join("; ", grep { defined($_) } @hr);

    return $value_string;
}


=head2 delete_fields

The use case for this method is fields that are being hidden by rules.
We receive a list of field_ids (bibliotheek_kenmerken) and we look
if any values are stored. If so, these are deleted, and the fields that
are actually affected are gathered and put in a log.

=cut

sub delete_fields {
    my ($self, $arguments) = @_;

    my $bibliotheek_kenmerken_ids = $arguments->{bibliotheek_kenmerken_ids} or die "need bibliotheek_kenmerken_ids";

    throw('delete_fields/missing_argument', 'ZaakKenmerk::delete_fields needs a "zaak" argument')
        unless $arguments->{zaak};

    my $zaak_id = $arguments->{zaak}->id;

    if ($self->log->is_trace) {
        $self->log->trace("Hiding fields in case $zaak_id " . join ", ", @$bibliotheek_kenmerken_ids);
    }

    my $current = $self->search({
        zaak_id => $zaak_id, # ensure only this case is affected
        bibliotheek_kenmerken_id => {
            '-in' => $bibliotheek_kenmerken_ids
        }
    });

    # if there are no values stored that need deletion, we're done. nothing to do here
    my @current = $current->all
        or return;

    # a list with removed attributes. because checkboxes are stored as separate values
    # abuse a hash to ensure uniqueness.
    # using get_column to avoid subqueries - performance tweak
    my $deleted = { map { $_->get_column('bibliotheek_kenmerken_id') => 1 } @current };

    $current->delete;

    my $schema = $self->result_source->schema;

    my $logging = $schema->resultset('Logging');
    my $bibliotheek_kenmerken = $schema->resultset('BibliotheekKenmerken');

    # get the names of the deceased attributes
    my $attributes = join ", ", map { $_->naam } $bibliotheek_kenmerken->search({
        id => {
            '-in' => [keys %$deleted]
        }
    })->all;

    # if you delete a bunch of fields, the logging line get too long
    # we need to solve this in the logging module, danger, danger
    $attributes = substr($attributes, 0, 100) . '...' if length $attributes > 100;

    $arguments->{zaak}->trigger_logging('case/attribute/removebulk', {
        component => 'kenmerk',
        data => {
            attributes => $attributes,
            reason => 'verborgen door regels'
        }
    });
}

define_profile _process_appointment_fields => (
    required => {
        values => 'HashRef',
        case   => 'Zaaksysteem::Zaken::ComponentZaak'
    },
);

sub _process_appointment_fields {
    my ($self, $rs, $values) = @_ ;

    while(my $attribute = $rs->next) {
        my $id = $attribute->get_column('bibliotheek_kenmerken_id');
        my $val = $values->{$id};
        if (ref $val eq 'ARRAY') {
            foreach (@$val) {
                if (defined $_ && !UUID->check($_)) {
                    throw('case/attribute/appointment/invalid_value', "'$_' is not a valid UUID");
                }
            }
        }
        elsif (defined $val && !UUID->check($val)) {
            throw('case/attribute/appointment/invalid_value', "'$val' is not a valid UUID");
        }
    }
}

sub _process_value_types {
    my ($self, $case, $values) = @_;

    my @keys = grep(/^\d+$/, keys %{$values});

    my $rs = $case->zaaktype_node_id->zaaktype_kenmerken->search(
        { 'bibliotheek_kenmerken_id.id' => { -in => \@keys } },
        { prefetch => 'bibliotheek_kenmerken_id' }
    );

    $self->_process_uppercase_fields(
            $rs->search_rs({ 'bibliotheek_kenmerken_id.value_type' => 'text_uc' }),
            $values
    );
    $self->_process_appointment_fields(
        $rs->search_rs({ 'bibliotheek_kenmerken_id.value_type' => 'appointment' }),
        $values
    );
    return $values;
}

sub _process_uppercase_fields {
    my ($self, $rs, $values) = @_ ;

    while(my $attribute = $rs->next) {
        my $id = $attribute->get_column('bibliotheek_kenmerken_id');
        my $val = $values->{$id};
        if (ref $val eq 'ARRAY') {
            $val = [ map { uc($_) } @$val ];
        }
        else {
            $val = uc($val);
        }
        $values->{$id} = $val;
    }

}

sub _process_case_location {
    my ($self, $rs, $values, $case) = @_ ;

    while (my $zt_kenmerk = $rs->next) {
        my $bib_id = $zt_kenmerk->get_column('bibliotheek_kenmerken_id');
        my $values = $values->{ $bib_id };

        # Multivalue fields aren't supported for case location
        if (ref $values eq 'ARRAY') {
            $values = $values->[0];
        }

        my $type = $zt_kenmerk->bibliotheek_kenmerken_id->value_type;

        my $zaak_bag;
        if ($type eq 'googlemaps') {
            $zaak_bag = $self->_create_zaak_bag_googlemaps($case->id, $values);
        }
        elsif ($type =~ /^bag_/) {
            $zaak_bag = $self->_create_zaak_bag($case->id, $values);
        }

        my $old_bag = $case->locatie_zaak;

        $case->update({ locatie_zaak => $zaak_bag });

        $old_bag->delete if defined $old_bag;
    }

    return;
}

sub _lookup_nearest_bag_object {
    my $self = shift;
    my $value = shift;

    my $schema = $self->result_source->schema;
    my $bag_model = $schema->bag_model;

    my $bag_query = $bag_model->parse_search_term($value);
    return unless $bag_query;

    my $bag_object = try {
        $bag_model->get_exact(%$bag_query);
    } catch {
        $self->log->error("Error retrieving BAG entry: '$_'");
        return;
    };

    return $bag_object;
}

sub _create_zaak_bag_googlemaps {
    my $self    = shift;
    my $case_id = shift;
    my $value   = shift;

    my $bag_object = try {
        $self->_lookup_nearest_bag_object($value);
    } catch {
        $self->log->debug("No nearest BAG object found for '$value' - $_");
        return;
    };
    return if not $bag_object;

    return $self->_create_zaak_bag(
        $case_id,
        $bag_object->nummeraanduiding_id,
    ); 
}

sub _create_zaak_bag {
    my $self    = shift;
    my $case_id = shift;
    my $value   = shift;

    return if not defined $value;

    my @ALLOWED = qw/nummeraanduiding openbareruimte/;
    my ($bag_type, $bag_id) = $value =~ m/^(\w+)-(\d+)$/;

    return unless $bag_type && $bag_id && any { $bag_type eq $_ } @ALLOWED;

    my $table_reference = "bag_${bag_type}_id";
    my $zaakbag = $self->result_source->schema->resultset('ZaakBag')->create_bag(
        {
            zaak_id          => $case_id,
            bag_type         => $bag_type,
            $table_reference => $bag_id,
            bag_id           => $bag_id,
        }
    );

    return $zaakbag;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 create_kenmerk

TODO: Fix the POD

=cut

=head2 create_kenmerken

TODO: Fix the POD

=cut

=head2 get

TODO: Fix the POD

=cut

=head2 log_field_create

TODO: Fix the POD

=cut

=head2 log_field_update

TODO: Fix the POD

=cut

=head2 replace_kenmerk

TODO: Fix the POD

=cut

=head2 update_field

TODO: Fix the POD

=cut

=head2 update_fields

TODO: Fix the POD

=cut

