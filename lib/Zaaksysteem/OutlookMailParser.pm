package Zaaksysteem::OutlookMailParser;
use Moose;

use Email::Outlook::Message;
use IO::All;
use Mail::Track;
use BTTW::Tools;

=head2 process_outlook_message

Processes an outlook message file (from disk) to a Mail::Track::Message object.

=cut

define_profile process_outlook_message =>
    (required => [qw[path original_name]]);

# TODO: Fix this so we can perform actions on the mime type
sub process_outlook_message {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;
    my $path = $opts->{path};

    die "file $path does not exist" unless -e $path;

    my $original_name = $opts->{original_name};

    my $mime;

    if ($original_name =~ m|\.msg$|i) {
        $mime = Email::Outlook::Message->new($path)->to_email_mime->as_string;
    }
    elsif ($original_name =~ m|\.eml$|i) {
        $mime = io($path)->slurp;
    }

    if ($mime) {
        return Mail::Track->new()->parse($mime);
    }
    return;
}


# TODO:
# Move this 'format' function to elsewhere, has nothing to do with outlook mail
# parsing.

=head2 format

Format an the e-mail header for outputting it to a file.

=cut

define_profile format => (
    required => [qw/from to subject body/],
    optional => [qw/attachments cc bcc/],
);

sub format {
    my $self   = shift;
    my $params = assert_profile(shift)->valid;

    my $attachments
        = $params->{attachments}
        ? join "\n", map { $_->{filename} } @{ $params->{attachments} }
        : 'geen';

    my $str = sprintf("Van: %s\nAan: %s\n", $params->{from}, $params->{to});
    if ($params->{cc}) {
        $str .= "CC: $params->{cc}\n";
    }
    if ($params->{bcc}) {
        $str .= "BCC: $params->{bcc}\n";
    }

    $str .= sprintf("Onderwerp: %s\n\nBijlagen: %s\n\n%s", $params->{subject}, $attachments,    $params->{body});
    return $str;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
