use utf8;
package Zaaksysteem::Schema::CaseProperty;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CaseProperty

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<case_property>

=cut

__PACKAGE__->table("case_property");

=head1 ACCESSORS

=head2 id

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 name

  data_type: 'text'
  is_nullable: 0

unified magic_string/v1 attribute/v0 property

=head2 type

  data_type: 'text'
  is_nullable: 0

de-normalized type of the /value/ of the property in the context of the referent object

=head2 namespace

  data_type: 'text'
  is_nullable: 0

=head2 value

  data_type: 'jsonb'
  is_nullable: 1

syzygy-style value blob; {"value_type_name":"text","value":"myval","meta":"data"}

=head2 value_v0

  data_type: 'jsonb'
  is_nullable: 1

object_data-style value blob; {"human_label":"Aanvrager KvK-nummer","human_value":"123456789","value":":123456789","name":"case.requestor.coc","attribute_type":"text"}

=head2 case_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 object_id

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 0
  size: 16

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "name",
  { data_type => "text", is_nullable => 0 },
  "type",
  { data_type => "text", is_nullable => 0 },
  "namespace",
  { data_type => "text", is_nullable => 0 },
  "value",
  { data_type => "jsonb", is_nullable => 1 },
  "value_v0",
  { data_type => "jsonb", is_nullable => 1 },
  "case_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "object_id",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 0, size => 16 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<case_property_name_ref_idx>

=over 4

=item * L</name>

=item * L</namespace>

=item * L</object_id>

=item * L</case_id>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "case_property_name_ref_idx",
  ["name", "namespace", "object_id", "case_id"],
);

=head1 RELATIONS

=head2 case_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("case_id", "Zaaksysteem::Schema::Zaak", { id => "case_id" });

=head2 object_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ObjectData>

=cut

__PACKAGE__->belongs_to(
  "object_id",
  "Zaaksysteem::Schema::ObjectData",
  { uuid => "object_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2018-08-08 16:58:18
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:AyBwkR2nfd6eznqR/d6QyA

use JSON::XS qw[];

__PACKAGE__->resultset_class(
    'Zaaksysteem::Backend::Case::Property::ResultSet'
);

__PACKAGE__->load_components(
    '+Zaaksysteem::Backend::Case::Property::Component',
    __PACKAGE__->load_components()
);

__PACKAGE__->inflate_column(value => {
    inflate => sub { JSON::XS->new->decode(shift || '{}') },
    deflate => sub { JSON::XS->new->canonical(1)->encode(shift || {}) }
});

__PACKAGE__->inflate_column(value_v0 => {
    inflate => sub { JSON::XS->new->decode(shift || '{}') },
    deflate => sub {
        JSON::XS->new->canonical(1)->convert_blessed(1)->encode(shift || {})
    }
});

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
