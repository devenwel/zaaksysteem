package Zaaksysteem::External::DataB;
use Moose;

with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Roles::UserAgent
);

=head1 NAME

Zaaksysteem::External::DataB - An interface for DataB calls

=head1 DESCRIPTION

Takes care of all the calls used for the DataB interface

=head1 SYNOPSIS

    use Zaaksysteem::External::DataB;

    my $datab = Zaaksysteem::External::DataB->new(
    );

    $datab->get_session_url();

=head1 ATTRIBUTES

=head2 cid

The customer ID on for the DataB customer

=head2 secret

The shared secret

=head2 endpoint

The endpoint for MultiChannel DataB

=cut

has cid => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has secret => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has endpoint => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

use HTTP::Request;
use URI;
use XML::XPath;
use BTTW::Tools;
use Digest::MD5 qw(md5_hex);

=head1 METHODS

=head2 get_session_url

Get the session URL for the client

=cut

define_profile get_session_url => (
    required => {
        subject    => 'Zaaksysteem::Betrokkene::Object',
        useragent  => 'Str'
    }
);

sub get_session_url {
    my $self      = shift;
    my $params    = assert_profile({@_})->valid;

    my $res = $self->ua->request($self->build_session_request(%$params));
    my $xml = $self->assert_http_response($res);
    return $self->assert_session_xml($xml);
}

=head2 assert_session_xml

Get the session response URI from the XML returned by MultiChannel/DataB

=cut

sub assert_session_xml {
    my ($self, $xml) = @_;

    my $xpath = XML::XPath->new(xml => $xml);

    unless ($xpath->findvalue('/docserver/response/succes') eq 'true') {
        throw(
            'sysin/multichannel/request_unsuccessful',
            sprintf('MultiChannel session URL request did not succeed: %s',
                $xpath->findvalue('/docserver/response/error/@description')
                    ->value)
        );
    }

    return $xpath->findvalue('/docserver/response/url');
}

=head2 build_session_request

Helper method for constructing a MultiChannel Authentication request. Returns
an instance of L<HTTP::Request> for that purpose.

=cut

define_profile build_session_request => (
    required => {
        subject    => 'Zaaksysteem::Betrokkene::Object',
        useragent  => 'Str'
    }
);

sub build_session_request {
    my $self      = shift;
    my $params    = assert_profile({@_})->valid;

    my $hash = md5_hex($params->{useragent});

    my $uri = URI->new($self->endpoint);
    $uri->query_form(
        {
            cid     => $self->cid,
            secret  => $self->secret,
            request => 'authenticate',
            clhash  => $hash,
            uid     => $params->{subject}->bsn,
        }
    );

    my $req = HTTP::Request->new('GET');
    $req->uri($uri);
    return $req;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
