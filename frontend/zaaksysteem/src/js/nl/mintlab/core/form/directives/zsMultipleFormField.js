/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.directive('zsMultipleFormField', [ function ( ) {
			
			return {
				scope: true,
				require: [ 'ngModel' ],
				link: function ( scope, element, attrs, controllers ) {

					var ngModel = controllers[0],
						fields = angular.copy(scope.$eval(attrs.fields)),
						name = attrs.name,
						config;

					config = {
						name: name,
						options: {
							autosave: true
						},
						actions: [
							{
								id: 'submit',
								label: 'Opslaan',
								type: 'submit'
							}
						],
						fields: fields
					};

					scope.addField = function ( ) {
						var value = ngModel.$viewValue;

						if(!value) {
							value = [];
						}

						value = value.concat({});

						ngModel.$setViewValue(value);
					};

					scope.getFormConfig = function ( index ) {
						return _.assign({}, config, { name: config.name + '_' + index });
					};

					scope.removeField = function ( index ) {

						var value = ngModel.$modelValue || [];

						value = angular.copy(value);

						value.splice(index, 1);

						ngModel.$setViewValue(value);

					};

					scope.getValues = function ( ) {
						return ngModel.$modelValue;
					};

					scope.onViewChange = function ( ) {
						ngModel.$setViewValue(angular.copy(ngModel.$viewValue));
					};

				}
			};
			
		}]);
	
})();
