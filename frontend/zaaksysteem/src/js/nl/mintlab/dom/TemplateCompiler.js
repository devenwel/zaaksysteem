/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.dom')
		.service('templateCompiler', [ '$q', '$templateCache', '$compile', '$http', '$document', '$interpolate', function ( $q, $templateCache, $compile, $http, $document, $interpolate ) {
			
			var trim = window.zsFetch('nl.mintlab.utils.shims.trim'),
				templatePromises = {},
				templateData = {},
				compilerPromises = {},
				compilers = {};
			
			function TemplateCompiler ( ) {
				
			}
			
			function compileElement ( template ) {
				var deferred = $q.defer(),
					promise = deferred.promise;
					
				if(templatePromises[template]) {
					templatePromises[template].then(function ( ) {
						deferred.resolve(templateData[template]);
					});
					return promise;
				}
				
				templatePromises[template] = $q.when($templateCache.get(template) || $http.get(template, { cache: $templateCache })).then(function ( tpl ) {
					
					var div,
						element;
									
					if(angular.isObject(tpl)) {
						tpl = tpl.data;
					}
					
					div = $document[0].createElement('div');
					div.innerHTML = trim(
						tpl.replace(/<\[/g, $interpolate.startSymbol())
							.replace(/\]>/g, $interpolate.endSymbol())
					);
					
					element = angular.element(div.childNodes);
					
					templateData[template] = element;
					
					deferred.resolve(element);
					
				});
				
				return promise;
			}
			
			function getCompiler ( template ) {
				
				var deferred;
				
				if(!compilerPromises[template]) {
					deferred = $q.defer();
					compilerPromises[template] = deferred.promise;
					compileElement(template).then(function ( element ) {
						var compiler = $compile(element);
						compilers[template] = compiler;
						deferred.resolve(compiler);
					});
				}
				
				return compilerPromises[template];
			}
			
			TemplateCompiler.prototype.getCompiler = function ( template ) {
				return getCompiler(template);
			};
			
			TemplateCompiler.prototype.getElement = function ( template ) {
				return compileElement(template);
			};
			
			return new TemplateCompiler();
			
		}]);
	
})();

