/*global angular*/
(function ( ) {

	angular.module('Zaaksysteem')
		.directive('ngFocusIn', function ( ) {
			
			return function ( scope, element, attrs ) {
				element.bind('focusin', function ( ) {
					scope.$eval(attrs.ngFocusIn);
				});
			};
			
		});
	
})();