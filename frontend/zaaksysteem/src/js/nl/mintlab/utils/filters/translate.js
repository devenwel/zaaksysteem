/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.locale')
		.filter('translate', [ 'translationService', function ( translationService ) {
			
			return function ( /*from*/  ) {
				return translationService.get.apply(translationService, arguments);
			};
			
		}]);
	
})();