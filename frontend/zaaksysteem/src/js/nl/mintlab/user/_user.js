/*global angular*/
(function () {
    "use strict";

    angular.module('Zaaksysteem.user', [ 'Zaaksysteem.events' ]);
}());