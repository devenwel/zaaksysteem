/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.net')
		.service('fileUploader', [ '$q', '$window', function ( $q, $window) {
			
			var EventDispatcher = window.zsFetch('nl.mintlab.events.EventDispatcher'),
				inherit = window.zsFetch('nl.mintlab.utils.object.inherit'),
				FileUpload = window.zsFetch('nl.mintlab.docs.FileUpload'),
				IFrameUpload = window.zsFetch('nl.mintlab.docs.IFrameUpload'),
				safeApply = window.zsFetch('nl.mintlab.utils.safeApply'),
				indexOf = window.zsFetch('nl.mintlab.utils.shims.indexOf'),
				supports = !!$window.File;
				
			function FileUploader ( ) {
                this._uploadList = [];
                this._busy = false;
			}
			
			inherit(FileUploader, EventDispatcher);
			
			FileUploader.prototype.upload = function ( file, url, params, scope ) {
                var self = this,
                    upload = supports ? new FileUpload(file) : new IFrameUpload(file),
					deferred = $q.defer(),
					promise = deferred.promise,
                    uploadList = self._uploadList;
					
				if(!params) {
					params = {};
				}
					
				if(!supports) {
					params.return_content_type = 'text/plain';
                }

                upload.file = file;
                upload.url = url;
                upload.params = params;
				
                uploadList.push(upload);
				
				function resolve ( ) {
					deferred.resolve(upload);
				}
				
				function reject ( ) {
					deferred.reject(upload);
				}
				
				upload.subscribe('complete', function ( ) {
                    if(scope) { 
						safeApply(scope, resolve);
					} else {
						resolve();
                    }
				});
				upload.subscribe('error', function ( ) {
                    self._busy = false;
                    if(scope) {
						safeApply(scope, reject);
					} else {
						reject();
                    }
				});
				upload.subscribe('end', function ( ) {
                    uploadList.splice(indexOf(uploadList, upload), 1);

                    self._busy = false;
                    self.doNextUpload();
                });
                
                upload.subscribe('start', function() {
                    self._busy = true;
                });
				
				upload.promise = promise;
				
				self.publish('fileadd', upload);
                self.doNextUpload();
                
				return upload;
            };
            

			FileUploader.prototype.doNextUpload = function ( ) {
                if (this._busy || !this._uploadList.length) {
                    return;
                }

                var nextUpload = _.find(this._uploadList, function(thisUpload) {
                    return thisUpload.hasOwnProperty('file');
                });

                nextUpload.send(nextUpload.url, nextUpload.params);
            };
            
			
			FileUploader.prototype.getUploadList = function ( ) {
				return this._uploadList;
			};
			
			FileUploader.prototype.supports = function ( ) {
				return supports;
			};
			
			return new FileUploader();
			
		}]);
	
})();
