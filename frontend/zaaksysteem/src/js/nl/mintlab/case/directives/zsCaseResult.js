/*global angular,isResolved*/
// ZS-TODO: refactor module and declare isResolved in the directive
(function ( ) {

	angular.module('Zaaksysteem.case')
		.directive('zsCaseResult', [ '$window', 'systemMessageService', function ( $window, systemMessageService ) {

			return {
				require: [ 'zsCaseResult', '^?zsCaseWebformRuleManager', '^?zsCaseView' ],
				controller: [ '$scope', '$attrs', function ( $scope, $attrs ) {

					var ctrl = this,
						zsCaseWebformRuleManager,
						zsCaseView,
						canChange,
						submitting;

					function getResult ( ) {
						return ctrl.result || ctrl.getValue();
					}

					ctrl.link = function ( controllers ) {
						zsCaseWebformRuleManager = controllers[0];
						zsCaseView = controllers[1];

						if(!zsCaseWebformRuleManager || !zsCaseView) {
							// get caseWebformRuleManager/caseView from scope because zsPopup
							// breaks inheritedData()
							zsCaseWebformRuleManager = $scope.caseWebformRuleManager;
							zsCaseView = $scope.caseView;
						}
					};

					ctrl.canChange = function ( ) {
						return canChange && !isResolved && !zsCaseWebformRuleManager.isFixedValue('case.result');
					};

					ctrl.getValue = function ( ) {
						return zsCaseWebformRuleManager.getValue('case.result');
					};

					ctrl.isResult = function ( value ) {
						var isResult,
							result = getResult();

						isResult = result === value;

						return isResult;
					};

					ctrl.setResult = function ( value ) {
						if(ctrl.getValue() !== value) {
							if(ctrl.resolveEarly()) {
								ctrl.result = value;
							} else {
								zsCaseWebformRuleManager.setValue('case.result', value);
							}
						}
					};

					ctrl.resolveEarly = function ( ) {
						return $scope.$eval($attrs.resolveEarly);
					};

					ctrl.isResolveValid = function ( ) {
						return ctrl.reason && !!getResult();
					};

					ctrl.isResolveButtonEnabled = function ( ) {
						return ctrl.isResolveValid() && !submitting;
					};

					ctrl.resolve = function ( ) {

						submitting = false;

						zsCaseView.resolveCase(getResult(), ctrl.reason)
							.then(function ( ) {
								systemMessageService.emit('De zaak is succesvol afgehandeld.');
								$window.location.reload(true);
							})
							['catch'](function ( ) {
								systemMessageService.emit('Er ging iets fout bij het afhandelen van de zaak. Probeer het later opnieuw.');
							})
							['finally'](function ( ) {
								submitting = false;
							});
					};

					canChange = $scope.$eval($attrs.zsCaseResultCanChange);
					isResolved = $scope.$eval($attrs.zsCaseResultIsResolved);

					return ctrl;
				}],
				controllerAs: 'caseResult',
				link: function ( scope, element, attrs, controllers ) {

					controllers[0].link(controllers.slice(1));

				}
			};

		}]);

})();
